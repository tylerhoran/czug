# frozen_string_literal: true

class AddChargeToEnrollment < ActiveRecord::Migration[5.0]
  def change
    add_reference :enrollments, :charge, foreign_key: true
  end
end
