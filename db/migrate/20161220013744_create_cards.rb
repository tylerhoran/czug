# frozen_string_literal: true

class CreateCards < ActiveRecord::Migration[5.0]
  def change
    create_table :cards do |t|
      t.integer :card_type
      t.integer :brand
      t.string :last_four
      t.string :stripe_card_token
      t.string :expiration_month
      t.string :expiration_year
      t.boolean :default, default: false
      t.references :user, index: true
      t.timestamps
    end
  end
end
