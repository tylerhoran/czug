# frozen_string_literal: true

class AddAvailabilityToInstance < ActiveRecord::Migration[5.0]
  def change
    add_reference :instances, :availability, foreign_key: true
  end
end
