# frozen_string_literal: true

class AddCalendlyLinkToUser < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :calendly_link, :string
  end
end
