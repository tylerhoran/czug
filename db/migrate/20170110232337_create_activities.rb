# frozen_string_literal: true

class CreateActivities < ActiveRecord::Migration[5.0]
  def change
    create_table :activities do |t|
      t.references :subject, polymorphic: true
      t.string :name
      t.string :direction
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end
