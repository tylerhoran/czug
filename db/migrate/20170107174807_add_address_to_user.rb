# frozen_string_literal: true

class AddAddressToUser < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :street_address, :string
    add_column :users, :city, :string
    add_column :users, :region, :string
    add_column :users, :postcode, :string
    add_column :users, :country, :string
  end
end
