# frozen_string_literal: true

class AddSlugToInstance < ActiveRecord::Migration[5.0]
  def change
    add_column :instances, :slug, :string
    add_index :instances, :slug, unique: true
  end
end
