# frozen_string_literal: true

class AddStatusToCard < ActiveRecord::Migration[5.0]
  def change
    add_column :cards, :status, :integer, default: 0
  end
end
