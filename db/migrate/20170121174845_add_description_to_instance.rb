# frozen_string_literal: true

class AddDescriptionToInstance < ActiveRecord::Migration[5.0]
  def change
    add_column :instances, :description, :text
  end
end
