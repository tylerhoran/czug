# frozen_string_literal: true

class CreateSchools < ActiveRecord::Migration[5.0]
  def change
    create_table :schools do |t|
      t.references :user, foreign_key: true
      t.string :name
      t.string :fb_id
      t.string :school_type
      t.string :degree

      t.timestamps
    end
  end
end
