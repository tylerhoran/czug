# frozen_string_literal: true

class AddGoogleIdToMeeting < ActiveRecord::Migration[5.0]
  def change
    add_column :meetings, :google_id, :string
  end
end
