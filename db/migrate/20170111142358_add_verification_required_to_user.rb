# frozen_string_literal: true

class AddVerificationRequiredToUser < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :verification_required, :boolean, default: false
  end
end
