# frozen_string_literal: true

class CreateAssignments < ActiveRecord::Migration[5.0]
  def change
    create_table :assignments do |t|
      t.references :instance, foreign_key: true
      t.string :name
      t.text :body
      t.attachment :attachment

      t.timestamps
    end
  end
end
