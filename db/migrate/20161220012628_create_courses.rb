# frozen_string_literal: true

class CreateCourses < ActiveRecord::Migration[5.0]
  def change
    create_table :courses do |t|
      t.string :name
      t.references :degree, foreign_key: true
      t.string :slug
      t.integer :status, default: 0
      t.integer :proposer_id
      t.timestamps
    end
    add_index :courses, :slug, unique: true
  end
end
