# frozen_string_literal: true

class CreateCharges < ActiveRecord::Migration[5.0]
  def change
    create_table :charges do |t|
      t.references :card, foreign_key: true
      t.references :instance, foreign_key: true
      t.integer :amount
      t.string :stripe_charge_token

      t.timestamps
    end
  end
end
