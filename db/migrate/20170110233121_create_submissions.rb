# frozen_string_literal: true

class CreateSubmissions < ActiveRecord::Migration[5.0]
  def change
    create_table :submissions do |t|
      t.references :assignment, foreign_key: true
      t.references :user, foreign_key: true
      t.attachment :attachment
      t.text :body

      t.timestamps
    end
  end
end
