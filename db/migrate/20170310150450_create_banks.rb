# frozen_string_literal: true

class CreateBanks < ActiveRecord::Migration[5.0]
  def change
    create_table :banks do |t|
      t.references :user, foreign_key: true
      t.string :name
      t.string :country
      t.string :token

      t.timestamps
    end
  end
end
