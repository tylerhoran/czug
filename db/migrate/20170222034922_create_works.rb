# frozen_string_literal: true

class CreateWorks < ActiveRecord::Migration[5.0]
  def change
    create_table :works do |t|
      t.references :user, foreign_key: true
      t.string :name
      t.string :position
      t.string :fb_id

      t.timestamps
    end
  end
end
