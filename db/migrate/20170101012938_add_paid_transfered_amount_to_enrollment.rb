# frozen_string_literal: true

class AddPaidTransferedAmountToEnrollment < ActiveRecord::Migration[5.0]
  def change
    add_column :enrollments, :transfer_amount, :integer
  end
end
