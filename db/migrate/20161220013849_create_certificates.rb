# frozen_string_literal: true

class CreateCertificates < ActiveRecord::Migration[5.0]
  def change
    create_table :certificates do |t|
      t.references :user, foreign_key: true
      t.references :degree, foreign_key: true
      t.references :petition, foreign_key: true
      t.timestamps
    end
  end
end
