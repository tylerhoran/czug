# frozen_string_literal: true

class CreatePosts < ActiveRecord::Migration[5.0]
  def change
    create_table :posts do |t|
      t.references :user, foreign_key: true
      t.references :instance, foreign_key: true
      t.text :body
      t.attachment :attachment

      t.timestamps
    end
  end
end
