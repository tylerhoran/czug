# frozen_string_literal: true

class CreateRefunds < ActiveRecord::Migration[5.0]
  def change
    create_table :refunds do |t|
      t.references :user, foreign_key: true
      t.references :charge, foreign_key: true
      t.integer :amount
      t.string :stripe_refund_token

      t.timestamps
    end
  end
end
