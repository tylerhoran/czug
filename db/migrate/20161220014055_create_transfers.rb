# frozen_string_literal: true

class CreateTransfers < ActiveRecord::Migration[5.0]
  def change
    create_table :transfers do |t|
      t.references :card, foreign_key: true
      t.integer :amount
      t.string :stripe_transfer_token

      t.timestamps
    end
  end
end
