# frozen_string_literal: true

class CreateSchedules < ActiveRecord::Migration[5.0]
  def change
    create_table :schedules do |t|
      t.references :instance, foreign_key: true
      t.date :start_date
      t.string :day_1
      t.time :time_1
      t.string :day_2
      t.time :time_2
      t.string :day_3
      t.time :time_3

      t.timestamps
    end
  end
end
