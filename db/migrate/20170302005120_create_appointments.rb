# frozen_string_literal: true

class CreateAppointments < ActiveRecord::Migration[5.0]
  def change
    create_table :appointments do |t|
      t.references :user, foreign_key: true
      t.integer :attendee_id
      t.datetime :time

      t.timestamps
    end
  end
end
