# frozen_string_literal: true

class CreateDivisions < ActiveRecord::Migration[5.0]
  def change
    create_table :divisions do |t|
      t.string :name
      t.string :slug
      t.timestamps
    end
    add_index :divisions, :slug, unique: true
  end
end
