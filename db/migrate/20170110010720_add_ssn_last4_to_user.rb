# frozen_string_literal: true

class AddSsnLast4ToUser < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :ssn_last_4, :string
  end
end
