# frozen_string_literal: true

class CreateInstances < ActiveRecord::Migration[5.0]
  def change
    create_table :instances do |t|
      t.references :user, foreign_key: true
      t.references :course, foreign_key: true
      t.attachment :syllabus

      t.timestamps
    end
  end
end
