# frozen_string_literal: true

class CreateEvaluations < ActiveRecord::Migration[5.0]
  def change
    create_table :evaluations do |t|
      t.references :enrollment, foreign_key: true
      t.integer :rating

      t.timestamps
    end
  end
end
