# frozen_string_literal: true

class AddFinishTokenToUser < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :finish_token, :string
  end
end
