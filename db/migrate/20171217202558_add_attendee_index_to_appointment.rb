class AddAttendeeIndexToAppointment < ActiveRecord::Migration[5.0]
  def change
    add_index :appointments, :attendee_id
  end
end
