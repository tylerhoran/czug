# frozen_string_literal: true

class CreateMeetings < ActiveRecord::Migration[5.0]
  def change
    create_table :meetings do |t|
      t.references :schedule, foreign_key: true
      t.datetime :occurrence

      t.timestamps
    end
  end
end
