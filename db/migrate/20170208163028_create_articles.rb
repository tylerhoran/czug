# frozen_string_literal: true

class CreateArticles < ActiveRecord::Migration[5.0]
  def change
    create_table :articles do |t|
      t.string :name
      t.text :body
      t.attachment :attachment

      t.timestamps
    end
  end
end
