# frozen_string_literal: true

class AddHangoutLinkToAppointment < ActiveRecord::Migration[5.0]
  def change
    add_column :appointments, :google_id, :string
    add_column :appointments, :hangout_link, :string
  end
end
