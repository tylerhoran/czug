# frozen_string_literal: true

class RemoveEnrollableFromEnrollment < ActiveRecord::Migration[5.0]
  def change
    remove_column :enrollments, :enrollable_id, :integer
    remove_column :enrollments, :enrollable_type, :integer
  end
end
