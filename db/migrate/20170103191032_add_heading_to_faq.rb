# frozen_string_literal: true

class AddHeadingToFaq < ActiveRecord::Migration[5.0]
  def change
    add_column :faqs, :heading, :string
  end
end
