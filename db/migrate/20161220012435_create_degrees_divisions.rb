# frozen_string_literal: true

class CreateDegreesDivisions < ActiveRecord::Migration[5.0]
  def change
    create_table :degrees_divisions, id: false do |t|
      t.references :degree
      t.references :division
    end
  end
end
