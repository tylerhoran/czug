# frozen_string_literal: true

class AddInstanceToEnrollment < ActiveRecord::Migration[5.0]
  def change
    add_reference :enrollments, :instance, foreign_key: true
  end
end
