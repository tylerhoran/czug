# frozen_string_literal: true

class CreatePetitions < ActiveRecord::Migration[5.0]
  def change
    create_table :petitions do |t|
      t.references :user, foreign_key: true
      t.references :degree, foreign_key: true
      t.integer :status

      t.timestamps
    end
  end
end
