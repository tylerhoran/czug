# frozen_string_literal: true

class AddProposerIndexToCourses < ActiveRecord::Migration[5.0]
  def change
    add_index(:courses, :proposer_id)
    add_index(:users, :taught_degree_id)
  end
end
