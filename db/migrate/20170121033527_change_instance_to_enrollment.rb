# frozen_string_literal: true

class ChangeInstanceToEnrollment < ActiveRecord::Migration[5.0]
  def change
    remove_column :grades, :instance_id, :integer
    add_column :grades, :enrollment_id, :integer
  end
end
