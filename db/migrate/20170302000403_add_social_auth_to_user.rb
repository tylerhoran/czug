# frozen_string_literal: true

class AddSocialAuthToUser < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :social_auth, :boolean, default: false
  end
end
