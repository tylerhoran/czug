# frozen_string_literal: true

class CreateAuthenticationProviders < ActiveRecord::Migration
  def change
    create_table 'providers', force: true do |t|
      t.string   'name'
      t.datetime 'created_at',                 null: false
      t.datetime 'updated_at',                 null: false
    end
    add_index 'providers', ['name'], name: 'index_name_on_providers'
  end
end
