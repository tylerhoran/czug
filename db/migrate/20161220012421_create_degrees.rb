# frozen_string_literal: true

class CreateDegrees < ActiveRecord::Migration[5.0]
  def change
    create_table :degrees do |t|
      t.string :name
      t.text :description
      t.integer :degree_type
      t.string :slug
      t.timestamps
    end
    add_index :degrees, :slug, unique: true
  end
end
