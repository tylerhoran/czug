# frozen_string_literal: true

class AddTaughtDegreeToUser < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :taught_degree_id, :integer
  end
end
