# frozen_string_literal: true

class AddStatusToInstance < ActiveRecord::Migration[5.0]
  def change
    add_column :instances, :status, :integer
  end
end
