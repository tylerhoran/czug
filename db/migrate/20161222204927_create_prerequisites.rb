# frozen_string_literal: true

class CreatePrerequisites < ActiveRecord::Migration[5.0]
  def change
    create_table :prerequisites do |t|
      t.references :course, foreign_key: true
      t.references :prior, references: :course
      t.timestamps
    end
  end
end
