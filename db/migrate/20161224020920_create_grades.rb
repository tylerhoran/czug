# frozen_string_literal: true

class CreateGrades < ActiveRecord::Migration[5.0]
  def change
    create_table :grades do |t|
      t.references :user, foreign_key: true
      t.references :instance, foreign_key: true
      t.integer :percent

      t.timestamps
    end
  end
end
