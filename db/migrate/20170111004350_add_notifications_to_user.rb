# frozen_string_literal: true

class AddNotificationsToUser < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :activity_notifications, :boolean, default: true
    add_column :users, :messages_notifications, :boolean, default: true
    add_column :users, :newsletter_notifications, :boolean, default: true
  end
end
