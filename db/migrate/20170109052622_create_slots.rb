# frozen_string_literal: true

class CreateSlots < ActiveRecord::Migration[5.0]
  def change
    create_table :slots do |t|
      t.time :start_time
      t.time :end_time
      t.integer :day
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end
