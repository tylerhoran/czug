# frozen_string_literal: true

class AddDescriptionToDivision < ActiveRecord::Migration[5.0]
  def change
    add_column :divisions, :description, :text
  end
end
