# frozen_string_literal: true

class AddCloseActiveToUser < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :close_active, :boolean, default: false
  end
end
