# frozen_string_literal: true

class AddHangoutLinkToMeeting < ActiveRecord::Migration[5.0]
  def change
    add_column :meetings, :hangout_link, :string
  end
end
