# frozen_string_literal: true

class CreateReservations < ActiveRecord::Migration[5.0]
  def change
    create_table :reservations do |t|
      t.references :user, foreign_key: true
      t.references :availability, foreign_key: true

      t.timestamps
    end
  end
end
