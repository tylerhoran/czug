# frozen_string_literal: true

class RemoveCertificates < ActiveRecord::Migration[5.0]
  def change
    drop_table :certificates
  end
end
