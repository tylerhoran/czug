# frozen_string_literal: true

class AddGradeToEnrollment < ActiveRecord::Migration[5.0]
  def change
    add_column :enrollments, :grade, :integer
  end
end
