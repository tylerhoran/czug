# frozen_string_literal: true

class AddNoteToPetition < ActiveRecord::Migration[5.0]
  def change
    add_column :petitions, :note, :text
  end
end
