# frozen_string_literal: true

class CanInterviewToUser < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :can_interview, :boolean, default: false
  end
end
