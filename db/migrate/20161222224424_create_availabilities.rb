# frozen_string_literal: true

class CreateAvailabilities < ActiveRecord::Migration[5.0]
  def change
    create_table :availabilities do |t|
      t.references :user, foreign_key: true
      t.references :course, foreign_key: true
      t.text :description
      t.timestamps
    end
  end
end
