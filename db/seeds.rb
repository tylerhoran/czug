# frozen_string_literal: true

# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
load('db/seeds/providers.rb')
load('db/seeds/divisions.rb')
load('db/seeds/degrees.rb')
load('db/seeds/courses.rb')
load('db/seeds/prerequisites.rb')
load('db/seeds/faqs.rb')
load('db/seeds/admin.rb')
load('db/seeds/articles.rb')
