# frozen_string_literal: true

degree = Degree.find_by(name: 'Psychology')
Course.create!(seed: true, name: 'Intro to Psychology', degree: degree)
Course.create!(seed: true, name: 'Clinical Psychology', degree: degree)
Course.create!(seed: true, name: 'Developmental Psychology', degree: degree)
Course.create!(seed: true, name: 'Sensory Neuroscience', degree: degree)
Course.create!(seed: true, name: 'Social Psychology', degree: degree)
Course.create!(seed: true, name: 'Cognitive Psychology', degree: degree)
Course.create!(seed: true, name: 'Behavioral Neuroscience', degree: degree)
Course.create!(seed: true, name: 'Cognitive Neuroscience', degree: degree)
Course.create!(seed: true, name: 'Junior Honors Research Seminar', degree: degree)
Course.create!(seed: true, name: 'Learning and Motivation', degree: degree)
Course.create!(seed: true, name: 'Laboratory in Operant Psychology', degree: degree)
Course.create!(seed: true, name: 'Mechanisms of Animal Behavior', degree: degree)
Course.create!(seed: true, name: 'Cognitive Control and Frontal Lobe Function', degree: degree)
Course.create!(seed: true, name: 'Clinical Assessment and Treatment', degree: degree)
Course.create!(seed: true, name: 'Clinical Neuropsychology', degree: degree)
Course.create!(seed: true, name: 'Psychology of Reading', degree: degree)
Course.create!(seed: true, name: 'The Logic of Perception', degree: degree)
Course.create!(seed: true, name: 'Delay of Gratification', degree: degree)
Course.create!(seed: true, name: 'Hormones and Behavior', degree: degree)
Course.create!(seed: true, name: 'Circadian Rhythms — Biological Clock', degree: degree)
Course.create!(seed: true, name: 'Eating Disorders', degree: degree)
Course.create!(seed: true, name: 'Cognitive Development', degree: degree)
Course.create!(seed: true, name: 'Social Cognition', degree: degree)
Course.create!(seed: true, name: 'Sound and Music Perception', degree: degree)
Course.create!(seed: true, name: 'Social Psychology of Sports', degree: degree)
Course.create!(seed: true, name: 'Human Behavior Laboratory', degree: degree)
Course.create!(seed: true, name: 'Evolution and Human Nature', degree: degree)
Course.create!(seed: true, name: 'Psychology of Consciousness', degree: degree)
Course.create!(seed: true, name: 'Control and Analysis of Human Behavior', degree: degree)
Course.create!(seed: true, name: 'Memory and Amnesia', degree: degree)
Course.create!(seed: true, name: 'Psychology of Language', degree: degree)
Course.create!(seed: true, name: 'Language & Conceptual Development', degree: degree)
Course.create!(seed: true, name: 'Psychology of Judgment and Decision', degree: degree)
Course.create!(seed: true, name: 'Cognitive Neuroscience of Vision', degree: degree)
Course.create!(seed: true, name: 'Tests and Measurement', degree: degree)
Course.create!(seed: true, name: 'Conceptions of Intelligence', degree: degree)
Course.create!(seed: true, name: 'Psychology of Emotion', degree: degree)
Course.create!(seed: true, name: 'Behavior Modification', degree: degree)
Course.create!(seed: true, name: 'Social Psychology and Medicine', degree: degree)
Course.create!(seed: true, name: 'Cognitive Development in Infancy', degree: degree)
Course.create!(seed: true, name: 'Happiness', degree: degree)
Course.create!(seed: true, name: 'Interpersonal Relationships', degree: degree)
Course.create!(seed: true, name: 'Physiological Basis of Perception', degree: degree)
Course.create!(seed: true, name: 'Engineering Psychology', degree: degree)
Course.create!(seed: true, name: 'Psychology and the Law', degree: degree)
Course.create!(seed: true, name: 'Criminology', degree: degree)
Course.create!(seed: true, name: 'History of Psychology', degree: degree)
Course.create!(seed: true, name: 'Psychological Disorders of Childhood', degree: degree)
Course.create!(seed: true, name: 'Brain Damage and Mental Functions', degree: degree)
Course.create!(seed: true, name: 'Cognitive Neuropsychology', degree: degree)
Course.create!(seed: true, name: 'Neurobiology of Learning and Memory', degree: degree)
Course.create!(seed: true, name: 'The Psychology of Human Sexuality', degree: degree)
Course.create!(seed: true, name: 'Psychology of Food and Behavior', degree: degree)
Course.create!(seed: true, name: 'Creativity', degree: degree)
Course.create!(seed: true, name: 'Industrial Organizational Psychology', degree: degree)
Course.create!(seed: true, name: 'Drugs, Addiction, and Mental Disorders', degree: degree)
Course.create!(seed: true, name: 'Adolescence', degree: degree)
Course.create!(seed: true, name: 'Drugs and Behavior', degree: degree)
Course.create!(seed: true, name: 'Illusions and the Brain', degree: degree)
Course.create!(seed: true, name: 'Choice and Self-Control', degree: degree)
Course.create!(seed: true, name: 'Development of Social Cognition', degree: degree)
Course.create!(seed: true, name: 'Impulse Control Disorders', degree: degree)
Course.create!(seed: true, name: 'Brain, Behavior, and Evolution', degree: degree)
Course.create!(seed: true, name: 'Parenting', degree: degree)
Course.create!(seed: true, name: 'Psychology of Sleep', degree: degree)
Course.create!(seed: true, name: 'Research Methods in Psychology', degree: degree)
