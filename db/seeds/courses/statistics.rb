# frozen_string_literal: true

degree = Degree.find_by(name: 'Statistics')
Course.create!(seed: true, name: 'Introductory Business Statistics', degree: degree)
Course.create!(seed: true, name: 'Introductory Business Statistics', degree: degree)
Course.create!(seed: true, name: 'Introductory Statistics', degree: degree)
Course.create!(seed: true, name: 'Introductory Statistics', degree: degree)
Course.create!(seed: true, name: 'Statistical Computing With R', degree: degree)
Course.create!(seed: true, name: 'Predictive Analytics for Business', degree: degree)
Course.create!(seed: true, name: 'Probability', degree: degree)
Course.create!(seed: true, name: 'Statistical Inference', degree: degree)
Course.create!(seed: true, name: 'Mathematical Statistics', degree: degree)
Course.create!(seed: true, name: 'Stochastic Processes', degree: degree)
Course.create!(seed: true, name: 'Forecasting Methods For Management', degree: degree)
Course.create!(seed: true, name: 'Fundamentals Of Actuarial Science I', degree: degree)
Course.create!(seed: true, name: 'Fundamentals of Actuarial Science Ii', degree: degree)
Course.create!(seed: true, name: 'Actuarial Statistics', degree: degree)
Course.create!(seed: true, name: 'Data Analytics And Statistical Computing', degree: degree)
Course.create!(seed: true, name: 'Modern Data Mining', degree: degree)
Course.create!(seed: true, name: 'Modern Regression For the Social, Behavioral And Biological Sciences', degree: degree)
Course.create!(seed: true, name: 'Applied Probability Models In Marketing', degree: degree)
Course.create!(seed: true, name: 'Applied Regression And Analysis Of Variance', degree: degree)
Course.create!(seed: true, name: 'Introduction to Nonparametric Methods And Log-Linear Models', degree: degree)
Course.create!(seed: true, name: 'Data Analytics And Statistical Computing', degree: degree)
Course.create!(seed: true, name: 'Probability', degree: degree)
Course.create!(seed: true, name: 'Statistical Inference', degree: degree)
Course.create!(seed: true, name: 'Mathematical Statistics', degree: degree)
Course.create!(seed: true, name: 'Applied Econometrics I', degree: degree)
Course.create!(seed: true, name: 'Stochastic Processes', degree: degree)
Course.create!(seed: true, name: 'Modern Data Mining', degree: degree)
