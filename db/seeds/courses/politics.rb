# frozen_string_literal: true

degree = Degree.find_by(name: 'Politics')
Course.create!(seed: true, name: 'Introduction to Political Theory', degree: degree)
Course.create!(seed: true, name: 'Introduction to Comparative Politics', degree: degree)
Course.create!(seed: true, name: 'Introduction to Nonwestern Politics', degree: degree)
Course.create!(seed: true, name: 'Introduction to International Relations', degree: degree)
Course.create!(seed: true, name: 'Introduction to American Politics', degree: degree)
Course.create!(seed: true, name: 'Privacy in the Information Age', degree: degree)
Course.create!(seed: true, name: 'Quantitative Analysis in Political Science', degree: degree)
Course.create!(seed: true, name: 'Political Institutions and Behavior in Western Europe', degree: degree)
Course.create!(seed: true, name: 'American Political Parties', degree: degree)
Course.create!(seed: true, name: 'State and Local Government', degree: degree)
Course.create!(seed: true, name: 'Political Issues', degree: degree)
Course.create!(seed: true, name: 'Classical and Medieval Political Theory', degree: degree)
Course.create!(seed: true, name: 'Modern Political Theory', degree: degree)
Course.create!(seed: true, name: 'Contemporary Political Theory', degree: degree)
Course.create!(seed: true, name: 'Western Marxist Tradition', degree: degree)
Course.create!(seed: true, name: 'Politics and Literature', degree: degree)
Course.create!(seed: true, name: 'American Political Thought and Ideology', degree: degree)
Course.create!(seed: true, name: 'Theories of Human Rights', degree: degree)
Course.create!(seed: true, name: 'Democratic Theory', degree: degree)
Course.create!(seed: true, name: 'Political Protest and Ideology', degree: degree)
Course.create!(seed: true, name: 'Critical Race Theory as Political Theory', degree: degree)
Course.create!(seed: true, name: 'Comparative Political Parties and Electoral Systems', degree: degree)
Course.create!(seed: true, name: 'Environmental Policy and Institutions', degree: degree)
Course.create!(seed: true, name: 'Voting Behavior and Public Opinion Around the World', degree: degree)
Course.create!(seed: true, name: 'Comparative Political Economy', degree: degree)
Course.create!(seed: true, name: 'Politics of Oil', degree: degree)
Course.create!(seed: true, name: 'Sustainable Energy in the 21st Century', degree: degree)
Course.create!(seed: true, name: 'Ethnic Conflict and Democracy in Comparative Perspective', degree: degree)
Course.create!(seed: true, name: 'Politics of Water', degree: degree)
Course.create!(seed: true, name: 'Comparative Perspectives on Human Rights', degree: degree)
Course.create!(seed: true, name: 'Comparative Social Policy', degree: degree)
Course.create!(seed: true, name: 'Women in Political Development', degree: degree)
Course.create!(seed: true, name: "Indigenous Peoples' Politics and Rights", degree: degree)
Course.create!(seed: true, name: 'Politics of Russia and the Former Soviet Union', degree: degree)
Course.create!(seed: true, name: 'Latin American Politics', degree: degree)
Course.create!(seed: true, name: 'Democratic Culture and Citizenship in Latin America', degree: degree)
Course.create!(seed: true, name: 'Politics of the Environment and Development', degree: degree)
Course.create!(seed: true, name: 'Chinese Politics and Economy', degree: degree)
Course.create!(seed: true, name: 'Gender and War', degree: degree)
Course.create!(seed: true, name: 'Gender Politics and Islam', degree: degree)
Course.create!(seed: true, name: 'The Political Economy of East Asia', degree: degree)
Course.create!(seed: true, name: 'Politics in Africa', degree: degree)
Course.create!(seed: true, name: 'Politics of South Africa', degree: degree)
Course.create!(seed: true, name: 'Politics and Human Rights in Global Supply Chains', degree: degree)
Course.create!(seed: true, name: 'Contemporary International Politics', degree: degree)
Course.create!(seed: true, name: 'Globalization and Political Change', degree: degree)
Course.create!(seed: true, name: 'International Political Economy', degree: degree)
Course.create!(seed: true, name: 'Global Environmental Politics', degree: degree)
Course.create!(seed: true, name: 'National and International Security', degree: degree)
Course.create!(seed: true, name: 'International Organizations and Law', degree: degree)
Course.create!(seed: true, name: 'International Negotiation and Bargaining', degree: degree)
Course.create!(seed: true, name: 'Politics, Propaganda, and Cinema', degree: degree)
Course.create!(seed: true, name: 'The Politics of Torture', degree: degree)
Course.create!(seed: true, name: 'Political Violence', degree: degree)
Course.create!(seed: true, name: 'Evaluating Human Rights Practices of Countries', degree: degree)
Course.create!(seed: true, name: 'American Diplomacy', degree: degree)
Course.create!(seed: true, name: 'Recent American Diplomacy', degree: degree)
Course.create!(seed: true, name: 'Writing Seminar in Recent American Diplomacy', degree: degree)
Course.create!(seed: true, name: 'The Politics of American Foreign Policy', degree: degree)
Course.create!(seed: true, name: 'American Diplomacy in the Middle East', degree: degree)
Course.create!(seed: true, name: 'Foreign Policies of the Russian Federation and the Former USSR', degree: degree)
Course.create!(seed: true, name: 'International Relations of the Middle East', degree: degree)
Course.create!(seed: true, name: 'Arab-Israeli Conflict', degree: degree)
Course.create!(seed: true, name: 'South Asia in World Politics', degree: degree)
Course.create!(seed: true, name: 'World Political Leaders', degree: degree)
Course.create!(seed: true, name: 'The Presidency and Congress', degree: degree)
Course.create!(seed: true, name: 'Congress in Theory and Practice', degree: degree)
Course.create!(seed: true, name: 'Electoral Behavior', degree: degree)
Course.create!(seed: true, name: 'Congressional Elections', degree: degree)
Course.create!(seed: true, name: 'Electoral Realignment', degree: degree)
Course.create!(seed: true, name: 'American Political Economy', degree: degree)
Course.create!(seed: true, name: 'Politics of Inequality', degree: degree)
Course.create!(seed: true, name: 'American Political Leadership', degree: degree)
Course.create!(seed: true, name: 'Public Opinion', degree: degree)
Course.create!(seed: true, name: 'Connecticut State and Municipal Politics', degree: degree)
Course.create!(seed: true, name: 'Urban Politics', degree: degree)
Course.create!(seed: true, name: 'Race and Policy', degree: degree)
Course.create!(seed: true, name: 'African-American Politics', degree: degree)
Course.create!(seed: true, name: 'Black Leadership and Civil Rights', degree: degree)
Course.create!(seed: true, name: 'Black Feminist Politics', degree: degree)
Course.create!(seed: true, name: 'Latino Political Behavior', degree: degree)
Course.create!(seed: true, name: 'Puerto Rican Politics and Culture', degree: degree)
Course.create!(seed: true, name: 'Women and Politics', degree: degree)
Course.create!(seed: true, name: 'Constitutional Law', degree: degree)
Course.create!(seed: true, name: 'Constitutional Rights and Liberties', degree: degree)
Course.create!(seed: true, name: 'Judiciary in the Political Process', degree: degree)
Course.create!(seed: true, name: 'Law and Society', degree: degree)
Course.create!(seed: true, name: 'Law and Popular Culture', degree: degree)
Course.create!(seed: true, name: 'Politics of Crime and Justice', degree: degree)
Course.create!(seed: true, name: 'Maritime Law', degree: degree)
Course.create!(seed: true, name: 'Immigration and Transborder Politics', degree: degree)
Course.create!(seed: true, name: 'Civil Rights and Legal Mobilization', degree: degree)
Course.create!(seed: true, name: 'Public Administration', degree: degree)
Course.create!(seed: true, name: 'The Policy-making Process', degree: degree)
Course.create!(seed: true, name: 'Politics and Ethics', degree: degree)
Course.create!(seed: true, name: 'Politics of Budgeting', degree: degree)
Course.create!(seed: true, name: 'Politics, Society, and Education Policy', degree: degree)
