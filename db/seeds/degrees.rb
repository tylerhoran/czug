# frozen_string_literal: true

science = Division.find_by(name: 'Science and Mathematics')
language = Division.find_by(name: 'Languages and the Arts')
social = Division.find_by(name: 'Social Studies')
d = Degree.create!(name: 'Anthropology', degree_type: 'ba', description: 'Anthropology is the study of various aspects of humans within past and present societies. Social anthropology and cultural anthropology study the norms and values of societies. Linguistic anthropology studies how language affects social life. Biological or physical anthropology studies the biological development of humans.')
social.degrees << d
unless ENV['REVIEW_APP']
  d = Degree.create!(name: 'Astronomy', degree_type: 'bs', description: 'Astronomy is a natural science that studies celestial objects and phenomena. It applies mathematics, physics, and chemistry, in an effort to explain the origin of those objects and phenomena and their evolution')
  science.degrees << d
  d = Degree.create!(name: 'Art History', degree_type: 'ba', description: 'Art history is the study of objects of art in their historical development and stylistic contexts, i.e. genre, design, format, and style. This includes the "major" arts of painting, sculpture, and architecture as well as the "minor" arts of ceramics, furniture, and other decorative objects.')
  language.degrees << d
  d = Degree.create!(name: 'Business', degree_type: 'bs', description: 'Business administration is a wide field that incorporates many types of management positions. From major corporations to independent businesses, every operation needs skilled administrators in order to succeed. Motivated, organized personalities will thrive in business, where environments are often high-powered.')
  social.degrees << d
  science.degrees << d
  d = Degree.create!(name: 'Classics', degree_type: 'ba', description: 'Classics or classical studies is the study of classical antiquity. This covers the ancient Mediterranean world, particularly ancient Greece and Rome. It encompasses the study of ancient languages, literature, philosophy, history, and archaeology.')
  language.degrees << d
  d = Degree.create!(name: 'Computer Science', degree_type: 'bs', description: 'Computer science is the study of the theory, experimentation, and engineering that form the basis for the design and use of computers.')
  science.degrees << d
  d = Degree.create!(name: 'Economics', degree_type: 'bs', description: 'Economics is a social science concerned with the factors that determine the production, distribution, and consumption of goods and services.')
  social.degrees << d
  science.degrees << d
  d = Degree.create!(name: 'English', degree_type: 'ba', description: 'English is the study of the english language, writing and culture from it\'s roots to it\'s various manifestations across the globe.')
  language.degrees << d
  d = Degree.create!(name: 'Geography', degree_type: 'ba', description: 'Geography (from Greek γεωγραφία, geographia, literally "earth description") is a field of science devoted to the study of the lands, the features, the inhabitants, and the phenomena of Earth.')
  science.degrees << d
  d = Degree.create!(name: 'History', degree_type: 'ba', description: 'History (from Greek ἱστορία, historia, meaning "inquiry, knowledge acquired by investigation") is the study of the past as it is described in written documents')
  social.degrees << d
  d = Degree.create!(name: 'Linguistics', degree_type: 'ba', description: 'Linguistics is the scientific study of language, specifically of language form, language meaning, and language in context.')
  language.degrees << d
  d = Degree.create!(name: 'Mathematics', degree_type: 'bs', description: 'Mathematics (from Greek μάθημα máthēma, "knowledge, study, learning") is the study of topics such as quantity (numbers), structure, space, and change')
  science.degrees << d
  d = Degree.create!(name: 'Philosophy', degree_type: 'ba', description: 'Philosophy (from Greek φιλοσοφία, philosophia, literally "love of wisdom") is the study of general and fundamental problems concerning matters such as existence, knowledge, values, reason, mind, and language.')
  social.degrees << d
  d = Degree.create!(name: 'Politics', degree_type: 'bs', description: 'Politics is a social science discipline that deals with systems of lgovernment, and the analysis of political activities, political thoughts and political behaviour.')
  social.degrees << d
  science.degrees << d
  d = Degree.create!(name: 'Psychology', degree_type: 'bs', description: 'Psychology is the study of behavior and mind, embracing all aspects of conscious and unconscious experience as well as thought.')
  social.degrees << d
  science.degrees << d
  d = Degree.create!(name: 'Religion', degree_type: 'ba', description: 'Religious studies, alternately known as the study of religion, is the multi-disciplinary academic field devoted to research into religious beliefs, behaviors, and institutions. It describes, compares, interprets, and explains religion, emphasizing systematic, historically based, and cross-cultural perspectives.')
  social.degrees << d
  d = Degree.create!(name: 'Sociology', degree_type: 'ba', description: 'Sociology is the study of social behaviour or society, including its origins, development, organization, networks, and institutions.')
  social.degrees << d
  science.degrees << d
  d = Degree.create!(name: 'Statistics', degree_type: 'bs', description: 'Statistics is the study of the collection, analysis, interpretation, presentation, and organization of data. In applying statistics to, e.g., a scientific, industrial, or social problem, it is conventional to begin with a statistical population or a statistical model process to be studied.')
  science.degrees << d
end
