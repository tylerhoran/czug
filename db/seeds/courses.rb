# frozen_string_literal: true

load('db/seeds/courses/anthropology.rb')
unless ENV['REVIEW_APP']
  load('db/seeds/courses/art_history.rb')
  load('db/seeds/courses/astronomy.rb')
  load('db/seeds/courses/business.rb')
  load('db/seeds/courses/classics.rb')
  load('db/seeds/courses/computer_science.rb')
  load('db/seeds/courses/economics.rb')
  load('db/seeds/courses/english.rb')
  load('db/seeds/courses/geography.rb')
  load('db/seeds/courses/history.rb')
  load('db/seeds/courses/linguistics.rb')
  load('db/seeds/courses/mathematics.rb')
  load('db/seeds/courses/philosophy.rb')
  load('db/seeds/courses/politics.rb')
  load('db/seeds/courses/psychology.rb')
  load('db/seeds/courses/religion.rb')
  load('db/seeds/courses/sociology.rb')
  load('db/seeds/courses/statistics.rb')
end
Course.update_all(status: 'approved')
