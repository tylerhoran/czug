# frozen_string_literal: true

# Anthropology
anthro = Degree.find_by(name: 'Anthropology')
intro = anthro.courses.find_by(name: 'Introduction to Social/Cultural Anthropology')
foundational = anthro.courses.where(name: ['Survey of Anthropological Research', 'History of Anthropological Thought', 'Language, Culture, and Society'])
anthro.courses.each do |course|
  Prerequisite.create!(course: course, prior: intro) unless course == intro
  foundational.map { |foundation| Prerequisite.create!(course: course, prior: foundation) } unless foundational.include?(course) || intro == course
end

unless ENV['REVIEW_APP']
  # Art History
  art = Degree.find_by(name: 'Art History')
  intro = art.courses.find_by(name: 'Introduction to Western Art')
  foundational = art.courses.where(name: [])
  art.courses.each do |course|
    Prerequisite.create!(course: course, prior: intro) unless course == intro
    foundational.map { |foundation| Prerequisite.create!(course: course, prior: foundation) } unless foundational.include?(course) || intro == course
  end

  # Astronomy
  astronomy = Degree.find_by(name: 'Astronomy')
  intro = astronomy.courses.find_by(name: 'Introduction to Astronomy')
  foundational = astronomy.courses.where(name: [])
  astronomy.courses.each do |course|
    Prerequisite.create!(course: course, prior: intro) unless course == intro
    foundational.map { |foundation| Prerequisite.create!(course: course, prior: foundation) } unless foundational.include?(course) || intro == course
  end

  # Business
  business = Degree.find_by(name: 'Business')
  intro = business.courses.find_by(name: 'Introduction to Business Management')
  foundational = business.courses.where(name: ['Introduction to Accounting', 'Introduction to Entrepreneurship', 'Business, Society and Ethics', 'Marketing', 'Principles of Economics'])
  business.courses.each do |course|
    Prerequisite.create!(course: course, prior: intro) unless course == intro
    foundational.map { |foundation| Prerequisite.create!(course: course, prior: foundation) } unless foundational.include?(course) || intro == course
  end

  # Classics
  classics = Degree.find_by(name: 'Classics')
  intro = classics.courses.find_by(name: 'Introduction to Greek Civilization')
  foundational = classics.courses.where(name: ['Introduction to Roman Civilization', 'Greek Drama', 'Greek Philosophy', 'The Classic Myths'])
  classics.courses.each do |course|
    Prerequisite.create!(course: course, prior: intro) unless course == intro
    foundational.map { |foundation| Prerequisite.create!(course: course, prior: foundation) } unless foundational.include?(course) || intro == course
  end

  # Computer Science
  computers = Degree.find_by(name: 'Computer Science')
  intro = computers.courses.find_by(name: 'Computer Science 1: Starting Computing')
  foundational = computers.courses.where(name: ['Computer Science 2: Data Structures', 'Algorithms'])
  computers.courses.each do |course|
    Prerequisite.create!(course: course, prior: intro) unless course == intro
    foundational.map { |foundation| Prerequisite.create!(course: course, prior: foundation) } unless foundational.include?(course) || intro == course
  end

  # Economics
  economics = Degree.find_by(name: 'Economics')
  intro = economics.courses.find_by(name: 'Intro to Economics')
  foundational = economics.courses.where(name: ['Behavioral Economics', 'Econometrics', 'Introduction to Financial Investments'])
  economics.courses.each do |course|
    Prerequisite.create!(course: course, prior: intro) unless course == intro
    foundational.map { |foundation| Prerequisite.create!(course: course, prior: foundation) } unless foundational.include?(course) || intro == course
  end

  # English
  english = Degree.find_by(name: 'English')
  intro = english.courses.find_by(name: 'Academic Writing')
  foundational = english.courses.where(name: ['Having Arguments', 'Introduction to English Literature: 14th to 18th Century'])
  english.courses.each do |course|
    Prerequisite.create!(course: course, prior: intro) unless course == intro
    foundational.map { |foundation| Prerequisite.create!(course: course, prior: foundation) } unless foundational.include?(course) || intro == course
  end

  # Geography
  geography = Degree.find_by(name: 'Geography')
  intro = geography.courses.find_by(name: 'Human Geography')
  foundational = geography.courses.where(name: ["Earth's Physical Environment", 'Environment And Development'])
  geography.courses.each do |course|
    Prerequisite.create!(course: course, prior: intro) unless course == intro
    foundational.map { |foundation| Prerequisite.create!(course: course, prior: foundation) } unless foundational.include?(course) || intro == course
  end

  # History
  history = Degree.find_by(name: 'History')
  intro = history.courses.find_by(name: 'Introduction to History')
  foundational = history.courses.where(name: ['Historiography', 'The Age of Ideas', 'Ancient History'])
  history.courses.each do |course|
    Prerequisite.create!(course: course, prior: intro) unless course == intro
    foundational.map { |foundation| Prerequisite.create!(course: course, prior: foundation) } unless foundational.include?(course) || intro == course
  end

  # Linguistics
  linguistics = Degree.find_by(name: 'Linguistics')
  intro = linguistics.courses.find_by(name: 'Introduction To Linguistics')
  foundational = linguistics.courses.where(name: ['Semantics', 'Language, Society, And Culture', 'History Of Linguistics', 'Foundations Of Linguistic Theory'])
  linguistics.courses.each do |course|
    Prerequisite.create!(course: course, prior: intro) unless course == intro
    foundational.map { |foundation| Prerequisite.create!(course: course, prior: foundation) } unless foundational.include?(course) || intro == course
  end
end
