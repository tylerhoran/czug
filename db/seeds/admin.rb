# frozen_string_literal: true

User.create!(
  email: ENV['ADMIN_EMAIL'],
  first_name: ENV['ADMIN_FIRST_NAME'],
  last_name: ENV['ADMIN_LAST_NAME'],
  role: 'admin',
  status: 'approved',
  password: ENV['ADMIN_PASSWORD'],
  skip_callbacks: true,
  calendly_link: 'https://calendly.com/octad'
)
