# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20171217202558) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "activities", force: :cascade do |t|
    t.string   "subject_type"
    t.integer  "subject_id"
    t.string   "name"
    t.string   "direction"
    t.integer  "user_id"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.index ["subject_type", "subject_id"], name: "index_activities_on_subject_type_and_subject_id", using: :btree
    t.index ["user_id"], name: "index_activities_on_user_id", using: :btree
  end

  create_table "appointments", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "attendee_id"
    t.datetime "time"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.string   "google_id"
    t.string   "hangout_link"
    t.index ["attendee_id"], name: "index_appointments_on_attendee_id", using: :btree
    t.index ["user_id"], name: "index_appointments_on_user_id", using: :btree
  end

  create_table "articles", force: :cascade do |t|
    t.string   "name"
    t.text     "body"
    t.string   "attachment_file_name"
    t.string   "attachment_content_type"
    t.integer  "attachment_file_size"
    t.datetime "attachment_updated_at"
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.string   "slug"
    t.index ["slug"], name: "index_articles_on_slug", unique: true, using: :btree
  end

  create_table "assignments", force: :cascade do |t|
    t.integer  "instance_id"
    t.string   "name"
    t.text     "body"
    t.string   "attachment_file_name"
    t.string   "attachment_content_type"
    t.integer  "attachment_file_size"
    t.datetime "attachment_updated_at"
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.index ["instance_id"], name: "index_assignments_on_instance_id", using: :btree
  end

  create_table "authentications", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "provider_id"
    t.string   "uid"
    t.string   "token"
    t.datetime "token_expires_at"
    t.text     "params"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.index ["provider_id"], name: "index_authentications_on_provider_id", using: :btree
    t.index ["user_id"], name: "index_authentications_on_user_id", using: :btree
  end

  create_table "availabilities", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "course_id"
    t.text     "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.index ["course_id"], name: "index_availabilities_on_course_id", using: :btree
    t.index ["user_id"], name: "index_availabilities_on_user_id", using: :btree
  end

  create_table "banks", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "name"
    t.string   "country"
    t.string   "token"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_banks_on_user_id", using: :btree
  end

  create_table "cards", force: :cascade do |t|
    t.integer  "card_type"
    t.integer  "brand"
    t.string   "last_four"
    t.string   "stripe_card_token"
    t.string   "expiration_month"
    t.string   "expiration_year"
    t.boolean  "default",           default: false
    t.integer  "user_id"
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
    t.string   "name"
    t.integer  "status",            default: 0
    t.string   "slug"
    t.index ["slug"], name: "index_cards_on_slug", unique: true, using: :btree
    t.index ["user_id"], name: "index_cards_on_user_id", using: :btree
  end

  create_table "charges", force: :cascade do |t|
    t.integer  "card_id"
    t.integer  "instance_id"
    t.integer  "amount"
    t.string   "stripe_charge_token"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
    t.index ["card_id"], name: "index_charges_on_card_id", using: :btree
    t.index ["instance_id"], name: "index_charges_on_instance_id", using: :btree
  end

  create_table "courses", force: :cascade do |t|
    t.string   "name"
    t.integer  "degree_id"
    t.string   "slug"
    t.integer  "status",      default: 0
    t.integer  "proposer_id"
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.text     "description"
    t.index ["degree_id"], name: "index_courses_on_degree_id", using: :btree
    t.index ["proposer_id"], name: "index_courses_on_proposer_id", using: :btree
    t.index ["slug"], name: "index_courses_on_slug", unique: true, using: :btree
  end

  create_table "degrees", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.integer  "degree_type"
    t.string   "slug"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.index ["slug"], name: "index_degrees_on_slug", unique: true, using: :btree
  end

  create_table "degrees_divisions", id: false, force: :cascade do |t|
    t.integer "degree_id"
    t.integer "division_id"
    t.index ["degree_id"], name: "index_degrees_divisions_on_degree_id", using: :btree
    t.index ["division_id"], name: "index_degrees_divisions_on_division_id", using: :btree
  end

  create_table "delayed_jobs", force: :cascade do |t|
    t.integer  "priority",   default: 0, null: false
    t.integer  "attempts",   default: 0, null: false
    t.text     "handler",                null: false
    t.text     "last_error"
    t.datetime "run_at"
    t.datetime "locked_at"
    t.datetime "failed_at"
    t.string   "locked_by"
    t.string   "queue"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.index ["priority", "run_at"], name: "delayed_jobs_priority", using: :btree
  end

  create_table "divisions", force: :cascade do |t|
    t.string   "name"
    t.string   "slug"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.text     "description"
    t.index ["slug"], name: "index_divisions_on_slug", unique: true, using: :btree
  end

  create_table "emails", force: :cascade do |t|
    t.string   "name"
    t.integer  "status"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "enrollments", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "status"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.integer  "charge_id"
    t.integer  "transfer_amount"
    t.integer  "instance_id"
    t.integer  "grade"
    t.index ["charge_id"], name: "index_enrollments_on_charge_id", using: :btree
    t.index ["instance_id"], name: "index_enrollments_on_instance_id", using: :btree
    t.index ["user_id"], name: "index_enrollments_on_user_id", using: :btree
  end

  create_table "evaluations", force: :cascade do |t|
    t.integer  "enrollment_id"
    t.integer  "rating"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.index ["enrollment_id"], name: "index_evaluations_on_enrollment_id", using: :btree
  end

  create_table "faqs", force: :cascade do |t|
    t.integer  "faq_type"
    t.string   "title"
    t.text     "body"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string   "heading"
  end

  create_table "friendly_id_slugs", force: :cascade do |t|
    t.string   "slug",                      null: false
    t.integer  "sluggable_id",              null: false
    t.string   "sluggable_type", limit: 50
    t.string   "scope"
    t.datetime "created_at"
    t.index ["slug", "sluggable_type", "scope"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type_and_scope", unique: true, using: :btree
    t.index ["slug", "sluggable_type"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type", using: :btree
    t.index ["sluggable_id"], name: "index_friendly_id_slugs_on_sluggable_id", using: :btree
    t.index ["sluggable_type"], name: "index_friendly_id_slugs_on_sluggable_type", using: :btree
  end

  create_table "instances", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "course_id"
    t.string   "syllabus_file_name"
    t.string   "syllabus_content_type"
    t.integer  "syllabus_file_size"
    t.datetime "syllabus_updated_at"
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
    t.integer  "availability_id"
    t.integer  "status"
    t.text     "description"
    t.string   "slug"
    t.index ["availability_id"], name: "index_instances_on_availability_id", using: :btree
    t.index ["course_id"], name: "index_instances_on_course_id", using: :btree
    t.index ["slug"], name: "index_instances_on_slug", unique: true, using: :btree
    t.index ["user_id"], name: "index_instances_on_user_id", using: :btree
  end

  create_table "mailboxer_conversation_opt_outs", force: :cascade do |t|
    t.string  "unsubscriber_type"
    t.integer "unsubscriber_id"
    t.integer "conversation_id"
    t.index ["conversation_id"], name: "index_mailboxer_conversation_opt_outs_on_conversation_id", using: :btree
    t.index ["unsubscriber_id", "unsubscriber_type"], name: "index_mailboxer_conversation_opt_outs_on_unsubscriber_id_type", using: :btree
  end

  create_table "mailboxer_conversations", force: :cascade do |t|
    t.string   "subject",    default: ""
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  create_table "mailboxer_notifications", force: :cascade do |t|
    t.string   "type"
    t.text     "body"
    t.string   "subject",              default: ""
    t.string   "sender_type"
    t.integer  "sender_id"
    t.integer  "conversation_id"
    t.boolean  "draft",                default: false
    t.string   "notification_code"
    t.string   "notified_object_type"
    t.integer  "notified_object_id"
    t.string   "attachment"
    t.datetime "updated_at",                           null: false
    t.datetime "created_at",                           null: false
    t.boolean  "global",               default: false
    t.datetime "expires"
    t.index ["conversation_id"], name: "index_mailboxer_notifications_on_conversation_id", using: :btree
    t.index ["notified_object_id", "notified_object_type"], name: "index_mailboxer_notifications_on_notified_object_id_and_type", using: :btree
    t.index ["sender_id", "sender_type"], name: "index_mailboxer_notifications_on_sender_id_and_sender_type", using: :btree
    t.index ["type"], name: "index_mailboxer_notifications_on_type", using: :btree
  end

  create_table "mailboxer_receipts", force: :cascade do |t|
    t.string   "receiver_type"
    t.integer  "receiver_id"
    t.integer  "notification_id",                            null: false
    t.boolean  "is_read",                    default: false
    t.boolean  "trashed",                    default: false
    t.boolean  "deleted",                    default: false
    t.string   "mailbox_type",    limit: 25
    t.datetime "created_at",                                 null: false
    t.datetime "updated_at",                                 null: false
    t.boolean  "is_delivered",               default: false
    t.string   "delivery_method"
    t.string   "message_id"
    t.index ["notification_id"], name: "index_mailboxer_receipts_on_notification_id", using: :btree
    t.index ["receiver_id", "receiver_type"], name: "index_mailboxer_receipts_on_receiver_id_and_receiver_type", using: :btree
  end

  create_table "meetings", force: :cascade do |t|
    t.integer  "schedule_id"
    t.datetime "occurrence"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.string   "google_id"
    t.string   "hangout_link"
    t.index ["schedule_id"], name: "index_meetings_on_schedule_id", using: :btree
  end

  create_table "petitions", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "degree_id"
    t.integer  "status"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text     "note"
    t.index ["degree_id"], name: "index_petitions_on_degree_id", using: :btree
    t.index ["user_id"], name: "index_petitions_on_user_id", using: :btree
  end

  create_table "posts", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "instance_id"
    t.text     "body"
    t.string   "attachment_file_name"
    t.string   "attachment_content_type"
    t.integer  "attachment_file_size"
    t.datetime "attachment_updated_at"
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.index ["instance_id"], name: "index_posts_on_instance_id", using: :btree
    t.index ["user_id"], name: "index_posts_on_user_id", using: :btree
  end

  create_table "prerequisites", force: :cascade do |t|
    t.integer  "course_id"
    t.integer  "prior_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["course_id"], name: "index_prerequisites_on_course_id", using: :btree
    t.index ["prior_id"], name: "index_prerequisites_on_prior_id", using: :btree
  end

  create_table "providers", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["name"], name: "index_name_on_providers", using: :btree
  end

  create_table "refunds", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "charge_id"
    t.integer  "amount"
    t.string   "stripe_refund_token"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
    t.index ["charge_id"], name: "index_refunds_on_charge_id", using: :btree
    t.index ["user_id"], name: "index_refunds_on_user_id", using: :btree
  end

  create_table "reservations", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "availability_id"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.index ["availability_id"], name: "index_reservations_on_availability_id", using: :btree
    t.index ["user_id"], name: "index_reservations_on_user_id", using: :btree
  end

  create_table "schedules", force: :cascade do |t|
    t.integer  "instance_id"
    t.date     "start_date"
    t.string   "day_1"
    t.time     "time_1"
    t.string   "day_2"
    t.time     "time_2"
    t.string   "day_3"
    t.time     "time_3"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.index ["instance_id"], name: "index_schedules_on_instance_id", using: :btree
  end

  create_table "schools", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "name"
    t.string   "fb_id"
    t.string   "school_type"
    t.string   "degree"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.index ["user_id"], name: "index_schools_on_user_id", using: :btree
  end

  create_table "slots", force: :cascade do |t|
    t.time     "start_time"
    t.time     "end_time"
    t.integer  "day"
    t.integer  "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_slots_on_user_id", using: :btree
  end

  create_table "submissions", force: :cascade do |t|
    t.integer  "assignment_id"
    t.integer  "user_id"
    t.string   "attachment_file_name"
    t.string   "attachment_content_type"
    t.integer  "attachment_file_size"
    t.datetime "attachment_updated_at"
    t.text     "body"
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.integer  "grade"
    t.index ["assignment_id"], name: "index_submissions_on_assignment_id", using: :btree
    t.index ["user_id"], name: "index_submissions_on_user_id", using: :btree
  end

  create_table "transfers", force: :cascade do |t|
    t.integer  "card_id"
    t.integer  "amount"
    t.string   "stripe_transfer_token"
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
    t.index ["card_id"], name: "index_transfers_on_card_id", using: :btree
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                            default: "",    null: false
    t.string   "encrypted_password",               default: "",    null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",                    default: 0,     null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at",                                       null: false
    t.datetime "updated_at",                                       null: false
    t.integer  "role"
    t.string   "prefix"
    t.string   "first_name"
    t.string   "middle_name"
    t.string   "last_name"
    t.string   "suffix"
    t.string   "stripe_customer_token"
    t.string   "stripe_account_token"
    t.string   "photo_file_name"
    t.string   "photo_content_type"
    t.integer  "photo_file_size"
    t.datetime "photo_updated_at"
    t.integer  "status",                           default: 0
    t.string   "vita_file_name"
    t.string   "vita_content_type"
    t.integer  "vita_file_size"
    t.datetime "vita_updated_at"
    t.string   "previous_transcript_file_name"
    t.string   "previous_transcript_content_type"
    t.integer  "previous_transcript_file_size"
    t.datetime "previous_transcript_updated_at"
    t.integer  "last_degree_type"
    t.string   "last_degree_name"
    t.string   "last_degree_major"
    t.string   "slug"
    t.integer  "taught_degree_id"
    t.string   "street_address"
    t.string   "city"
    t.string   "region"
    t.string   "postcode"
    t.string   "country"
    t.date     "dob"
    t.string   "ssn_last_4"
    t.boolean  "activity_notifications",           default: true
    t.boolean  "messages_notifications",           default: true
    t.boolean  "newsletter_notifications",         default: true
    t.boolean  "verification_required",            default: false
    t.integer  "level"
    t.integer  "step",                             default: 0
    t.string   "auth_token"
    t.string   "authy_id"
    t.datetime "last_sign_in_with_authy"
    t.boolean  "authy_enabled",                    default: false
    t.boolean  "can_interview",                    default: false
    t.integer  "karma",                            default: 0
    t.string   "calendly_link"
    t.string   "finish_token"
    t.boolean  "social_auth",                      default: false
    t.string   "close_opportunity_id"
    t.boolean  "close_active",                     default: false
    t.index ["authy_id"], name: "index_users_on_authy_id", using: :btree
    t.index ["email"], name: "index_users_on_email", unique: true, using: :btree
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
    t.index ["slug"], name: "index_users_on_slug", unique: true, using: :btree
    t.index ["taught_degree_id"], name: "index_users_on_taught_degree_id", using: :btree
  end

  create_table "works", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "name"
    t.string   "position"
    t.string   "fb_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_works_on_user_id", using: :btree
  end

  add_foreign_key "activities", "users"
  add_foreign_key "appointments", "users"
  add_foreign_key "assignments", "instances"
  add_foreign_key "availabilities", "courses"
  add_foreign_key "availabilities", "users"
  add_foreign_key "banks", "users"
  add_foreign_key "charges", "cards"
  add_foreign_key "charges", "instances"
  add_foreign_key "courses", "degrees"
  add_foreign_key "enrollments", "charges"
  add_foreign_key "enrollments", "instances"
  add_foreign_key "enrollments", "users"
  add_foreign_key "evaluations", "enrollments"
  add_foreign_key "instances", "availabilities"
  add_foreign_key "instances", "courses"
  add_foreign_key "instances", "users"
  add_foreign_key "mailboxer_conversation_opt_outs", "mailboxer_conversations", column: "conversation_id", name: "mb_opt_outs_on_conversations_id"
  add_foreign_key "mailboxer_notifications", "mailboxer_conversations", column: "conversation_id", name: "notifications_on_conversation_id"
  add_foreign_key "mailboxer_receipts", "mailboxer_notifications", column: "notification_id", name: "receipts_on_notification_id"
  add_foreign_key "meetings", "schedules"
  add_foreign_key "petitions", "degrees"
  add_foreign_key "petitions", "users"
  add_foreign_key "posts", "instances"
  add_foreign_key "posts", "users"
  add_foreign_key "prerequisites", "courses"
  add_foreign_key "refunds", "charges"
  add_foreign_key "refunds", "users"
  add_foreign_key "reservations", "availabilities"
  add_foreign_key "reservations", "users"
  add_foreign_key "schedules", "instances"
  add_foreign_key "schools", "users"
  add_foreign_key "slots", "users"
  add_foreign_key "submissions", "assignments"
  add_foreign_key "submissions", "users"
  add_foreign_key "transfers", "cards"
  add_foreign_key "works", "users"
end
