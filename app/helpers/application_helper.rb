# frozen_string_literal: true

module ApplicationHelper
  def flash_class(level)
    case level
    when 'notice' then 'alert alert-info'
    when 'success' then 'alert alert-success'
    when 'error' then 'alert alert-danger'
    when 'alert' then 'alert alert-danger'
    end
  end

  def on_page(controller, action)
    controller_name == controller && action.to_a.include?(action_name)
  end

  def full_page?
    if controller_name == 'sessions' && action_name == 'new'
      true
    elsif controller_name == 'sessions' && action_name == 'create'
      true
    elsif controller_name == 'devise_authy' && action_name == 'GET_verify_authy'
      true
    elsif controller_name == 'registrations' && action_name == 'new'
      true
    elsif controller_name == 'registrations' && action_name == 'create'
      true
    elsif controller_name == 'passwords' && action_name == 'new'
      true
    elsif controller_name == 'passwords' && action_name == 'edit'
      true
    elsif controller_name == 'passwords' && action_name == 'create'
      true
    elsif controller_name == 'passwords' && action_name == 'update'
      true
    else
      false
    end
  end
end
