# frozen_string_literal: true

module MessagesHelper
  def recipients_options
    s = ''
    User.all.each do |user|
      s << "<option value='#{user.id}' data-img-src='#{user.photo_image}'>#{user.name}</option>"
    end
    content_tag(:div, s)
  end
end
