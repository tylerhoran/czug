# frozen_string_literal: true

class ApplicationMailer < ActionMailer::Base
  default from: 'site@brasden.ch'
  layout 'mailer'
end
