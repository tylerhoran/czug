# frozen_string_literal: true

class NotificationMailer < ApplicationMailer
  def send_email(notification, receiver)
    new_notification_email(notification, receiver)
  end

  def new_notification_email(notification, receiver)
    @notification = notification
    @receiver     = receiver
    show_subject(notification)
    mail(to: receiver.send(Mailboxer.email_method, notification),
         subject: t('mailboxer.notification_mailer.subject', subject: @subject),
         template_name: 'new_notification_email'
    )
  end

  private

  def show_subject(container)
    @subject = container.subject.html_safe? ? container.subject : strip_tags(container.subject)
  end

  def strip_tags(text)
    ::Mailboxer::Cleaner.instance.strip_tags(text)
  end
end
