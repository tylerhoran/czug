# frozen_string_literal: true

class UserMailer < ApplicationMailer
  def acceptance_mail(user)
    @user = user
    mail to: user.email, subject: "Congratulations! You've been accepted into the College of Brasden"
  end

  def interview_mail(user, interviewer)
    @user = user
    @interviewer = interviewer
    mail to: user.email, subject: "Congratulations! You've been invited to interview at the College of Brasden"
  end

  def professor_interview_mail(user)
    @user = user
    mail to: user.email, subject: "Congratulations! You've been invited to interview at the College of Brasden"
  end

  def professor_acceptance_mail(user)
    @user = user
    mail to: user.email, subject: "Congratulations! You're application to the College of Brasden has been approved"
  end

  def rejection_mail(user)
    @user = user
    mail to: user.email, subject: 'Information on your application to the College of Brasden'
  end

  def professor_rejection_mail(user)
    @user = user
    mail to: user.email, subject: 'Information on your application to the College of Brasden'
  end

  def course_approved(course)
    @course = course
    @user = course.proposer
    mail to: @user.email, subject: 'Congratulations! Your course has been approved'
  end

  def course_rejected(course)
    @course = course
    @user = course.proposer
    mail to: @user.email, subject: 'Information on your course proposal at the College of Brasden'
  end

  def receipt_mail(charge)
    @charge = charge
    @user = charge.card.user
    mail to: @user.email, subject: 'Receipt for your course enrollments at the college of Brasden'
  end

  def refund_created(enrollment, refund)
    @enrollment = enrollment
    @user = enrollment.user
    @refund = refund
    mail to: @user.email, subject: "Receipt for your refund for #{enrollment.instance.course_name}"
  end

  def reservation_removed(availability, user)
    @availability = availability
    @user = user
    mail to: @user.email, subject: "Notification of withdrawal from #{availability.course_name} at the College of Brasden"
  end

  def withdrawal_mail(enrollment)
    @enrollment = enrollment
    @user = enrollment.user
    mail to: @user.email, subject: "Notification of withdrawal from
                                    #{enrollment.instance.course_name} at the College of Brasden"
  end

  def user_enrolled(enrollment)
    @enrollment = enrollment
    @user = enrollment.user
    @professor = enrollment.instance.user
    mail to: @professor.email, subject: "#{@user.name} has enrolled to your course #{@enrollment.instance.course_name}"
  end

  def grade_submitted(enrollment)
    @enrollment = enrollment
    @user = enrollment.user
    mail to: @user.email, subject: "You've received a grade for: #{@enrollment.instance.course_name}"
  end

  def instance_post(post, user)
    @post = post
    @user = user
    (attachments[@post.attachment_file_name] = File.read(@post.attachment.path)) if @post.attachment.present?
    mail to: @user.email, subject: "#{@post.user.name} added a post in the #{@post.instance.course_name} messageboard"
  end

  def transfer_created(instances, transfer, amount)
    @transfer = transfer
    @instances = instances
    @user = transfer.card.user
    @amount = amount
    mail to: @user.email, subject: "The College of Brasden: Transfer #{Time.zone.today}"
  end

  def verification_mail(user)
    @user = user
    mail to: @user.email, subject: 'We need additional information from you to process your transfers'
  end

  def assignment_created(assignment, user)
    @assignment = @assignment
    @user = user
    mail to: @user.email, subject: "New assignment added to: #{assignment.instance.course_name}"
  end

  def course_created(course, user)
    @course = @course
    @user = user
    mail to: @user.email, subject: "New available course added to: #{course.degree_name}"
  end

  def grade_created(grade, user)
    @grade = @grade
    @user = user
    mail to: @user.email, subject: "Your grade to #{grade.instance.course_name} has been posted"
  end

  def post_created(post, user)
    @post = @post
    @user = user
    mail to: @user.email, subject: "New post added to: #{post.instance.course_name}"
  end

  def comment_created(comment, user)
    @comment = @comment
    @user = user
    mail to: @user.email, subject: "#{comment.user.name} commented on your post"
  end

  def enrollment_created(enrollment, user)
    @enrollment = @enrollment
    @user = user
    mail to: @user.email, subject: "The course: #{enrollment.instance.course_name}, is starting soon"
  end

  def certificate_created(_certificate, user)
    @certificate = @certificate
    @user = user
    mail to: @user.email, subject: 'Your Degree Certificate has been issued!'
  end

  def meeting_updated(meeting, user)
    @meeting = meeting
    @user = user
    mail to: @user.email, subject: "You Meeting for #{meeting.schedule.instance.course_name} has been changed"
  end

  def grade_prompt(user, instance)
    @user = user
    @instance = instance
    mail to: @user.email, subject: "Your course #{@instance.course_name} has ended. Time to submit grades."
  end

  def register_prompt(user, instance)
    @user = user
    @instance = instance
    mail to: @user.email, subject: "Your course #{@instance.course_name} is ending soon. Time to enroll in another."
  end

  def notify_professor_of_reservations(user, availability)
    @user = user
    @availability = availability
    mail to: @user.email, subject: "Your course has #{@availability.reservations.length} of 24 seats reserved."
  end

  def notify_petition_rejection(petition)
    @user = petition.user
    @petition = petition
    mail to: @user.email, subject: 'Information on your degree petition at the College of Brasden'
  end

  def prompt_evaluation(enrollment)
    @enrollment = enrollment
    @user = enrollment.user
    mail to: @user.email, subject: "Please provide feedback on your course: #{@enrollment.instance.course_name}"
  end

  def retarget_email(email)
    @email = email
    mail to: @email.name, subject: 'Your application to the College of Brasden is almost complete!'
  end

  def interviewable_welcome(user)
    @user = user
    mail to: @user.email, subject: "You've been invited to become an application reviewer."
  end

  def notify_of_prospective_interview(interviewable, user)
    @user = user
    @interviewable = interviewable
    mail to: @user.email, subject: 'A student has been notified to schedule an interview with you.'
  end
end
