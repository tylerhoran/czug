# frozen_string_literal: true

class MessageMailer < ApplicationMailer
  def send_email(message, receiver)
    if message.conversation.messages.size > 1
      reply_message_email(message, receiver)
    else
      new_message_email(message, receiver)
    end
  end

  # Sends an email for indicating a new message for the receiver
  def new_message_email(message, receiver)
    @message  = message
    @receiver = receiver
    show_subject(message)
    mail(to: receiver.send(Mailboxer.email_method, message),
         subject: t('mailboxer.message_mailer.subject_new', subject: @subject)
    )
  end

  # Sends and email for indicating a reply in an already created conversation
  def reply_message_email(message, receiver)
    @message  = message
    @receiver = receiver
    show_subject(message)
    mail(to: receiver.send(Mailboxer.email_method, message),
         subject: t('mailboxer.message_mailer.subject_reply', subject: @subject)
    )
  end

  private

  def show_subject(container)
    @subject = container.subject.html_safe? ? container.subject : strip_tags(container.subject)
  end

  def strip_tags(text)
    ::Mailboxer::Cleaner.instance.strip_tags(text)
  end
end
