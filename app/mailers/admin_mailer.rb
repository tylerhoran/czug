# frozen_string_literal: true

class AdminMailer < ApplicationMailer
  def new_user_waiting_for_approval(user)
    @user = user
    if @user.professor?
      if @user.vita.present?
        url = Rails.env.production? ? "https:#{@user.vita.url}" : @user.vita.path
        attachments[@user.vita_file_name] = begin
                                              open(url).read
                                            rescue StandardError
                                              nil
                                            end
      end
    elsif @user.previous_transcript.present?
      url = Rails.env.production? ? "https:#{@user.previous_transcript.url}" : @user.previous_transcript.path
      attachments[@user.previous_transcript_file_name] = begin
                                                           open(url).read
                                                         rescue StandardError
                                                           nil
                                                         end
    end
    mail(to: ENV['ADMIN_EMAIL'], subject: 'New User waiting for approval')
  end

  def notify_petition(user, degree)
    @user = user
    @degree = degree
    mail(to: ENV['ADMIN_EMAIL'], subject: 'New degree petition')
  end

  def course_proposal(user)
    @user = user
    mail(to: ENV['ADMIN_EMAIL'], subject: 'New Course Proposal')
  end

  def transfer_summary(transfers, count, errors)
    @transfers = transfers
    @count = count
    @errors = errors
    mail to: ENV['ADMIN_EMAIL'], subject: "Transfer Summary for #{Time.zone.today}"
  end
end
