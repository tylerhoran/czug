# frozen_string_literal: true

class Enrollment < ApplicationRecord
  belongs_to :user
  belongs_to :charge
  belongs_to :instance
  validates :user, presence: true
  enum status: %i[pending active rejected withdrawal]

  after_create :notify_student, unless: :skip_callbacks
  after_create :charge_student, unless: :skip_callbacks
  after_update :process_refunds, unless: :skip_callbacks
  after_commit :update_meeting_participants, unless: :skip_callbacks

  validates :grade, inclusion: { in: 1..100, allow_nil: true }

  after_update :notify_grade, unless: :skip_callbacks

  def notify_grade
    if grade_changed?
      Activity.create(
        subject: self,
        name: 'grade_added',
        direction: 'to',
        user: user
      )
    end
  end

  def notify_student
    Activity.create(
      subject: self,
      name: 'enrollment_added',
      direction: 'to',
      user: user
    )
  end
  handle_asynchronously :notify_student

  def charge_student
    charge = Charge.create!(
      amount: (ENV['COURSE_PRICE'].to_i * 100),
      card: user.cards.find_by(default: true),
      instance: instance
    )
    update(charge: charge)
  end
  handle_asynchronously :charge_student

  def update_meeting_participants
    instance.schedule.meetings.each(&:update_meeting_participants)
  end
  handle_asynchronously :update_meeting_participants

  def process_refunds
    if status_changed?(from: 'active', to: 'withdrawal') || status_changed?(from: 'active', to: 'rejected')
      if (instance.schedule_start_date + 7.days) > Time.current
        refund = Refund.create(amount: 20_000, charge: charge, user: user)
        UserMailer.refund_created(self, refund)
      end
      UserMailer.withdrawal_mail(self)
    end
  end
end
