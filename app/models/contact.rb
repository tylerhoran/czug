# frozen_string_literal: true

class Contact < MailForm::Base
  attribute :first_name, validate: true
  attribute :last_name, validate: true
  attribute :email, validate: /\A([\w\.%\+\-]+)@([\w\-]+\.)+([\w]{2,})\z/i
  attribute :subject, validate: true

  attribute :message, validate: true

  def headers
    {
      subject: 'Contact form inquiry',
      to: ENV['ADMIN_EMAIL'],
      from: %("#{first_name} #{last_name}" <#{email}>),
    }
  end
end
