# frozen_string_literal: true

class Submission < ApplicationRecord
  belongs_to :assignment
  belongs_to :user
  validates :assignment, :user, presence: true

  has_attached_file :attachment,
    default_url: ActionController::Base.helpers.asset_path('defaults/documents_default.jpg')
  validates_attachment_content_type :attachment,
    content_type: ['application/pdf', 'application/msword',
                   'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
                   'text/plain', %r{\Aimage\/.*\z}]
end
