# frozen_string_literal: true

class Authentication < ApplicationRecord
  belongs_to :user
  belongs_to :provider

  serialize :params

  def self.create_from_omniauth(params, user, provider)
    expires_at = params['credentials']['expires_at']
    token_expires_at = expires_at ? Time.zone.at(expires_at).to_datetime : nil
    graph = Koala::Facebook::API.new(params['credentials']['token'])
    me = graph.get_object('me?fields=work,education')
    create(
      user: user,
      provider: provider,
      uid: params['uid'],
      token: params['credentials']['token'],
      token_expires_at: token_expires_at,
      params: params
    )
    if me['work']&.any?
      me['work'].each do |job|
        Work.create(
          user: user,
          name: job['employer'] ? job['employer']['name'] : nil,
          fb_id: job['id'],
          position: job['position'] ? job['position']['name'] : nil
        )
      end
    end

    if me['education']&.any?
      me['education'].each do |school|
        School.create(
          user: user,
          name: school['school']['name'],
          fb_id: school['school']['id'],
          school_type: school['type'],
          degree: school['concentration'] ? school['concentration'].first['name'] : nil
        )
      end
    end
  end
end
