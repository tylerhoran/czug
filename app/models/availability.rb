# frozen_string_literal: true

class Availability < ApplicationRecord
  belongs_to :user
  belongs_to :course
  validates :user, :course, presence: true
  has_many :reservations, dependent: :destroy
  has_many :instances, dependent: :destroy
  after_create :notify_students, unless: :skip_callbacks
  after_create :update_karma, unless: :skip_callbacks
  accepts_nested_attributes_for :instances, reject_if: :all_blank, allow_destroy: true
  delegate :name, to: :course, prefix: true
  delegate :name, to: :user, prefix: true

  def update_karma
    user.update(karma: user.karma + 5)
  end

  def users_grid(offset)
    collection = {}
    reservations.each do |r|
      student = r.user
      collection.merge!(student.availability_grid(offset)) { |_key, o, n| o + n }
    end
    collection.merge!(user.availability_grid(offset)) { |_key, o, n| o + n }
    counts = Hash.new 0
    output_hash = Hash.new 0
    collection.each do |day, hours|
      counts = Hash.new 0
      hours.each do |hour|
        counts[hour] += 1
      end
      output_hash[day] = counts
    end
    output_array = []
    output_hash.each do |day, hours|
      hours.each do |hour|
        output_array << [day, hour[0], hour[1]]
      end
    end
    output_array
  end

  def notify_students
    course.degree.enrolled_users.each do |user|
      Activity.create(
        subject: self,
        name: 'course_added',
        direction: 'by',
        user: user
      )
    end
  end
end
