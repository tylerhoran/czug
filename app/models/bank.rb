# frozen_string_literal: true

class Bank < ApplicationRecord
  belongs_to :user
  attr_accessor :number
  validates :token, :name, :country, presence: true

  def self.supported_countries
    [%w[Denmark DK],
     %w[Finland FI],
     %w[France FR],
     %w[Ireland IE],
     %w[Norway NO],
     %w[Spain ES],
     %w[Sweden SE],
     ['United Kingdom', 'UK'],
     %w[Austria AT],
     %w[Belgium BE],
     %w[Germany DE]]
  end
end
