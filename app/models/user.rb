# frozen_string_literal: true

class User < ApplicationRecord
  extend FriendlyId
  attr_accessor :full_name, :personal_id_number, :prof
  acts_as_messageable
  devise :omniauthable, :authy_authenticatable, :database_authenticatable, :registerable,
    :recoverable, :rememberable, :trackable, :validatable
  has_many :cards, dependent: :destroy
  has_many :banks, dependent: :destroy
  has_many :appointments, dependent: :destroy
  has_many :availabilities, dependent: :destroy
  has_many :reservations, dependent: :destroy
  has_many :petitions, dependent: :destroy
  has_many :slots, dependent: :destroy
  has_many :charges, through: :cards, dependent: :destroy
  has_many :transfers, through: :cards, dependent: :destroy
  has_many :enrollments, dependent: :destroy
  has_many :instances, dependent: :destroy
  has_many :activities, dependent: :destroy
  has_many :works, dependent: :destroy
  has_many :schools, dependent: :destroy
  has_many :authentications, dependent: :destroy
  belongs_to :taught_degree,
    class_name: 'Degree', foreign_key: 'taught_degree_id'
  has_many :proposed_courses,
    class_name: 'Course', foreign_key: 'proposer_id', dependent: :destroy
  friendly_id :name, use: :slugged

  enum level: %i[bachelors masters doctorate]
  enum step: %i[registration profile availability first_courses teachable_courses payment transfer]

  accepts_nested_attributes_for :proposed_courses, reject_if: :all_blank, allow_destroy: true
  accepts_nested_attributes_for :schools, reject_if: :all_blank, allow_destroy: true
  accepts_nested_attributes_for :works, reject_if: :all_blank, allow_destroy: true
  accepts_nested_attributes_for :availabilities, reject_if: :all_blank, allow_destroy: true
  accepts_nested_attributes_for :instances, reject_if: :all_blank, allow_destroy: true
  accepts_nested_attributes_for :reservations, reject_if: :all_blank, allow_destroy: true
  accepts_nested_attributes_for :slots, reject_if: :all_blank, allow_destroy: true
  accepts_nested_attributes_for :cards, reject_if: :all_blank, allow_destroy: true
  accepts_nested_attributes_for :banks, reject_if: :all_blank, allow_destroy: true

  enum role: %i[professor student admin]
  enum status: %i[pending interview approved rejected]

  validates :email, presence: true
  validates :email, format: { without: /\Achange@me/, on: :update }

  has_attached_file :photo, styles: {
    medium: '300x300>', thumb: '100x100>'
  }, default_url: ActionController::Base.helpers.asset_path('defaults/photos_default.jpg')
  validates_attachment_content_type :photo, content_type: %r{\Aimage\/.*\z}

  has_attached_file :previous_transcript,
    default_url: ActionController::Base.helpers.asset_path('defaults/documents_default.jpg')
  validates_attachment_content_type :previous_transcript,
    content_type: ['application/pdf', 'application/msword',
                   'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
                   'text/plain']

  has_attached_file :vita,
    default_url: ActionController::Base.helpers.asset_path('defaults/documents_default.jpg')
  validates_attachment_content_type :vita,
    content_type: ['application/pdf', 'application/msword',
                   'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
                   'text/plain']

  after_commit :send_admin_mail, on: :create, unless: :skip_callbacks_or_finish_token?
  before_create :split_name, unless: :skip_callbacks
  before_validation :set_role
  after_create :generate_authentication_token!, unless: :skip_callbacks
  after_update :send_acceptance_mail, unless: :skip_callbacks
  after_update :send_interview_mail, unless: :skip_callbacks
  after_update :send_rejection_mail, unless: :skip_callbacks
  after_update :send_verification_mail, unless: :skip_callbacks
  after_update :set_default_card
  after_update :set_close_active, unless: :close_active
  # after_create :create_close_opportunity, unless: :skip_callbacks

  validate :slots_greater_than_availabilities
  validate :verification_fields_if_required

  validates :first_name, presence: true, if: :profile?
  validates :last_name, presence: true, if: :profile?
  validates :street_address, presence: true, if: :profile?
  validates :city, presence: true, if: :profile?
  validates :region, presence: true, if: :profile?
  validates :postcode, presence: true, if: :profile?
  validates :country, presence: true, if: :profile?
  validates :previous_transcript, presence: true, if: :student_without_social_auth?
  validates :vita, presence: true, if: :professor_without_social_auth?

  validates :slots, presence: true, if: :availability?
  validates :reservations, presence: true, if: :first_courses?
  validates :availabilities, presence: true, if: :teachable_courses?
  validates :cards, presence: true, if: :payment?
  validates :cards, presence: true, if: :transfer?

  delegate :name, to: :taught_degree, allow_nil: true, prefix: true

  def photo_image
    photo.present? ? photo.url(:thumb) : ActionController::Base.helpers.image_url('defaults/photos_default.jpg')
  end

  def set_role
    self.role ||= prof ? 'professor' : 'student'
  end

  def student_without_social_auth?
    student? && social_auth == false
  end

  def professor_without_social_auth?
    professor? && social_auth == false
  end

  def skip_callbacks_or_finish_token?
    finish_token || skip_callbacks
  end

  def generate_authentication_token!
    loop do
      self.auth_token = Devise.friendly_token
      break unless self.class.exists?(auth_token: auth_token)
    end
  end

  def set_default_card
    cards.active.first.update(default: true) if cards.active.any? && cards.find_by(default: true).nil?
  end

  def primary_card
    cards.find_by(default: true)
  end

  def split_name
    split = full_name.to_s.split(' ')
    case split.length
    when 1
      self.first_name = split[0]
    when 2
      self.first_name = split[0]
      self.last_name = split[1]
    when 3
      self.first_name = split[0]
      self.middle_name = split[1]
      self.middle_name = split[2]
    end
  end

  def name_with_title
    "#{prefix ? "#{prefix} " : nil}
     #{first_name} #{middle_name ? "#{middle_name} " : nil}
     #{last_name}#{suffix ? ", #{suffix}" : nil}"
  end

  def slots_greater_than_availabilities
    if slots.any?
      unless (slots.map(&:slot_length).sum / 60) > (availabilities.count * ENV['MIN_COURSE_LENGTH'].to_f)
        errors.add(:base, "Your slots available ust be greater than
                          #{availabilities.count * ENV['MIN_COURSE_LENGTH'].to_i} minutes"
        )
      end
    end
  end

  def verification_fields_if_required
    unless verification_required_changed?(from: false, to: true)
      if verification_required
        unless personal_id_number.present? && dob.present?
          errors.add(:base,
            'Please enter your Personal ID number (SSN) and date of birth'
          )
        end
      end
    end
  end

  def mailboxer_email(_object)
    email
  end

  def name
    "#{first_name} #{middle_name ? "#{middle_name} " : nil}#{last_name}"
  end

  def active_for_authentication?
    super && approved?
  end

  def inactive_message
    if !approved?
      :not_approved
    else
      super
    end
  end

  def self.send_reset_password_instructions(attributes = {})
    recoverable = find_or_initialize_with_errors(reset_password_keys, attributes, :not_found)
    if !recoverable.approved?
      recoverable.errors[:base] << I18n.t('devise.failure.not_approved')
    elsif recoverable.persisted?
      recoverable.send_reset_password_instructions
    end
    recoverable
  end

  def send_admin_mail
    AdminMailer.new_user_waiting_for_approval(self).deliver_now
  end

  def send_verification_mail
    UserMailer.verification_mail(self).deliver_now if verification_required_changed?(from: false, to: true)
  end

  # def send_interviewable_mail
  #   if can_interview_changed?(from: false, to: true)
  #     UserMailer.interviewable_welcome(self).deliver_now
  #   end
  # end

  def availability_grid(offset)
    offset_amount = Time.zone_offset(offset)
    hourly_grid = {}
    slots.each do |slot|
      start_time = slot.start_time + offset_amount
      end_time = slot.end_time + offset_amount
      hours_today = (start_time.hour..end_time.hour).to_a
      slot_days = hourly_grid[Slot.days[slot.day]]
      hourly_grid[Slot.days[slot.day]] = ((slot_days ? slot_days : []) + hours_today).uniq.sort
    end

    enrollments.each do |enrollment|
      schedule = enrollment.instance.schedule
      (1..3).to_a.each do |i|
        case schedule["day_#{i}"]
        when 'sunday'
          (hourly_grid[0] - (schedule["time_#{i}"] + offset_amount).hour) if hourly_grid[0]
        when 'monday'
          (hourly_grid[1] - (schedule["time_#{i}"] + offset_amount).hour) if hourly_grid[1]
        when 'tuesday'
          (hourly_grid[2] - (schedule["time_#{i}"] + offset_amount).hour) if hourly_grid[2]
        when 'wednesday'
          (hourly_grid[3] - (schedule["time_#{i}"] + offset_amount).hour) if hourly_grid[3]
        when 'thursday'
          (hourly_grid[4] - (schedule["time_#{i}"] + offset_amount).hour) if hourly_grid[4]
        when 'friday'
          (hourly_grid[5] - (schedule["time_#{i}"] + offset_amount).hour) if hourly_grid[5]
        when 'saturday'
          (hourly_grid[6] - (schedule["time_#{i}"] + offset_amount).hour) if hourly_grid[6]
        end
      end
    end
    hourly_grid
  end

  def next_seven_day_availability(offset)
    grid = availability_grid(offset)
    offset_amount = Time.zone_offset(offset)
    array = []
    grid.each do |day, times|
      times.each do |t|
        case day
        when 0
          day_word = 'sunday'
        when 1
          day_word = 'monday'
        when 2
          day_word = 'tuesday'
        when 3
          day_word = 'wednesday'
        when 4
          day_word = 'thursday'
        when 5
          day_word = 'friday'
        when 6
          day_word = 'saturday'
        end
        Chronic.time_class = ActiveSupport::TimeZone[offset_amount]
        array << Chronic.parse("next #{day_word} #{t}:00")
      end
    end
    array
  end

  def send_acceptance_mail
    if status_changed?(from: 'pending', to: 'approved') || status_changed?(from: 'interview', to: 'approved')
      case role
      when 'student'
        enrollments.first.status.active! if enrollments.any?
        UserMailer.acceptance_mail(self).deliver_now
        customer = Stripe::Customer.create(
          description: name,
          email: email
        )
        update_columns(stripe_customer_token: customer.id)
      when 'professor'
        UserMailer.professor_acceptance_mail(self).deliver_now
        account = Stripe::Account.create(
          managed: true,
          country: 'US',
          transfer_schedule: {
            interval: 'manual',
          },
          email: email,
          business_name: name
        )
        update_columns(stripe_account_token: account.id)
        # $close_io.update_opportunity(
        #   close_opportunity_id, status_id: 'stat_KZhc6V0S38dTyBh8QAlyECRZzH3GmJkNhVlbpSkJDX9',
        # confidence: professor? ? 50 : 70)
      end
    end
  end

  def available_interviewer
    user = taught_degree.enrolled_users.professor.approved.sample
    user ||= User.admin.first
    user
  end

  def send_interview_mail
    if status_changed?(from: 'pending', to: 'interview')
      if student?
        interviewer = available_interviewer
        UserMailer.interview_mail(self, interviewer).deliver_now
      else
        UserMailer.professor_interview_mail(self).deliver_now
      end
      # $close_io.update_opportunity(
      #   close_opportunity_id, status_id: 'stat_95tjaoTkMXCDi8bgP4KkNOWVeiKRlQYN4KCrGO4qHu0',
      # confidence: professor? ? 50 : 20)
    end
  end

  def member_of?(object)
    (object.user == self) || object.enrollments.map(&:user).include?(self)
  end

  def send_rejection_mail
    if status_changed?(from: 'pending', to: 'rejected')
      case role
      when 'student'
        enrollments.first.status.rejected! if enrollments.any?
        UserMailer.rejection_mail(self).deliver_now
      when 'professor'
        UserMailer.professor_rejection_mail(self).deliver_now
      end
      # $close_io.update_opportunity(
      #   close_opportunity_id, status_id: 'stat_ObuDzx5SIdExjsu4cSAVZEjDLPlDMpUYN0aF9IpflD3'
      # )
    end
  end

  def set_close_active
    # $close_io.update_opportunity(
    #    close_opportunity_id, status_id: 'stat_0zidCfu1nOhaCbVjfEFPAImy3S8SYC7uj76fYrvApLE'
    # ) if cards.any?
    update(close_active: true)
  end

  def update_stripe_details
    account = Stripe::Account.retrieve(stripe_account_token)
    account.legal_entity.first_name = first_name
    account.legal_entity.last_name = last_name
    account.legal_entity.type = 'individual'
    if dob
      account.legal_entity.dob.day = dob.strftime('%d').to_i
      account.legal_entity.dob.month = dob.strftime('%m').to_i
      account.legal_entity.dob.year = dob.strftime('%Y').to_i
    end
    account.legal_entity.address.city = city
    account.legal_entity.address.state = region
    account.legal_entity.address.postal_code = postcode
    account.legal_entity.address.line1 = street_address
    account.legal_entity.personal_id_number = personal_id_number if personal_id_number
    if sign_in_count < 2
      account.tos_acceptance.date = Time.current.to_i
      account.tos_acceptance.ip = last_sign_in_ip.to_s
    end
    begin
      account.save
    rescue StandardError
      nil
    end
    update(verification_required: false)
  end

  def temp_email?
    email !~ /\Achange@me/ ? false : true
  end

  # def create_close_opportunity
  #   lead = $close_io.list_leads(email).data.first
  #   lead ||= $close_io.create_lead(
  #     name: name,
  #     'custom.lcf_FGhjfjW86ah6k8DbeYdytnDwPMJVVFehpelTxN5mmdn' => taught_degree.try(:name),
  #     'custom.lcf_cfoYjKpjabJGezPkfHz5E0FRHG7mCDKnY2gVF58gq5w' => role.to_s,
  #     contacts: [{ name: name, emails: [{ type: 'personal', email: email }] }]
  #   )
  #   $close_io.update_lead(lead.id, status_id: 'stat_Lgc9s1Nop9OB2F7Ptv2E8ypIEvxksHuYtNDofzcdNWJ')
  #   opportunity = $close_io.create_opportunity(
  #     lead_id: lead.id,
  #     status_id: 'stat_BzE6XhtqfVlhuqFNlf7X69Oo0EvR5ktlCZ3fp7uMy2U',
  #     confidence: professor? ? 50 : 10,
  #     value: professor? ? 0 : 640_000,
  #     value_period: 'annual'
  #   )
  #   update(close_opportunity_id: opportunity.id)
  # end
  #
  # handle_asynchronously :create_close_opportunity

  def self.create_from_omniauth(email, params)
    attributes = {
      social_auth: true,
      email: email,
      password: Devise.friendly_token,
      first_name: params['info']['name'].split(' ').first,
      last_name: params['info']['name'].split(' ').last,
      finish_token: Devise.friendly_token,
    }

    create(attributes)
  end
end
