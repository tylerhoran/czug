# frozen_string_literal: true

class Article < ApplicationRecord
  extend FriendlyId
  validates :name, :body, presence: true
  friendly_id :name, use: :slugged
  has_attached_file :attachment,
    styles: { large: '1500x860#' },
    default_url: ActionController::Base.helpers.asset_path('defaults/photos_default.jpg')
  validates_attachment_content_type :attachment, content_type: %r{\Aimage\/.*\z}
end
