# frozen_string_literal: true

class Degree < ApplicationRecord
  extend FriendlyId
  validates :name, :degree_type, :description, presence: true
  has_many :courses, dependent: :destroy
  has_many :enrolled_users,
    class_name: 'User', foreign_key: 'taught_degree_id', dependent: :destroy,
    inverse_of: :taught_degree
  enum degree_type: %i[ba bs]
  friendly_id :name, use: :slugged
  has_and_belongs_to_many :divisions
end
