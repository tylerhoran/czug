# frozen_string_literal: true

class Activity < ApplicationRecord
  belongs_to :subject, polymorphic: true
  belongs_to :user
  validates :subject, :user, :direction, :name, presence: true
  after_create :notify_user, unless: :skip_callbacks

  def notify_user
    if user.activity_notifications
      case subject.class.name
      when 'Assignment'
        UserMailer.assignment_created(subject, user).deliver_later
      when 'Course'
        UserMailer.course_created(subject, user).deliver_later
      when 'Grade'
        UserMailer.grade_created(subject, user).deliver_later
      when 'Post'
        UserMailer.post_created(subject, user).deliver_later
      when 'Comment'
        UserMailer.comment_created(subject, user).deliver_later
      when 'Refund'
        UserMailer.refund_created(subject, user).deliver_later
      when 'Enrollment'
        UserMailer.enrollment_created(subject, user).deliver_later
      when 'Meeting'
        UserMailer.meeting_updated(subject, user).deliver_later
      when 'Reservation'
        UserMailer.reservation_removed(subject, user).deliver_later
      when 'Transfer'
        UserMailer.transfer_created(subject, user).deliver_later
      end
    end
  end
end
