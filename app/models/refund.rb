# frozen_string_literal: true

class Refund < ApplicationRecord
  belongs_to :user
  belongs_to :charge
  validates :user, :charge, :stripe_refund_token, presence: true

  before_validation :process_stripe, on: [:create], unless: :skip_callbacks

  def process_stripe
    if charge
      refund = Stripe::Refund.create(
        amount: amount,
        charge: charge.stripe_charge_token,
        requested_by_customer: 'requested_by_customer'
      )
    end
    self.stripe_refund_token = refund.try(:id)
  end
end
