# frozen_string_literal: true

class Evaluation < ApplicationRecord
  belongs_to :enrollment
  validates :enrollment, :rating, presence: true
  validates :rating, inclusion: { in: 1..10 }
end
