# frozen_string_literal: true

class Assignment < ApplicationRecord
  belongs_to :instance
  has_many :submissions, dependent: :destroy
  validates :instance, :name, presence: true
  after_create :notify_students, unless: :skip_callbacks

  def notify_students
    instance.enrollments.each do |enrollment|
      Activity.create(
        subject: self,
        name: 'assignment_added',
        direction: 'by',
        user: enrollment.user
      )
    end
  end
end
