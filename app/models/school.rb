# frozen_string_literal: true

class School < ApplicationRecord
  belongs_to :user
  validates :name, presence: true
end
