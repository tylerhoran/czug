# frozen_string_literal: true

class Work < ApplicationRecord
  belongs_to :user
  validates :name, presence: true
end
