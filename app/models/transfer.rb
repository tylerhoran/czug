# frozen_string_literal: true

class Transfer < ApplicationRecord
  belongs_to :card
  validates :card, :amount, :stripe_transfer_token, presence: true
  before_validation :process_stripe, on: [:create]
  after_create :notify_user, unless: :skip_callbacks
  delegate :user, to: :card, prefix: true
  delegate :last_four, :brand, :expiration_month, :expiration_year, to: :card, prefix: true

  def process_stripe
    if card
      transfer = Stripe::Transfer.create(
        amount: amount,
        currency: 'usd',
        destination: card_user.stripe_account_token,
        description: 'Weekly Transfer from the College of Brasden'
      )
      self.stripe_transfer_token = transfer.try(:id)
    end
  end

  def notify_user
    Activity.create(
      subject: self,
      name: 'transfer_added',
      direction: 'to',
      user: card_user
    )
  end
end
