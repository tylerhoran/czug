# frozen_string_literal: true

class Division < ApplicationRecord
  extend FriendlyId
  validates :name, presence: true
  friendly_id :name, use: :slugged
  has_and_belongs_to_many :degrees
end
