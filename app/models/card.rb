# frozen_string_literal: true

class Card < ApplicationRecord
  extend FriendlyId
  attr_accessor :token, :number, :expiration, :cvc, :currency
  belongs_to :user
  has_many :transfers, dependent: :destroy
  has_many :charges, dependent: :destroy
  validates :user, :card_type, :last_four, :expiration_month, :expiration_year, presence: true
  enum card_type: %i[credit debit]
  enum status: %i[active removed]
  enum brand: %i[visa mastercard amex jcb discover diners]

  before_create :process_stripe, on: [:create], unless: :skip_callbacks
  after_create :set_default, unless: :skip_callbacks
  after_destroy :set_default_destroyed, unless: :skip_callbacks

  before_create :set_default_card
  friendly_id :code, use: :slugged


  def code
    Digest::SHA1.hexdigest id.to_s
  end

  def set_default_card
    self.default ||= true
  end

  def process_stripe
    if user
      case card_type
      when 'credit'
        customer = Stripe::Customer.retrieve(user.stripe_customer_token)
        card_token = customer.sources.create(source: token)
        self.stripe_card_token = card_token.try(:id)
      when 'debit'
        account = Stripe::Account.retrieve(user.stripe_account_token)
        card_token = account.external_accounts.create(external_account: token)
        self.stripe_card_token = card_token.try(:id)
      end
    end
  end

  def set_default
    update(default: true) if user.cards.active.count == 1
  end

  def set_default_destroyed
    user.cards.first.update(default: true) if user.cards.count == 1
  end
end
