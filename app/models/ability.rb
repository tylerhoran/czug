# frozen_string_literal: true

class Ability
  include CanCan::Ability
  def initialize(user)
    user ||= User.new
    can :access, :rails_admin if user.admin?
    can :dashboard if user.admin?
    can :manage, :all if user.admin?
    can :read, Activity, user: user
    can :read, Assignment, instance: { id: user.enrollments.pluck(:instance_id) }
    can :read, Assignment, instance: { user: user }
    can :manage, Assignment, instance: { user: user }
    can :read, Availability, user: user
    can :manage, Availability, user: user
    can :read, Card, user: user
    can :manage, Card, user: user
    cannot :update, Card, user: user
    can :read, Charge, card: { user: user }
    can :create, Contact
    can :manage, Course, proposer: user
    can :read, Course
    can :read, Degree
    can :read, Division
    can :read, Enrollment, user: user
    can :read, Enrollment, instance: { user: user }
    can :manage, Enrollment, instance: { user: user }
    can :manage, Evaluation, enrollment: { id: user.enrollments.pluck(:id) }
    can :read, Faq
    can :read, Instance, id: user.enrollments.pluck(:instance_id)
    can :read, Instance, user: user
    can :manage, Instance, user: user
    can :read, Meeting, schedule: { instance: { id: user.enrollments.pluck(:instance_id) } }
    can :read, Meeting, schedule: { instance: { user: user } }
    can :manage, Meeting, schedule: { instance: { user: user } }
    can :read, Petition, user: user
    can :manage, Petition, user: user
    can :read, Post, instance: { id: user.enrollments.pluck(:instance_id) }
    can :read, Post, instance: { user: user }
    can :manage, Post, user: user
    can :manage, Post, instance: { user: user }
    can :read, Prerequisite
    can :manage, Prerequisite if user.professor?
    can :read, Refund, user: user
    can :read, Reservation, user: user
    can :read, Reservation, availability: { user_id: user.id }
    can :manage, Reservation, user: user
    can :manage, Reservation, availability: { user_id: user.id }
    can :read, Schedule, instance: { user: user }
    can :read, Schedule, instance: { enrollments: { user: user } }
    can :read, Slot, user: user
    can :manage, Slot, user: user
    can :read, Submission, user: user
    can :read, Submission, assignment: { instance: { user_id: user.id } }
    can :manage, Submission, user: user
    can :manage, Submission, assignment: { instance: { user_id: user.id } }
    can :read, Transfer, card: { user: user }
    can :read, User
    can :manage, User, id: user.id if user.persisted?
    can :reject, User if user.admin?
    can :approve, User if user.admin?
    can :interview, User if user.admin?
  end
end
