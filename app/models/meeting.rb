# frozen_string_literal: true

class Meeting < ApplicationRecord
  belongs_to :schedule
  default_scope { order('occurrence ASC') }
  scope :remaining, -> { where('occurrence >= ?', Time.current) }

  after_create :build_google_event, unless: :skip_callbacks
  after_update :update_google_event, unless: :skip_callbacks
  after_update :notify_student, unless: :skip_callbacks
  after_destroy :delete_google_event, unless: :skip_callbacks

  def google_attributes(e)
    e.title = schedule.instance.course_name
    e.description = Rails.application.routes.url_helpers.instance_meeting_url(schedule.instance,
      self,
      host: 'brasden.ch')
    e.location = 'Google Hangout'
    e.start_time = occurrence
    e.end_time = (occurrence + ENV['MIN_COURSE_LENGTH'].to_i.minutes)
    e.reminders = { 'useDefault' => false, 'overrides' => ['minutes' => 10, 'method' => 'popup'] }
    e.attendees = (schedule.instance.enrollments.active.map do |e|
                     {
                       'email' => e.user.email,
      'displayName' => e.user.name,
      'responseStatus' => 'accepted',
                     } end << {
      'email' => schedule.instance.user.email,
                    'displayName' => schedule.instance.user.name,
                    'responseStatus' => 'accepted',
    })
    e.guests_can_invite_others = false
    e.guests_can_see_other_guests = false
    e.transparency = 'transparent'
  end

  def build_google_event
    calendar.login_with_auth_code(ENV['GOOGLE_OAUTH_REFRESH_TOKEN'])
    event = calendar.create_event do |e|
      google_attributes(e)
    end
    update(google_id: event.id, hangout_link: event.raw['hangoutLink'])
  end

  def update_google_event
    calendar.login_with_auth_code(ENV['GOOGLE_OAUTH_REFRESH_TOKEN'])
    event = calendar.find_or_create_event_by_id(google_id) do |e|
      google_attributes(e)
    end
  end

  def delete_google_event
    event = calendar.find_or_create_event_by_id(google_id)
    event.delete
  end

  def calendar
    Google::Calendar.new(refresh_token: ENV['GOOGLE_OAUTH_REFRESH_TOKEN'],
                         client_id: ENV['GOOGLE_OAUTH_CLIENT_ID'],
                         client_secret: ENV['GOOGLE_OAUTH_CLIENT_SECRET'],
                         calendar: ENV['GOOGLE_CALENDAR_ID'],
                         redirect_url: 'urn:ietf:wg:oauth:2.0:oob')
  end

  def update_meeting_participants
    event = calendar.find_or_create_event_by_id(google_id) do |ev|
      ev.attendees = (schedule.instance.enrollments.map do |e|
        { 'email' => e.user.email,
          'displayName' => e.user.name,
          'responseStatus' => 'accepted' }
      end << {
        'email' => schedule.instance.user.email,
        'displayName' => schedule.instance.user.name,
        'responseStatus' => 'accepted',
      })
    end
  end

  def notify_student
    schedule.instance.enrollments.each do |enrollment|
      next unless schedule && occurrence_changed?
      Activity.create(
        subject: self,
        name: 'meeting_changed',
        direction: 'for',
        user: enrollment.user
      )
    end
  end

  def self.get_next_day(date, day_of_week)
    date + ((day_of_week - date.wday) % 7)
  end
end
