# frozen_string_literal: true

class Petition < ApplicationRecord
  belongs_to :user
  belongs_to :degree
  enum status: %i[pending approved rejected]
  validates :user, :degree, :status, presence: true
  before_validation :set_status
  after_create :notify_admin, unless: :skip_callbacks
  after_update :notify_student, unless: :skip_callbacks
  delegate :name, :degree_type, to: :degree, prefix: true

  def set_status
    self.status ||= 'pending'
  end

  def notify_admin
    AdminMailer.notify_petition(user, degree).deliver_now
  end

  def notify_student
    if status_changed?(from: 'pending', to: 'rejected')
      Activity.create(
        subject: self,
        name: 'petition_rejected',
        direction: 'to',
        user: user
      )
    elsif status_changed?(from: 'pending', to: 'approved')
      Activity.create(
        subject: self,
        name: 'petition_accepted',
        direction: 'to',
        user: user
      )
    end
  end
end
