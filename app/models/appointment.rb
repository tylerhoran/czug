# frozen_string_literal: true

class Appointment < ApplicationRecord
  belongs_to :user
  belongs_to :attendee, class_name: 'User'
  validates :attendee, :user, :time, presence: true
  after_create :send_invites
  # after_create :update_close_opportunity

  def calendar
    Google::Calendar.new(refresh_token: ENV['GOOGLE_OAUTH_REFRESH_TOKEN'],
                         client_id: ENV['GOOGLE_OAUTH_CLIENT_ID'],
                         client_secret: ENV['GOOGLE_OAUTH_CLIENT_SECRET'],
                         calendar: ENV['GOOGLE_CALENDAR_ID'],
                         redirect_url: 'urn:ietf:wg:oauth:2.0:oob')
  end

  def send_invites
    calendar.login_with_auth_code(ENV['GOOGLE_OAUTH_REFRESH_TOKEN'])
    event = calendar.create_event do |e|
      e.title = "Interview with #{attendee.name} at Brasden College"
      e.description = 'Please us the Google Hangouts Video link on this event'
      e.location = 'Google Hangout'
      e.start_time = time
      e.send_notifications = true
      e.end_time = time + 15.minutes
      e.reminders = { 'useDefault' => false, 'overrides' => ['minutes' => 10, 'method' => 'popup'] }
      e.attendees = [
        {
          'email' => user.email,
          'displayName' => user.name,
          'responseStatus' => 'needsAction',
        },
        {
          'email' => attendee.email,
          'displayName' => attendee.name,
          'responseStatus' => 'needsAction',
        },
      ]
      e.guests_can_invite_others = false
      e.guests_can_see_other_guests = false
      e.transparency = 'transparent'
    end
    update(google_id: event.id, hangout_link: event.raw['hangoutLink'])
  end

  # def update_close_opportunity
  #   $close_io.update_opportunity(attendee.close_opportunity_id,
  #      status_id: 'stat_KZhc6V0S38dTyBh8QAlyECRZzH3GmJkNhVlbpSkJDX9',
  #      confidence: 30)
  # end
  #
  # handle_asynchronously :update_close_opportunity
end
