# frozen_string_literal: true

class Charge < ApplicationRecord
  belongs_to :card
  belongs_to :instance
  has_many :enrollments, dependent: :destroy
  validates :card, :instance, :amount, :stripe_charge_token, presence: true
  before_validation :process_stripe, on: [:create], unless: :skip_callbacks
  after_create :send_receipt, unless: :skip_callbacks
  delegate :user, :stripe_card_token, to: :card, prefix: true
  delegate :user, to: :instance, prefix: true
  delegate :last_four, :brand, :expiration_month, :expiration_year, to: :card, prefix: true

  def send_receipt
    UserMailer.receipt_mail(self).deliver
  end

  def process_stripe
    charge = Stripe::Charge.create(
      amount: amount,
      currency: 'usd',
      customer: card_user.stripe_customer_token,
      card: card_stripe_card_token,
      description: 'Tuition for Brasden College',
      application_fee: (amount * ENV['BRASDEN_COMMISSION'].to_f).to_i,
      destination: instance_user.stripe_account_token
    )
    self.stripe_charge_token = charge.try(:id)
  end
end
