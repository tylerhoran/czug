# frozen_string_literal: true

class Reservation < ApplicationRecord
  belongs_to :user
  belongs_to :availability
  validates :availability, presence: true
  validates :availability, uniqueness: { scope: :user }
  after_create :notify_professor, unless: :skip_callbacks
  after_create :notify_new_reservation, unless: :skip_callbacks
  delegate :name, to: :user, prefix: true

  def notify_new_reservation
    Activity.create(
      subject: self,
      name: 'reservation_added',
      direction: 'by',
      user: availability.user
    )
  end

  def notify_professor
    if [10, 20, 24].include?(availability.reservations.count)
      UserMailer.notify_professor_of_reservations(
        availability.user, availability
      ).deliver_now
    end
  end
end
