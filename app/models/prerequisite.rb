# frozen_string_literal: true

class Prerequisite < ApplicationRecord
  belongs_to :course
  belongs_to :prior, class_name: 'Course'
  validates :course, :prior, presence: true
end
