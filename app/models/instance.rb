# frozen_string_literal: true

class Instance < ApplicationRecord
  extend FriendlyId
  belongs_to :user
  belongs_to :course
  belongs_to :availability
  has_many :enrollments, dependent: :destroy
  has_many :charges, dependent: :destroy
  has_many :posts, dependent: :destroy
  has_many :activities, as: :subject, dependent: :destroy
  has_many :assignments, dependent: :destroy
  has_many :meetings, through: :schedule
  validates :user, :course, :schedule, :syllabus, :availability, presence: true
  has_one :schedule, dependent: :destroy
  validate :debit_card
  validate :enrollment_limit
  enum status: %i[active completed]
  friendly_id :code, use: :slugged
  delegate :name, to: :course, prefix: true
  delegate :name, to: :user, prefix: true
  delegate :start_date, to: :schedule, prefix: true

  def code
    Digest::SHA1.hexdigest id.to_s
  end
  # validate :enrollment_base

  after_create :create_meetings, unless: :skip_callbacks
  after_create :remove_reservations, unless: :skip_callbacks
  after_create :update_karma, unless: :skip_callbacks
  after_update :update_completed_karma, unless: :skip_callbacks

  has_attached_file :syllabus, default_url: ActionController::Base.helpers.asset_path('defaults/documents_default.jpg')
  validates_attachment_content_type :syllabus,
    content_type: ['application/pdf', 'application/msword',
                   'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
                   'text/plain']

  accepts_nested_attributes_for :enrollments, reject_if: :all_blank, allow_destroy: true
  accepts_nested_attributes_for :schedule, reject_if: :all_blank, allow_destroy: true

  def update_karma
    user.update(karma: user.karma + 10)
  end

  def update_completed_karma
    user.update(karma: user.karma + 20) if status_changed?(from: 'active', to: 'completed')
  end

  def create_meetings
    times = []
    first = Meeting.get_next_day(schedule.start_date,
      Slot.days[schedule.day_1]) +
            schedule.time_1.seconds_since_midnight.seconds
    second = Meeting.get_next_day(schedule.start_date,
      Slot.days[schedule.day_2]) +
             schedule.time_1.seconds_since_midnight.seconds
    times << first
    times << second
    10.times do
      first += 7.days
      second += 7.days
      times << first
      times << second
    end
    times.each do |time|
      Meeting.create(
        schedule: schedule,
        occurrence: time
      )
    end
  end
  handle_asynchronously :create_meetings

  def remove_reservations
    availability.reservations.where(user: enrollments.map(&:user)).destroy_all
  end

  def debit_card
    errors.add(:base, 'Debit Card required to create course instance') unless user&.cards&.debit&.any?
  end

  # def enrollment_base
  #   errors.add(:base, 'A Course can not be launched without students') if enrollments.empty?
  # end

  def enrollment_limit
    errors.add(:base, 'Enrollment count is limited to 24 people. Please remove students.') if enrollments.length > 24
  end
end
