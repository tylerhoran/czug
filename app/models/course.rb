# frozen_string_literal: true

class Course < ApplicationRecord
  extend FriendlyId
  attr_accessor :seed
  belongs_to :degree
  validates :name, :degree, presence: true
  friendly_id :name, use: :slugged
  has_many :availabilities, dependent: :destroy
  has_many :prerequisites, dependent: :destroy
  has_many :prerequisite_courses, through: :prerequisites, source: :prior, dependent: :destroy
  belongs_to :proposer, class_name: 'User'
  after_update :send_acceptance_mail, unless: :skip_callbacks
  after_update :send_rejection_mail, unless: :skip_callbacks
  after_create :notify_admin, unless: :seed
  enum status: %i[pending approved rejected]
  delegate :name, to: :degree, prefix: true

  def notify_admin
    AdminMailer.course_proposal(self).deliver_later
  end

  def send_acceptance_mail
    UserMailer.course_approved(self).deliver_later if status_changed?(from: 'pending', to: 'approved')
  end

  def send_rejection_mail
    UserMailer.course_rejected(self).deliver_later if status_changed?(from: 'pending', to: 'rejected')
  end
end
