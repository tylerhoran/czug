# frozen_string_literal: true

class Provider < ApplicationRecord
  has_many :users, dependent: :destroy
  has_many :authentications, dependent: :destroy
end
