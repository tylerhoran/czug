# frozen_string_literal: true

class Post < ApplicationRecord
  belongs_to :user
  belongs_to :instance
  validates :user, :instance, :body, presence: true

  has_attached_file :attachment,
    styles: { medium: '300x300>', thumb: '100x100>' },
    default_url: ->(_image) { ActionController::Base.helpers.asset_path('defaults/photos_default.jpg') }
  validates_attachment_content_type :attachment, content_type: %r{\Aimage\/.*\z}

  after_create :notify_enrolled_users, unless: :skip_callbacks
  after_create :create_activity_for_students_and_prof, unless: :skip_callbacks
  delegate :name, to: :user, prefix: true

  def notify_enrolled_users
    users = instance.enrollments.active.map(&:user) << instance.user
    users.each do |u|
      UserMailer.instance_post(self, u).deliver_later unless u == user
    end
  end

  def create_activity_for_students_and_prof
    instance.enrollments.where('user_id != ?', user.id).find_each do |enrollment|
      Activity.create(
        subject: self,
        name: 'post_added',
        direction: 'by',
        user: enrollment.user
      )
    end
    unless user == instance.user
      Activity.create(
        subject: self,
        name: 'post_added',
        direction: 'by',
        user: instance.user
      )
    end
  end
end
