# frozen_string_literal: true

class Email < ApplicationRecord
  validates :name, presence: true
  enum status: %i[pending retargeted onboarded]

  before_create :set_status

  def set_status
    self.status = 'pending'
  end
end
