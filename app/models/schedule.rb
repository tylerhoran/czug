# frozen_string_literal: true

class Schedule < ApplicationRecord
  belongs_to :instance
  has_many :meetings, dependent: :destroy
  validates :day_1, :day_2, :time_1, :time_2, :start_date, presence: true
end
