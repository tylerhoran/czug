# frozen_string_literal: true

class Faq < ApplicationRecord
  validates :title, :body, :faq_type, presence: true
  enum faq_type: %i[general student professor]
end
