# frozen_string_literal: true

class Slot < ApplicationRecord
  belongs_to :user
  validates :end_time, :start_time, :day, presence: true
  enum day: %i[sunday monday tuesday wednesday thursday friday saturday]

  validate :greater_than_90_mins

  def greater_than_90_mins
    errors.add(:base, 'Slot must be greater than 90 minutes') unless slot_length > ENV['MIN_COURSE_LENGTH'].to_i.minutes
  end

  def slot_length
    if end_time && start_time
      if end_time < start_time
        ((end_time + 24.hours).to_f - start_time.to_f)
      else
        (end_time.to_f - start_time.to_f)
      end
    else
      0
    end
  end

  def day_of_week
    case day
    when 'sunday'
      0
    when 'monday'
      1
    when 'tuesday'
      2
    when 'wednesday'
      3
    when 'thursday'
      4
    when 'friday'
      5
    when 'saturday'
      6
    end
  end
end
