Highcharts.theme =
  colors: [
    '#D8C1AB'
    '#8085e9'
    '#8d4654'
    '#7798BF'
    '#aaeeee'
    '#ff0066'
    '#eeaaee'
    '#55BF3B'
    '#DF5353'
    '#7798BF'
    '#aaeeee'
  ]
  tooltip: borderWidth: 0
  chart: style: fontFamily: 'Montserrat'
  legend: itemStyle:
    fontWeight: 'bold'
    fontSize: '15px'
  xAxis: labels: style: color: '#66615b'
  yAxis: labels: style: color: '#66615b'
Highcharts.setOptions(Highcharts.theme)
