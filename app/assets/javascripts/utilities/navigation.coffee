searchVisible = 0
transparent = true
transparentDemo = true
fixedTop = false
navbar_initialized = false

debounce = (func, wait, immediate) ->
  timeout = undefined
  ->
    context = this
    args = arguments
    clearTimeout timeout
    timeout = setTimeout((->
      timeout = null
      if !immediate
        func.apply context, args
      return
    ), wait)
    if immediate and !timeout
      func.apply context, args
    return

document.addEventListener 'turbolinks:before-visit', ->
  $('html').removeClass 'nav-open'
  return
$(document).on 'turbolinks:load', ->
  searchVisible = 0
  transparent = true
  transparentDemo = true
  fixedTop = false
  navbar_initialized = false
  window_width = $(window).width()
  burger_menu = if $('nav[role="navigation-demo"]').hasClass('navbar-burger') then true else false
  # Init navigation toggle for small screens
  if window_width < 768 or burger_menu
    brasden.initRightMenu()
  # Make the images from the card fill the hole space
  brasden.fitBackgroundForCards()
  # Init popovers
  brasden.initPopovers()
  $('.dropdown-sharing .switch').click (e) ->
    # custom handling here
    e.stopPropagation()
    return
  return
  # activate collapse right menu when the windows is resized
  $(window).resize ->
    if $(window).width() < 768
      brasden.initRightMenu()
    if $(window).width() >= 768 and !burger_menu
      $('nav[role="navigation-demo"]').removeClass 'navbar-burger'
      brasden.misc.navbar_menu_visible = 0
      navbar_initialized = false
      $('html').removeClass 'nav-open'
    return
brasden =
  misc: navbar_menu_visible: 0
  initRightMenu: ->
    if !navbar_initialized
      $nav = $('nav[role="navigation-demo"]')
      $nav.addClass 'navbar-burger'
      $navbar = $nav.find('.navbar-collapse').first().clone(true)
      ul_content = ''
      $navbar.children('ul').each ->
        content_buff = $(this).html()
        ul_content = ul_content + content_buff
        return
      ul_content = '<ul class="nav navbar-nav">' + ul_content + '</ul>'
      $navbar.html ul_content
      $('body').append $navbar
      background_image = $navbar.data('nav-image')
      if background_image != undefined
        $navbar.css('background', 'url(\'' + background_image + '\')').removeAttr('data-nav-image').css('background-size', 'cover').addClass 'has-image'
      $toggle = $('.navbar-toggle')
      $navbar.find('a, button').removeClass 'btn btn-round btn-default btn-simple btn-neutral btn-fill btn-info btn-primary btn-success btn-danger btn-warning'
      $navbar.find('button').addClass 'btn-simple btn-block'
      $toggle.click ->
        if brasden.misc.navbar_menu_visible == 1
          $('html').removeClass 'nav-open'
          brasden.misc.navbar_menu_visible = 0
          $('#bodyClick').remove()
          setTimeout (->
            $toggle.removeClass 'toggled'
            return
          ), 550
        else
          setTimeout (->
            $toggle.addClass 'toggled'
            return
          ), 580
          div = '<div id="bodyClick"></div>'
          $(div).appendTo('body').click ->
            $('html').removeClass 'nav-open'
            brasden.misc.navbar_menu_visible = 0
            $('#bodyClick').remove()
            setTimeout (->
              $toggle.removeClass 'toggled'
              return
            ), 550
            return
          $('html').addClass 'nav-open'
          brasden.misc.navbar_menu_visible = 1
        return
      navbar_initialized = true
    return
  fitBackgroundForCards: ->
    $('.card').each ->
      if !$(this).hasClass('card-product') and !$(this).hasClass('card-user')
        image = $(this).find('.image img')
        image.hide()
        image_src = image.attr('src')
        $(this).find('.image').css
          'background-image': 'url(\'' + image_src + '\')'
          'background-position': 'center center'
          'background-size': 'cover'
      return
    return
  initPopovers: ->
    if $('[data-toggle="popover"]').length != 0
      $('body').append '<div class="popover-filter"></div>'
      #    Activate Popovers
      $('[data-toggle="popover"]').popover().on('show.bs.popover', ->
        $('.popover-filter').click ->
          $(this).removeClass 'in'
          $('[data-toggle="popover"]').popover 'hide'
          return
        $('.popover-filter').addClass 'in'
        return
      ).on 'hide.bs.popover', ->
        $('.popover-filter').removeClass 'in'
        return
    return
