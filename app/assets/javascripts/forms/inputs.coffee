$(document).on 'turbolinks:load', ->
  $('#email-show').click ->
    $('#email-show').hide()
    $('.email-show').show()
  $('[rel="tooltip"]').tooltip
    container: 'body'
    selector: 'body'
  $('.btn-tooltip').tooltip()
  $('.label-tooltip').tooltip()
  if $('.switch').length != 0
    $('.switch')['bootstrapSwitch']()
  #      Activate regular switches
  if $("[data-toggle='switch']").length != 0
    $("[data-toggle='switch']").wrap('<div class="switch" />').parent().bootstrapSwitch()
  if $('.selectpicker').length != 0
    $('.selectpicker').selectpicker()
  if $('.tagsinput').length != 0
    $('.tagsinput').tagsInput()
  if $('.datepicker').length != 0
    $('.datepicker').datepicker
      weekStart: 1
      color: '{color}'
      format: 'yyyy-mm-dd'
  $('.form-control').on('focus', ->
    $(this).parent('.input-group').addClass 'input-group-focus'
    return
  ).on 'blur', ->
    $(this).parent('.input-group').removeClass 'input-group-focus'
    return
