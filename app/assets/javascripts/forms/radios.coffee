### =============================================================
# flatui-radio v0.0.3
# ============================================================
###

!(($) ->

  ###RADIO PUBLIC CLASS DEFINITION
  # ==============================
  ###

  Radio = (element, options) ->
    @init element, options
    return

  Radio.prototype =
    constructor: Radio
    init: (element, options) ->
      $el = @$element = $(element)
      @options = $.extend({}, $.fn.radio.defaults, options)
      $el.before @options.template
      @setState()
      return
    setState: ->
      $el = @$element
      $parent = $el.closest('.radio')
      $el.prop('disabled') and $parent.addClass('disabled')
      $el.prop('checked') and $parent.addClass('checked')
      return
    toggle: ->
      d = 'disabled'
      ch = 'checked'
      $el = @$element
      checked = $el.prop(ch)
      $parent = $el.closest('.radio')
      $parentWrap = if $el.closest('form').length then $el.closest('form') else $el.closest('body')
      $elemGroup = $parentWrap.find(':radio[name="' + $el.attr('name') + '"]')
      e = $.Event('toggle')
      if $el.prop(d) == false
        $elemGroup.not($el).each ->
          `var $el`
          `var $parent`
          $el = $(this)
          $parent = $(this).closest('.radio')
          if $el.prop(d) == false
            $parent.removeClass(ch) and $el.removeAttr(ch).trigger('change')
          return
        if checked == false
          $parent.addClass(ch) and $el.prop(ch, true)
        $el.trigger e
        if checked != $el.prop(ch)
          $el.trigger 'change'
      return
    setCheck: (option) ->
      ch = 'checked'
      $el = @$element
      $parent = $el.closest('.radio')
      checkAction = if option == 'check' then true else false
      checked = $el.prop(ch)
      $parentWrap = if $el.closest('form').length then $el.closest('form') else $el.closest('body')
      $elemGroup = $parentWrap.find(':radio[name="' + $el['attr']('name') + '"]')
      e = $.Event(option)
      $elemGroup.not($el).each ->
        `var $el`
        `var $parent`
        $el = $(this)
        $parent = $(this).closest('.radio')
        $parent.removeClass(ch) and $el.removeAttr(ch)
        return
      if $parent[if checkAction then 'addClass' else 'removeClass'](ch) and checkAction then $el.prop(ch, ch) else $el.removeAttr(ch)
      $el.trigger e
      if checked != $el.prop(ch)
        $el.trigger 'change'
      return

  ###RADIO PLUGIN DEFINITION
  # ========================
  ###

  old = $.fn.radio

  $.fn.radio = (option) ->
    @each ->
      $this = $(this)
      data = $this.data('radio')
      options = $.extend({}, $.fn.radio.defaults, $this.data(), typeof option == 'object' and option)
      if !data
        $this.data 'radio', data = new Radio(this, options)
      if option == 'toggle'
        data.toggle()
      if option == 'check' or option == 'uncheck'
        data.setCheck option
      else if option
        data.setState()
      return

  $.fn.radio.defaults = template: '<span class="icons"><span class="first-icon fa fa-circle-o fa-base"></span><span class="second-icon fa fa-dot-circle-o fa-base"></span></span>'

  ###RADIO NO CONFLICT
  # ==================
  ###

  $.fn.radio.noConflict = ->
    $.fn.radio = old
    this

  ###RADIO DATA-API
  # ===============
  ###

  $(document).on 'click.radio.data-api', '[data-toggle^=radio], .radio', (e) ->
    $radio = $(e.target)
    e and e.preventDefault() and e.stopPropagation()
    if !$radio.hasClass('radio')
      $radio = $radio.closest('.radio')
    $radio.find(':radio').radio 'toggle'
    return
  $ ->
    $('[data-toggle="radio"]').each ->
      $radio = $(this)
      $radio.radio()
      return
    return
  return
)(window.jQuery)
