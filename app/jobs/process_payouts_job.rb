# frozen_string_literal: true

class ProcessPayoutsJob < ApplicationJob
  queue_as :default

  def perform
    # Getting meetings in the last week
    meetings = Meeting.where('meetings.occurrence < ? and
                              meetings.occurrence >= ?', Time.current, (Time.current - 1.week))
    schedules = Schedule.where(id: meetings.pluck(:schedule_id))
    instances = Instance.where(id: schedules.pluck(:instance_id))
    professors = []

    instances.each do |instance|
      professors << {
        instance.user_id => (instance.enrollments.active.count * (((ENV['COURSE_PRICE'].to_i * 0.971) - 0.30) / 10)),
      }
    end

    professors ||= []

    # User id & payout amount
    professors = professors.inject { |prof, enrol| prof.merge(enrol) { |_k, old_v, new_v| old_v + new_v } }
    professors ||= []
    transfers = []
    begin
      professors.each do |u, amount|
        next unless amount > 0
        user = User.find(u)
        final_amount = amount - (amount.to_f * ENV['BRASDEN_VAT'].to_f) - (amount.to_f * ENV['BRASDEN_COMMISSION'].to_f)
        transfer = Transfer.create!(card: user.primary_card, amount: (final_amount * 100).to_i)
        transfers << transfer
        UserMailer.transfer_created(instances.where(user: user), transfer, amount).deliver_now
      end

      # setting how much has been paid for each enrollment
      instances.each do |instance|
        instance.enrollments.each do |enrollment|
          enrollment.update!(transfer_amount: enrollment.transfer_amount.to_i + (ENV['COURSE_PRICE'].to_i / 10))
        end
      end
    rescue StandardError => errors
      Rails.logger.debug errors
    end
    AdminMailer.transfer_summary(transfers, professors.count, @errors).deliver_now
  end
end
