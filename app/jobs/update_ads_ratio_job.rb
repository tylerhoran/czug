# frozen_string_literal: true
# class UpdateAdsRatioJob < ApplicationJob
#   queue_as :default
#
#   def perform
#     facebook = Koala::Facebook::OAuth.new(ENV['FACEBOOK_CLIENT_ID'], ENV['FACEBOOK_CLIENT_SECRET'])
#     new_token = facebook.exchange_access_token_info(ENV['FACEBOOK_ACCESS_TOKEN'])
#     Zuck.graph = Koala::Facebook::API.new(new_token['access_token'], ENV['FACEBOOK_CLIENT_SECRET'])
#     account = Zuck::AdAccount.all.first
#     campaigns = account.campaigns
#     professor_prospects = 0
#
#     Degree.find_each do |degree|
#
#       ideal_student_to_prof_ratio = ENV['IDEAL_STUDENT_RATIO'].to_f
#       students = degree.enrolled_users.student
#       accepted_students = students.approved
#       pending_students = students.pending
#       student_acceptance_rate = accepted_students.length / students.length # 0.10
#
#       professors = degree.enrolled_users.professor
#       accepted_professors = professors.approved
#       professor_acceptance_rate = accepted_professors.length / professors.length # 0.10
#
#       student_signups_this_week = students.where("created_at < ?", 1.week.ago).length # 100
#       professor_signups_this_week = students.where("created_at < ?", 1.week.ago).length # 20
#
#       student_prospects_amount = student_signups_this_week * student_acceptance_rate # 100 * 0.10 = 10
#       professor_prospects_amount = professor_signups_this_week * professor_acceptance_rate # 20 * 0.10 = 2
#       degree_balance_ratio = professor_prospects_amount / student_prospects_amount
#
#       # simple first time around
#       # too many professors in this degree
#       #professor_campaign.decrease_by_10_percent if degree_balance_ratio > 1
#       #professor_prospects += professor_prospects_amount
#     end
#     # student_acceptance_rate = User.student.approved.length / User.student.length
#     # total_student_prospects = User.student.where("created_at < ?", 1.week.ago).length * student_acceptance_rate
#   end
# end
