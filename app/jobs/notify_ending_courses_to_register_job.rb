# frozen_string_literal: true

class NotifyEndingCoursesToRegisterJob < ApplicationJob
  queue_as :default

  def perform
    Instance.includes(:meetings).active.each do |instance|
      next unless instance.meetings.remaining.length < 3
      instance.enrollments.each do |e|
        UserMailer.register_prompt(e.user, instance).deliver_now
      end
    end
  end
end
