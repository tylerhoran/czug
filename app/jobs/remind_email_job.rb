# frozen_string_literal: true

class RemindEmailJob < ApplicationJob
  queue_as :default

  def perform
    Email.pending.each do |email|
      if User.find_by(email: email.name)
        email.onboarded!
      else
        UserMailer.retarget_email(email).deliver_now
        email.retargeted!
      end
    end
  end
end
