# frozen_string_literal: true

class PromptEvaluationsJob < ApplicationJob
  queue_as :default

  def perform
    Instance.active.each do |instance|
      instance.enrollments.each do |enrollment|
        UserMailer.prompt_evaluation(enrollment).deliver_now
      end
    end
  end
end
