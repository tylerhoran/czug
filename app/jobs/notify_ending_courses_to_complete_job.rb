# frozen_string_literal: true

class NotifyEndingCoursesToCompleteJob < ApplicationJob
  queue_as :default

  def perform
    Instance.includes(:meetings).active.each do |instance|
      if instance.meetings.remaining.empty?
        UserMailer.grade_prompt(instance.user, instance).deliver_now
        instance.completed!
      end
    end
  end
end
