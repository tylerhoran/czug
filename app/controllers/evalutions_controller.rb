# frozen_string_literal: true

class EvaluationsController < ApplicationController
  before_action :authenticate_user!
  before_action :check_student!
  before_action :set_enrollment
  add_breadcrumb 'Dashboard', :authenticated_root_path
  load_and_authorize_resource

  def new
    add_breadcrumb 'Evaluation', new_enrollment_evaluation_path(@enrollment)
    @evaluation = @enrollment.evaluations.build
  end

  def create
    @evaluation = @enrollment.evaluations.build(evaluation_params)
    if @evaluation.save
      redirect_to root_path, notice: 'Thanks. You evaluation was recieved'
    else
      render :new
    end
  end

  private

  def set_enrollment
    @enrollment = Enrollment.find(params[:enrollment_id])
  end

  def evaluation_params
    params.require(:evaluation).permit(:enrollment_id, :rating)
  end
end
