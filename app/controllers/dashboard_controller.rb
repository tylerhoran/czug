# frozen_string_literal: true

class DashboardController < ApplicationController
  before_action :authenticate_user!
  before_action :go_to_admin_home, only: [:index]
  before_action :check_admin!, only: [:activities]
  before_action :check_for_card, only: [:index]
  add_breadcrumb 'Dashboard', :authenticated_root_path

  def index
    @activities = current_user.activities.order('created_at desc').page(params[:page]).per(20)
  end

  def activities
    @degrees = Degree.includes(:enrolled_users).all
    @names = @degrees.pluck(:name)
    # Number of outstanding applications in each degree by stage
    # Number of enrollable courses in each degree
    # Number of Active Courses
  end

  private

  def go_to_admin_home
    redirect_to activities_dashboard_index_path if current_user.admin?
  end
end
