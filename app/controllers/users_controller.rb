# frozen_string_literal: true

class UsersController < ApplicationController
  before_action :authenticate_user!, except: %i[finish_signup complete_signup]
  before_action :set_user
  before_action :check_for_card, only: %i[professor_welcome student_welcome]
  before_action :authenticate_interviewable!, only: %i[reject interview approve]
  before_action :check_finish_token, only: [:complete_signup]
  load_and_authorize_resource except: %i[finish_signup complete_signup]

  def show; end

  def update
    if @user.update(user_params)
      flash[:success] = 'Your account has been updated'
      redirect_to root_path
    else
      case URI(request.referer).path
      when '/reservations'
        flash[:error] = 'Your course reservations could not be saved'
        render 'reservations/index', template: 'reservations/index'
      when '/availabilities'
        flash[:error] = 'Your course availabilities could not be saved'
        render 'availabilities/index', template: 'availabilities/index'
      when '/slots'
        flash[:error] = 'Your availability could not be saved'
        render 'slots/index', template: 'slots/index'
      end
    end
  end

  def finish_signup; end

  def complete_signup
    if @user.update(user_params)
      @user.send_admin_mail
      redirect_to thanks_path
    else
      render :finish_signup
    end
  end

  def reject
    @user.rejected!
  end

  def interview
    @user.interview!
  end

  def approve
    @user.approved!
  end

  def professor_welcome
    add_breadcrumb 'Dashboard', :authenticated_root_path
    add_breadcrumb 'Welcome', professor_welcome_user_path(@user)
  end

  def student_welcome
    add_breadcrumb 'Dashboard', :authenticated_root_path
    add_breadcrumb 'Welcome', student_welcome_user_path(@user)
  end

  private

  def check_finish_token
    redirect_to root_path unless user_params && @user.finish_token == user_params['finish_token']
  end

  def authenticate_interviewable!
    redirect_to root_path unless current_user.admin? || current_user.can_interview
  end

  def user_params
    params.require(:user).permit(:finish_token, :taught_degree_id, :previous_transcript, :email,
      reservations_attributes: %i[id availability_id user_id _destroy],
      availabilities_attributes: %i[id _destroy course_id],
      slots_attributes: %i[id start_time end_time day user_id _destroy])
  end

  def set_user
    @user = User.friendly.find(params[:id])
  end
end
