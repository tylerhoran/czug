# frozen_string_literal: true

class ChargesController < ApplicationController
  before_action :authenticate_user!
  before_action :check_for_card
  before_action :check_student!
  add_breadcrumb 'Dashboard', :authenticated_root_path
  load_and_authorize_resource

  def index
    add_breadcrumb 'Receipts', :charges_path
    @charges = current_user.charges
  end
end
