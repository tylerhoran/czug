# frozen_string_literal: true

class ConversationsController < ApplicationController
  before_action :authenticate_user!
  before_action :check_for_card
  before_action :set_mailbox
  before_action :set_conversation, except: [:index]
  add_breadcrumb 'Dashboard', :authenticated_root_path

  def index
    add_breadcrumb 'Conversations', :conversations_path
    @conversations = @mailbox.conversations.page(params[:page]).per(10)
  end

  def show
    add_breadcrumb 'Conversations', :conversations_path
    add_breadcrumb("Conversation with #{@conversation.participants.find do |s|
      s.id != current_user.id
    end.try(:name)}", conversation_path(@conversation))
  end

  def reply
    current_user.reply_to_conversation(@conversation, params[:body])
    flash[:success] = 'Reply sent'
    redirect_to conversation_path(@conversation)
  end

  private

  def set_conversation
    @conversation ||= @mailbox.conversations.find(params[:id])
  end

  def set_mailbox
    @mailbox ||= current_user.mailbox
  end
end
