# frozen_string_literal: true

class Users::SessionsController < Devise::SessionsController
  skip_before_action :verify_authenticity_token, only: [:create]

  def create
    super
  end

  def after_sign_in_path_for(resource_or_scope)
    if resource_or_scope.cards.any? || resource_or_scope.admin?
      stored_location_for(resource_or_scope) || signed_in_root_path(resource_or_scope)
    else
      setup_path(:profile)
    end
  end
end
