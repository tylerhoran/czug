# frozen_string_literal: true

class Users::RegistrationsController < Devise::RegistrationsController
  before_action :configure_permitted_parameters
  before_action :set_role, only: [:new]

  def new
    super
  end

  def update
    super
    resource.update_stripe_details if resource.professor? && resource.valid?
  end

  protected

  def set_role
    session[:prof] = 'prof' if params[:professor]
  end

  def after_inactive_sign_up_path_for(_resource)
    thanks_path
  end

  def after_update_path_for(_resource)
    edit_user_registration_path
  end

  def update_resource(resource, params)
    resource.update_without_password(params)
  end

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:account_update,
      keys: %i[prefix first_name middle_name last_name suffix calendly_link
               photo previous_transcript level_id last_degree_name last_degree_major
               password password_confirmation email current_password activity_notifications
               messages_notifications personal_id_number newsletter_notifications
               street_address city region postcode country dob]
    )
    devise_parameter_sanitizer.permit(:sign_up, keys: %i[prof prefix full_name
                                                         suffix vita previous_transcript
                                                         level_id last_degree_name last_degree_major
                                                         taught_degree_id]
    )
  end
end
