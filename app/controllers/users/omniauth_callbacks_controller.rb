# frozen_string_literal: true

class Users::OmniauthCallbacksController < Devise::OmniauthCallbacksController
  def facebook
    create
  end

  private

  def create
    auth_params = request.env['omniauth.auth']
    provider = Provider.where(name: auth_params.provider).first
    authentication = provider.authentications.where(uid: auth_params.uid).first
    email = auth_params['info']['email']
    email ||= "change@me-#{auth_params['extra']['raw_info']['id']}-#{provider.name}.com"
    existing_user = current_user || User.find_by(email: email)
    if authentication
      sign_in_with_existing_authentication(authentication)
    elsif existing_user
      create_authentication_and_sign_in(auth_params, existing_user, provider)
    else
      create_user_and_authentication_and_sign_in(email, auth_params, provider)
    end
  end

  def sign_in_with_existing_authentication(authentication)
    sign_in_and_redirect(:user, authentication.user)
  end

  def create_authentication_and_sign_in(auth_params, user, provider)
    Authentication.create_from_omniauth(auth_params, user, provider)
    session[:finish_token] = user.finish_token
    redirect_to finish_signup_user_path(user)
  end

  def create_user_and_authentication_and_sign_in(email, auth_params, provider)
    user = User.create_from_omniauth(email, auth_params)
    create_authentication_and_sign_in(auth_params, user, provider)
  end
end
