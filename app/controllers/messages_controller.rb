# frozen_string_literal: true

class MessagesController < ApplicationController
  before_action :authenticate_user!
  before_action :check_for_card
  add_breadcrumb 'Dashboard', :authenticated_root_path
  add_breadcrumb 'Conversations', :conversations_path

  def new
    add_breadcrumb 'New Conversation', :new_message_path
  end

  def create
    recipients = User.where(id: params['recipients'])
    conversation = current_user.send_message(recipients,
      params[:message][:body],
      params[:message][:subject]).conversation
    flash[:success] = 'Message has been sent!'
    redirect_to conversation_path(conversation)
  end
end
