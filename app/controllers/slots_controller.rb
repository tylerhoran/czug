# frozen_string_literal: true

class SlotsController < ApplicationController
  before_action :authenticate_user!
  before_action :check_for_card
  add_breadcrumb 'Dashboard', :authenticated_root_path
  load_and_authorize_resource

  def index
    add_breadcrumb 'Availability', slots_path
  end
end
