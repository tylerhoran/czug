# frozen_string_literal: true

class ReservationsController < ApplicationController
  before_action :authenticate_user!
  before_action :check_for_card
  before_action :check_student!
  add_breadcrumb 'Dashboard', :authenticated_root_path
  load_and_authorize_resource

  def index
    add_breadcrumb 'Registration', reservations_path
  end

  def destroy
    reservation = Reservation.find(params[:id])
    reservation.destroy!
    flash[:success] = 'Reservation has been removed'
    redirect_to root_path
  end
end
