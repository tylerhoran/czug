# frozen_string_literal: true

class TransfersController < ApplicationController
  before_action :authenticate_user!
  before_action :check_for_card
  before_action :check_professor!
  add_breadcrumb 'Dashboard', :authenticated_root_path
  load_and_authorize_resource

  def index
    add_breadcrumb 'Transfers', :transfers_path
    @transfers = current_user.transfers
  end
end
