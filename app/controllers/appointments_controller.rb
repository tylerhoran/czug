# frozen_string_literal: true

class AppointmentsController < ApplicationController
  before_action :set_user
  before_action :set_attendee, only: :new
  before_action :set_appointment, only: [:destroy]

  def new
    @appointment = @user.appointments.build
  end

  def create
    @appointment = @user.appointments.build(appointment_params)
    if @appointment.save
      redirect_to user_appointment_path(@user, @appointment)
    else
      redirect_to :back
    end
  end

  def destroy
    @appointment.destroy
  end

  private

  def set_user
    @user = User.friendly.find(params[:user_id])
  end

  def set_appointment
    @appointment = @user.appointments.find(params[:id])
  end

  def appointment_params
    params.require(:appointment).permit(:time, :attendee_id)
  end

  def set_attendee
    session[:attendee] = params[:attendee] if params[:attendee]
    @attendee = User.find_by(id: session[:attendee])
    redirect_to root_path 'Not a valid user' unless @attendee
  end
end
