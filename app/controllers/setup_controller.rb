# frozen_string_literal: true

class SetupController < ApplicationController
  before_action :authenticate_user!
  include Wicked::Wizard

  steps :profile, :first_courses, :teachable_courses, :availability, :payment, :transfer

  def show
    @user = current_user
    case step
    when :first_courses
      jump_to(:teachable_courses) if @user.professor?
      @divisions = Division.includes(degrees: :courses)
    when :teachable_courses
      jump_to(:availability) if @user.student?
      @courses = @user.taught_degree.courses.approved if @user.professor?
      @user.availabilities.build if @user.professor? && @user.availabilities.none?
    when :availability
      @user.slots.build(day: 'sunday')
    when :payment
      @user.cards.build unless @user.cards.any?
      jump_to(:transfer) if @user.professor?
    when :transfer
      @user.cards.build unless @user.cards.any?
      @user.banks.build unless @user.banks.any?
      skip_step if @user.student?
    end
    render_wizard
  end

  def update
    @user = current_user
    mod_params = user_params
    case step
    when :availability
      mod_params[:slots_attributes]&.each do |_num, atts|
        atts[:start_time] = atts[:start_time].gsub(atts[:start_time], "#{atts[:start_time]} #{Time.current.zone}")
        atts[:end_time] = atts[:end_time].gsub(atts[:end_time], "#{atts[:end_time]} #{Time.current.zone}")
      end
      @user.update(mod_params)
    when :transfer
      @user.update(mod_params)
      @user.update_stripe_details
    when :payment
      @user.update(mod_params)
      @user.cards.build unless @user.cards.any?
    else
      @user.update(mod_params)
    end
    render_wizard @user
  end

  def finish_wizard_path
    current_user.professor? ? professor_welcome_user_path(current_user) : student_welcome_user_path(current_user)
  end

  def user_params
    params.require(:user).permit(:step, :prefix, :first_name, :middle_name,
      :last_name, :ssn_last_4, :suffix, :photo, :street_address, :city,
      :region, :postcode, :country, :dob,
      availabilities_attributes: %i[id course_id user_id _destroy],
      slots_attributes: %i[id start_time end_time day user_id _destroy],
      cards_attributes: %i[id name token card_type currency number expiration
                           expiration_month last_four expiration_year cvc _destroy],
      reservations_attributes: %i[id _destroy availability_id]
    )
  end
end
