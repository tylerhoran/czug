# frozen_string_literal: true

class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  rescue_from ActiveRecord::RecordNotFound do
    flash[:warning] = 'Resource not found.'
    redirect_back_or root_path
  end

  def redirect_back_or(path)
    redirect_to request.referer || path
  end

  def check_for_card
    redirect_to setup_path(:profile) unless current_user.cards.any?
  end

  def check_professor!
    redirect_to authenticated_root_path unless current_user.professor?
  end

  def check_student!
    redirect_to authenticated_root_path unless current_user.student?
  end

  def check_admin!
    redirect_to authenticated_root_path unless current_user.admin?
  end

  rescue_from CanCan::AccessDenied do |exception|
    respond_to do |format|
      format.json { head :forbidden, content_type: 'text/html' }
      format.html { redirect_to '/', notice: exception.message }
      format.js   { head :forbidden, content_type: 'text/html' }
    end
  end
end
