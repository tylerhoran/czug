# frozen_string_literal: true

class DegreesController < ApplicationController
  before_action :set_division
  before_action :set_degree, only: [:show]
  load_and_authorize_resource

  def show
    @courses = @degree.courses.approved
  end

  private

  def set_degree
    @degree = @division.degrees.friendly.find(params[:id])
  end

  def set_division
    @division = Division.friendly.find(params[:division_id])
  end
end
