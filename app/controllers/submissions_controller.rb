# frozen_string_literal: true

class SubmissionsController < ApplicationController
  before_action :authenticate_user!
  before_action :check_for_card
  before_action :set_instance
  before_action :set_assignment
  before_action :set_submission, only: [:update]
  before_action :authorize_instance!

  def new
    add_breadcrumb 'Dashboard', :authenticated_root_path
    add_breadcrumb @instance.course_name, instance_path(@instance)
    add_breadcrumb 'New Submission', new_instance_assignment_submission_path(@instance, @assignment)
    @submission ||= @assignment.submissions.build
  end

  def create
    @submission = @assignment.submissions.build(submission_params)
    @submission.user = current_user
    if @submission.save
      flash[:success] = 'Your submission was uploaded successfully'
      redirect_to instance_assignment_path(@instance, @assignment)
    else
      render :new
    end
  end

  def update
    respond_to do |format|
      @submission.update(submission_params)
      format.html { redirect_to :back, notice: 'Grade updated' }
      format.json { respond_with_bip(@submission) }
    end
  end

  private

  def submission_params
    params.require(:submission).permit(:body, :attachment, :grade)
  end

  def set_instance
    @instance = Instance.friendly.find(params[:instance_id])
  end

  def set_submission
    @submission = @assignment.submissions.find(params[:id])
  end

  def authorize_instance!
    redirect_to root_path unless current_user.member_of?(@instance)
  end

  def set_assignment
    @assignment = @instance.assignments.find(params[:assignment_id])
  end
end
