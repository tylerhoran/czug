# frozen_string_literal: true

class CardsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_card, only: :destroy
  add_breadcrumb 'Dashboard', :authenticated_root_path
  load_and_authorize_resource

  def new
    add_breadcrumb 'New Card', new_card_path
    @card ||= current_user.cards.build
  end

  def create
    @card = current_user.cards.build(card_params)
    if @card.save
      redirect_to edit_user_registration_path
    else
      render :new
    end
  end

  def destroy
    @card.update(status: 'removed', default: false)
    redirect_to edit_user_registration_path
  end

  private

  def set_card
    @card = current_user.cards.find(params[:id])
  end

  def card_params
    params.require(:card).permit(:name, :token, :card_type, :currency, :number,
      :expiration, :expiration_month, :last_four, :expiration_year, :cvc
    )
  end
end
