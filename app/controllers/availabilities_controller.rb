# frozen_string_literal: true

class AvailabilitiesController < ApplicationController
  before_action :authenticate_user!
  before_action :check_for_card
  before_action :check_professor!
  before_action :set_availability, only: %i[show update destroy]
  add_breadcrumb 'Dashboard', :authenticated_root_path
  load_and_authorize_resource

  def index
    add_breadcrumb 'Enrollable courses', availabilities_path
    @user = current_user
  end

  def show
    add_breadcrumb "Enrolling: #{@availability.course_name}", availability_path(@availability)
  end

  def update
    if @availability.update(availability_params) && @availability.instances.any?
      redirect_to confirmed_instance_path(@availability.instances.first)
    else
      render :show
    end
  end

  def destroy
    @availability.destroy
    redirect_to root_path
  end

  private

  def set_availability
    @availability = current_user.availabilities.includes(reservations: :user).find(params[:id])
  end

  def availability_params
    params.require(:availability).permit(instances_attributes: [:id, :_destroy, :user_id, :course_id, :syllabus,
                                                                schedule_attributes: %i[id _destroy start_date day_1
                                                                                        time_1 day_2 time_2
                                                                                        day_3 time_3],
                                                                enrollments_attributes: %i[user_id status id _destroy]])
  end
end
