# frozen_string_literal: true

class AssignmentsController < ApplicationController
  before_action :authenticate_user!
  before_action :check_for_card
  before_action :check_professor!, only: [:show]
  before_action :set_instance
  before_action :set_assignment, except: %i[create new]
  before_action :set_breadcrumbs
  load_and_authorize_resource except: [:new]

  def new
    authorize! :manage, @instance
    add_breadcrumb 'New Assignment', new_instance_assignment_path(@instance)
    @assignment ||= @instance.assignments.build
  end

  def show
    add_breadcrumb 'Submissions', instance_assignment_path(@instance, @assignment)
  end

  def edit
    add_breadcrumb 'Edit', edit_instance_assignment_path(@instance, @assignment)
  end

  def create
    @assignment = @instance.assignments.build(assignment_params)
    if @assignment.save
      flash[:success] = 'Your assignment was created successfully'
      redirect_to instance_path(@instance)
    else
      render :new
    end
  end

  def update
    respond_to do |format|
      if @assignment.update(assignment_params)
        format.html do
          redirect_to(instance_assignment_path(@instance, @assignment),
            notice: 'Assignment was successfully updated.')
        end
      else
        format.html { render :edit }
      end
      format.json { respond_with_bip(@assignment) }
    end
  end

  def destroy
    @assignment.destroy
    redirect_to instance_path(@instance)
  end

  private

  def set_breadcrumbs
    add_breadcrumb 'Dashboard', :authenticated_root_path
    add_breadcrumb @instance.course_name, instance_path(@instance)
  end

  def set_instance
    @instance = Instance.friendly.find(params[:instance_id])
  end

  def set_assignment
    @assignment = @instance.assignments.find(params[:id])
  end

  def assignment_params
    params.require(:assignment).permit(:name, :body, :attachment, :instance_id)
  end
end
