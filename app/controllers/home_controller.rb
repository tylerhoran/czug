# frozen_string_literal: true

class HomeController < ApplicationController
  def index
    @email = Email.new
  end

  def about; end

  def privacy; end

  def terms; end

  def professor_terms; end

  def faq
    @faqs = Faq.all
  end

  def tuition; end

  def academics; end

  def thanks; end

  def blog
    @articles = Article.all.order('created_at desc').page(params[:page]).per(5)
  end

  def cookies_policy; end

  def error
    fail('halp!')
  end

  def sitemap
    path = Rails.root.join('public', 'sitemaps', 'sitemap.xml')
    if File.exist?(path)
      render xml: open(path).read
    else
      render text: 'Sitemap not found.', status: :not_found
    end
  end

  def robots; end
end
