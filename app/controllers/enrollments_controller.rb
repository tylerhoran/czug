# frozen_string_literal: true

class EnrollmentsController < ApplicationController
  before_action :authenticate_user!
  before_action :check_for_card
  before_action :set_enrollment, only: [:update]

  add_breadcrumb 'Dashboard', :authenticated_root_path
  load_and_authorize_resource

  def index
    add_breadcrumb 'Transcript', :enrollments_path
    @enrollments = current_user.enrollments
  end

  def update
    @enrollment.update(enrollment_params)
    respond_to do |format|
      format.json { respond_with_bip(@enrollment) }
    end
  end

  private

  def set_enrollment
    @enrollment = Enrollment.find(params[:id])
  end

  def enrollment_params
    params.require(:enrollment).permit(:grade)
  end
end
