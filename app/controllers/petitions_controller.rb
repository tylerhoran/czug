# frozen_string_literal: true

class PetitionsController < ApplicationController
  before_action :authenticate_user!
  before_action :check_for_card
  add_breadcrumb 'Dashboard', :authenticated_root_path
  load_and_authorize_resource

  def create
    Petition.create!(user: current_user, degree: current_user.taught_degree, status: 'pending')
    redirect_to root_path
  end

  def show
    @petition = Petition.find(params[:id])
    add_breadcrumb 'Degree certificate', petition_path(@petition)
  end
end
