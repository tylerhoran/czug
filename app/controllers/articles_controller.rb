# frozen_string_literal: true

class ArticlesController < ApplicationController
  before_action :set_article

  def show; end

  private

  def set_article
    @article = Article.friendly.find(params[:id])
  end
end
