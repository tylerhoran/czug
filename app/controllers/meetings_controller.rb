# frozen_string_literal: true

class MeetingsController < ApplicationController
  before_action :authenticate_user!
  before_action :check_for_card
  before_action :set_instance
  before_action :set_meeting
  load_and_authorize_resource

  def show; end

  def edit
    add_breadcrumb 'Dashboard', :authenticated_root_path
    add_breadcrumb @instance.course_name, instance_path(@instance)
    add_breadcrumb 'Edit Meeting Time'
  end

  def update
    if @meeting.update(meeting_params)
      flash[:success] = 'Meeting time was updated'
      redirect_to instance_path(@instance)
    else
      render :edit
    end
  end

  private

  def set_instance
    @instance = Instance.friendly.find(params[:instance_id])
  end

  def set_meeting
    @meeting = @instance.schedule.meetings.find(params[:id])
  end

  def meeting_params
    params.require(:meeting).permit(:occurrence)
  end
end
