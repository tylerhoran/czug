# frozen_string_literal: true

class InstancesController < ApplicationController
  before_action :authenticate_user!
  before_action :check_for_card
  before_action :set_instance
  add_breadcrumb 'Dashboard', :authenticated_root_path
  load_and_authorize_resource

  def show
    add_breadcrumb @instance.course_name, instance_path(@instance)
    @posts = @instance.posts.page(params[:page]).per(20)
  end

  def update
    respond_to do |format|
      @instance.update(instance_params)
      format.html { redirect_to :back, notice: 'Course updated' }
      format.json { respond_with_bip(@instance) }
    end
  end

  def confirmed; end

  private

  def instance_params
    params.require(:instance).permit(:syllabus, :description)
  end

  def set_instance
    @instance = Instance.friendly.find(params[:id])
  end
end
