# frozen_string_literal: true

class PostsController < ApplicationController
  before_action :authenticate_user!
  before_action :check_for_card
  before_action :set_instance
  before_action :authorize_instance!

  def create
    post = @instance.posts.build(post_params)
    post.user = current_user
    post.save
    redirect_to instance_path(@instance)
  end

  private

  def authorize_instance!
    redirect_to root_path unless current_user.member_of?(@instance)
  end

  def set_instance
    @instance = Instance.friendly.find(params[:instance_id])
  end

  def post_params
    params.require(:post).permit(:user_id, :instance_id, :body)
  end
end
