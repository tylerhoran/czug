# frozen_string_literal: true

class EmailsController < ApplicationController
  def create
    email = Email.new(email_params)
    if email.save
      redirect_to new_user_registration_path(email: email.name)
    else
      redirect_to root_path
    end
  end

  private

  def email_params
    params.require(:email).permit(:name)
  end
end
