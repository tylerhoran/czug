# frozen_string_literal: true

class CoursesController < ApplicationController
  before_action :authenticate_user!, except: [:show]
  before_action :check_professor!, except: [:show]
  add_breadcrumb 'Dashboard', :authenticated_root_path, except: [:show]
  load_and_authorize_resource except: [:show]

  def index
    add_breadcrumb 'Course proposals', :courses_path
    @courses = current_user.proposed_courses
  end

  def show
    @course = Course.friendly.find(params[:id])
  end

  def new
    @course = Course.new
  end

  def create
    @course = Course.new(course_params)
    @course.save
    @course.proposer = current_user
    render :index
  end

  def destroy
    @course = Course.find(params[:id])
    @course.destroy
    redirect_to root_path
  end

  private

  def course_params
    params.require(:course).permit(:name, :description, :degree_id, prerequisite_ids: [])
  end
end
