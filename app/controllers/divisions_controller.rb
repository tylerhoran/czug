# frozen_string_literal: true

class DivisionsController < ApplicationController
  load_and_authorize_resource

  def index
    @divisions = Division.all
  end
end
