# frozen_string_literal: true

namespace :notify_ending_courses do
  desc 'prompting students to register for new classes (run weekly)'
  task to_register: :environment do
    NotifyEndingCoursesToRegisterJob.perform_later if Time.current.wday == 1
  end

  desc 'marking courses as complete with no remaining meetings and propt grades ( run daily )'
  task to_complete: :environment do
    NotifyEndingCoursesToCompleteJob.perform_later
  end
end
