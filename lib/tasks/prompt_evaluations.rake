# frozen_string_literal: true

namespace :prompt_evaluations do
  desc 'Prompting for an evaluation (run monthly)'
  task monthly: :environment do
    PromptEvaluationsJob.perform_later if Time.zone.today.end_of_month == Time.zone.today
  end
end
