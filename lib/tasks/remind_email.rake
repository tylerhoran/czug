# frozen_string_literal: true

namespace :remind_email do
  desc 'Reminding user to finish registration (run daily)'
  task signups: :environment do
    RemindEmailJob.perform_later
  end
end
