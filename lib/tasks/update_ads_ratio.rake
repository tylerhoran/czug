# frozen_string_literal: true

namespace :update_ads_ratio do
  desc 'Updating ad spend to reflect demand for degree'
  task weekly: :environment do
    UpdateAdsRatioJob.perform_later
  end
end
