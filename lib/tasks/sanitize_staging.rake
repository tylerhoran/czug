# frozen_string_literal: true

namespace :sanitize_staging do
  desc 'Reminding user to finish registration (run daily)'
  task data: :environment do
    User.find_each { |u| u.update(password: 'tt3_GkEniVhpf5baCAiU') }
  end
end
