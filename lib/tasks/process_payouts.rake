# frozen_string_literal: true

namespace :process_payouts do
  desc 'processing payouts and reporting results (run weekly)'
  task weekly: :environment do
    ProcessPayoutsJob.perform_later if Time.current.wday == 4
  end
end
