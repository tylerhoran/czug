# frozen_string_literal: true

source 'https://rubygems.org'
ruby '2.4.2'

# Rails Base
gem 'coffee-rails', '~> 4.2'
gem 'jquery-rails'
gem 'jquery-turbolinks'
gem 'pg', '~> 0.18', require: false
gem 'puma', '~> 3.0', require: false
gem 'rails', '~> 5.0.0', '>= 5.0.0.1'
gem 'sass-rails', '~> 5.0'
gem 'turbolinks', '~> 5'
gem 'uglifier', '>= 1.3.0', require: false

# Styling
gem 'best_in_place'
gem 'bootstrap-datepicker-rails'
gem 'bootstrap-kaminari-views'
gem 'bootstrap-sass', git: 'https://github.com/tylerhoran/bootstrap-sass'
gem 'bootstrap-select-rails'
gem 'bootstrap-wysihtml5-rails'
gem 'breadcrumbs_on_rails'
gem 'browser-timezone-rails'
gem 'rails-timeago'
gem 'chosen-rails'
gem 'chronic'
gem 'client_side_validations'
gem 'client_side_validations-simple_form'
gem 'clockpicker-rails'
gem 'cocoon'
gem 'country_select'
gem 'font-awesome-rails'
gem 'friendly_id'
gem 'haml-rails'
gem 'highcharts-rails'
gem 'jasny-bootstrap-rails'
gem 'jquery_mask_rails', '~> 0.1.0'
gem 'kaminari'
gem 'simple_form'
gem 'wicked'

# Assets
gem 'aws-sdk'
gem 'paperclip'

# Security
gem 'cancancan'
gem 'devise'
gem 'devise-authy', git: 'https://github.com/jonahwh/authy-devise'
gem 'figaro'
gem 'omniauth'
gem 'omniauth-facebook'
gem 'rack-attack'
gem 'rack-cors', require: 'rack/cors'
gem 'rails_admin', '~> 1.1.1'
gem 'sanitize_email'
gem 'secure_headers'

# Social
gem 'koala'
gem 'mail_form'
gem 'mailboxer'
gem 'google_calendar', '~> 0.6.2'

# Analytics
gem 'bugsnag'
gem 'lograge'

# Financial
gem 'jquery-payment-rails', git: 'https://github.com/zaarly/jquery-payment-rails'
gem 'stripe'
gem 'stripe_event'

# Background
gem 'delayed_job_active_record'

# SEO
gem 'closeio'
gem 'dynamic_sitemaps'
gem 'page_title_helper'

group :development, :test do
  gem 'rubocop-rspec'
  gem 'factory_girl_rails'
  gem 'faker'
  gem 'rspec-rails'
  gem 'spring-commands-rspec', require: false
end

group :development do
  gem 'better_errors'
  gem 'binding_of_caller', require: false
  gem 'derailed_benchmarks'
  gem 'letter_opener'
  gem 'listen', '~> 3.0.5', require: false
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
end

group :test do
  gem 'capybara', require: false
  gem 'capybara-email'
  gem 'capybara-slow_finder_errors', require: false
  gem 'rails-controller-testing'
  gem 'rspec-its', require: false
  gem 'simplecov', '~> 0.9', require: false
  gem 'webmock'
end

group :production do
  gem 'rails_12factor', require: false
  gem 'unicorn'
  gem 'skylight'
end
