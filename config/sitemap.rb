# frozen_string_literal: true

# Change this to your host. See the readme at https://github.com/lassebunk/dynamic_sitemaps
# for examples of multiple hosts and folders.
host 'https://www.octad.ch'

sitemap :site do
  url root_url, last_mod: Time.current, change_freq: 'daily', priority: 1.0
  url about_url, last_mod: Time.current, change_freq: 'daily', priority: 1.0
  url faq_url, last_mod: Time.current, change_freq: 'daily', priority: 1.0
  url privacy_url, last_mod: Time.current, change_freq: 'daily', priority: 1.0
  url terms_url, last_mod: Time.current, change_freq: 'daily', priority: 1.0
  url tuition_url, last_mod: Time.current, change_freq: 'daily', priority: 1.0
  url academics_url, last_mod: Time.current, change_freq: 'daily', priority: 1.0
  url blog_url, last_mod: Time.current, change_freq: 'daily', priority: 1.0
  Division.all.each do |division|
    division.degrees.each do |degree|
      url division_degree_url(division, degree), las_mod: degree.updated_at, priority: 1.0
    end
  end

  Course.approved.each do |course|
    url course, last_mod: course.updated_at, priority: 1.0
  end

  Article.all.each do |article|
    url article, last_mod: article.updated_at, priority: 1.0
  end
end

ping_with "#{host}/sitemap.xml"
