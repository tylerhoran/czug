# frozen_string_literal: true

Rails.application.routes.draw do
  get 'errors/not_found'
  get 'errors/internal_server_error'
  devise_for :users, controllers: { registrations: 'users/registrations', sessions: 'users/sessions', omniauth_callbacks: 'users/omniauth_callbacks' }

  HomeController.action_methods.each do |action|
    get action.to_sym, to: "home##{action}"
  end
  resources :articles, only: [:show]

  resources :divisions, only: [:index] do
    resources :degrees, only: [:show]
  end

  resources :contacts, only: %i[new create]
  resources :setup, only: %i[show update]
  resources :cards, only: [:new, :create, :destroy]
  resources :reservations, only: %i[index destroy]
  resources :charges, only: [:index]
  resources :transfers, only: [:index]
  resources :availabilities, only: %i[index show destroy update]
  resources :enrollments, only: %i[index update] do
    resources :evaluations, only: %i[new create]
  end
  resources :courses, except: [:edit, :update]
  resources :slots, only: [:index]
  resources :emails, only: [:create]
  resources :users, only: %i[show update] do
    resources :appointments, only: [:new, :create, :destroy]
    member do
      get :finish_signup
      patch :complete_signup
      get :student_welcome
      get :professor_welcome
      get :interview
      get :reject
      get :approve
    end
  end

  resources :petitions, only: %i[create show]
  resources :instances, only: [:show, :update] do
    member { get :confirmed }
    resources :posts, only: [:create]
    resources :meetings, only: %i[show edit update]
    resources :assignments, except: [:index] do
      resources :submissions, only: %i[new create update]
    end
  end
  resources :dashboard, only: [:index] do
    collection { get :activities }
  end

  resources :conversations, only: [:index, :show] do
    member do
      post :reply
    end
  end

  resources :messages, only: %i[new create]
  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'

  authenticated :user do
    root 'dashboard#index', as: :authenticated_root
  end

  root to: 'home#index'
  mount StripeEvent::Engine, at: '/stripe-events'
  match '/404', to: 'errors#not_found', via: :all
  match '/500', to: 'errors#internal_server_error', via: :all
  get 'sitemap.xml' => 'home#sitemap', format: :xml
  get 'robots.txt' => 'home#robots', format: :text
end
