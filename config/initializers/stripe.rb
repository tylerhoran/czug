# frozen_string_literal: true

Rails.configuration.stripe = {
  publishable_key: ENV['STRIPE_PUBLISHABLE_KEY'],
  secret_key: ENV['STRIPE_SECRET_KEY']
}

Stripe.api_key = Rails.configuration.stripe[:secret_key]

StripeEvent.event_retriever = lambda do |params|
  if params[:user_id].blank?
    return Stripe::Event.retrieve(params[:id])
  else
    evt = Stripe::Event.retrieve(params[:id], stripe_account: params[:user_id])
    evt['user_id'] = params[:user_id]
    return evt
  end
end

StripeEvent.configure do |events|
  events.subscribe 'account.updated' do |event|
    user = User.find_by(stripe_account_token: event.data.object.id)

    if event.data.object.verification.fields_needed.nil?
      user&.update(verification_required: false)
    else
      user&.update(verification_required: true)
    end
  end
end
