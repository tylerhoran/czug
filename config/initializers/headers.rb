# frozen_string_literal: true

SecureHeaders::Configuration.default do |config|
  config.csp = {
    default_src: ["'self'", 'https:'],
    font_src: ["'self'", 'https:', 'data:'],
    img_src: ["'self'", 'https:', 'data:'],
    object_src: ["'none'"],
    script_src: ["'self'", 'https:', "'unsafe-inline'", "'unsafe-eval'"],
    style_src: ["'self'", 'https:', "'unsafe-inline'"]
  }
end
