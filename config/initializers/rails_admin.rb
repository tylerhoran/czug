# frozen_string_literal: true

RailsAdmin.config do |config|
  config.current_user_method { current_user }
  config.authorize_with do
    redirect_to main_app.root_path unless warden.user && warden.user.admin?
  end
  config.parent_controller = 'ApplicationController'

  config.actions do
    dashboard
    index
    new
    export
    bulk_delete
    show
    edit
    delete
    show_in_app
  end
end
