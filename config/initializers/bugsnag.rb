# frozen_string_literal: true

if Rails.env.production?
  Bugsnag.configure do |config|
    config.api_key = ENV['BUGSNAG_TOKEN']
  end
end
