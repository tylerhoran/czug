# frozen_string_literal: true

Paperclip.interpolates :default_image_url do |_attachment, _style|
  ActionController::Base.helpers.asset_path('path/to/default/image.png')
end
