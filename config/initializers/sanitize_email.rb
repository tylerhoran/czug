# frozen_string_literal: true

SanitizeEmail::Config.configure do |config|
  config[:sanitized_to] = ENV['ADMIN_EMAIL']
  config[:activation_proc] = proc { ENV['SANITIZE_EMAIL'].present? }
  config[:use_actual_email_prepended_to_subject] = true         # or false
  config[:use_actual_environment_prepended_to_subject] = true   # or false
  config[:use_actual_email_as_sanitized_user_name] = true       # or false
end
