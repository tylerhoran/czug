# frozen_string_literal: true

# This file was generated by the `rails generate rspec:install` command. Conventionally, all
# specs live under a `spec` directory, which RSpec adds to the `$LOAD_PATH`.
# The generated `.rspec` file contains `--require spec_helper` which will cause
# this file to always be loaded, without a need to explicitly require it in any
# files.
#
# Given that it is always loaded, you are encouraged to keep this file as
# light-weight as possible. Requiring heavyweight dependencies from this file
# will add to the boot time of your test suite on EVERY test run, even for an
# individual file that may not need all of that loaded. Instead, consider making
# a separate helper file that requires the additional dependencies and performs
# the additional setup, and require it from the spec files that actually need
# it.
#
# The `.rspec` file also contains a few flags that are not defaults but that
# users commonly want.
#
# See http://rubydoc.info/gems/rspec-core/RSpec/Core/Configuration
require 'capybara/email/rspec'
require 'simplecov'
require 'webmock/rspec'
SimpleCov.start
Delayed::Worker.delay_jobs = false
Dir[Rails.root.join('spec/support/*.rb')].each { |f| require f }
RSpec.configure do |config|
  config.include FactoryGirl::Syntax::Methods
  # rspec-expectations config goes here. You can use an alternate
  # assertion/expectation library such as wrong or the stdlib/minitest
  # assertions if you prefer.
  config.before(:all, callbacks: true) do
    ActiveRecord::Base.skip_callbacks = false
  end

  config.after(:all, callbacks: true) do
    ActiveRecord::Base.skip_callbacks = true
  end
  ActiveRecord::Base.skip_callbacks = true
  config.include ControllerHelpers, type: :controller
  Warden.test_mode!

  config.after do
    Warden.test_reset!
  end
  config.expect_with :rspec do |expectations|
    # This option will default to `true` in RSpec 4. It makes the `description`
    # and `failure_message` of custom matchers include text for helper methods
    # defined using `chain`, e.g.:
    #     be_bigger_than(2).and_smaller_than(4).description
    #     # => "be bigger than 2 and smaller than 4"
    # ...rather than:
    #     # => "be bigger than 2"
    expectations.include_chain_clauses_in_custom_matcher_descriptions = true
  end

  # rspec-mocks config goes here. You can use an alternate test double
  # library (such as bogus or mocha) by changing the `mock_with` option here.
  config.mock_with :rspec do |mocks|
    # Prevents you from mocking or stubbing a method that does not exist on
    # a real object. This is generally recommended, and will default to
    # `true` in RSpec 4.
    mocks.verify_partial_doubles = true
  end

  # This option will default to `:apply_to_host_groups` in RSpec 4 (and will
  # have no way to turn it off -- the option exists only for backwards
  # compatibility in RSpec 3). It causes shared context metadata to be
  # inherited by the metadata hash of host groups and examples, rather than
  # triggering implicit auto-inclusion in groups with matching metadata.
  config.shared_context_metadata_behavior = :apply_to_host_groups

  # The settings below are suggested to provide a good initial experience
  # with RSpec, but feel free to customize to your heart's content.
  #   # This allows you to limit a spec run to individual examples or groups
  #   # you care about by tagging them with `:focus` metadata. When nothing
  #   # is tagged with `:focus`, all examples get run. RSpec also provides
  #   # aliases for `it`, `describe`, and `context` that include `:focus`
  #   # metadata: `fit`, `fdescribe` and `fcontext`, respectively.
  #   config.filter_run_when_matching :focus
  #
  #   # Allows RSpec to persist some state between runs in order to support
  #   # the `--only-failures` and `--next-failure` CLI options. We recommend
  #   # you configure your source control system to ignore this file.
  #   config.example_status_persistence_file_path = "spec/examples.txt"
  #
  #   # Limits the available syntax to the non-monkey patched syntax that is
  #   # recommended. For more details, see:
  #   #   - http://rspec.info/blog/2012/06/rspecs-new-expectation-syntax/
  #   #   - http://www.teaisaweso.me/blog/2013/05/27/rspecs-new-message-expectation-syntax/
  #   #   - http://rspec.info/blog/2014/05/notable-changes-in-rspec-3/#zero-monkey-patching-mode
  #   config.disable_monkey_patching!
  #
  #   # Many RSpec users commonly either run the entire suite or an individual
  #   # file, and it's useful to allow more verbose output when running an
  #   # individual spec file.
  #   if config.files_to_run.one?
  #     # Use the documentation formatter for detailed output,
  #     # unless a formatter has already been configured
  #     # (e.g. via a command-line flag).
  #     config.default_formatter = 'doc'
  #   end
  #
  #   # Print the 10 slowest examples and example groups at the
  #   # end of the spec run, to help surface which specs are running
  #   # particularly slow.
  #   config.profile_examples = 10
  #
  #   # Run specs in random order to surface order dependencies. If you find an
  #   # order dependency and want to debug it, you can fix the order by providing
  #   # the seed, which is printed after each run.
  #   #     --seed 1234
  #   config.order = :random
  #
  #   # Seed global randomization in this process using the `--seed` CLI option.
  #   # Setting this allows you to use `--seed` to deterministically reproduce
  #   # test failures related to randomization by passing the same `--seed` value
  #   # as the one that triggered the failure.
  #   Kernel.srand config.seed
  config.before(:each) do
    stub_request(:post, 'https://api.stripe.com/v1/refunds').to_return(status: 200, body: '{"id":"re_19XXqJAwy7q18dQ9RWaU3i1v","object":"refund","amount":100,"balance_transaction":null,"charge":"ch_19XXqJAwy7q18dQ93tt3mLtY","created":1483371323,"currency":"usd","metadata":{},"reason":null,"receipt_number":null,"status":"succeeded"}', headers: {})
    stub_request(:post, /https:\/\/www.googleapis.com\/calendar\/v3\/calendars\/meetings\@octad.ch\/events.{1,}/).to_return(status: 200, body: '{"summary":"fksljfd","visibility":"default","transparency":"transparent","description":null,"location":null,"start":{"dateTime":"2017-01-02T10:30:00Z"},"end":{"dateTime":"2017-01-02T11:30:00Z"},"reminders":{"useDefault":true},"guestsCanInviteOthers":null,"guestsCanSeeOtherGuests":null}', headers: {})
    stub_request(:delete, /https:\/\/www.googleapis.com\/calendar\/v3\/calendars\/meetings\@octad.ch\/events.{1,}/).to_return(status: 204, body: '', headers: {})
    stub_request(:post, 'https://accounts.google.com/o/oauth2/token').to_return(status: 200, body: '{"access_token":"1/fFAGRNJru1FTz70BzhT3Zg","expires_in":3920,"token_type":"Bearer"}', headers: { 'Content-Type' => 'application/json' })
    stub_request(:post, 'https://api.stripe.com/v1/charges').to_return(status: 200, body: '{"id":"ch_18waYLBD8BkkA2G1Kif8sZ8y","object":"charge","amount":46800,"amount_refunded":0,"application":null,"application_fee":null,"balance_transaction":"txn_18denSBD8BkkA2G1gV2QAiuM","captured":true,"created":1474563605,"currency":"usd","customer":"cus_9F4eqLicSW11ne","description":null,"destination":null,"dispute":null,"failure_code":null,"failure_message":null,"fraud_details":{},"invoice":"in_18waYLBD8BkkA2G1uNom9csg","livemode":false,"metadata":{},"order":null,"outcome":{"network_status":"approved_by_network","reason":null,"risk_level":"normal","seller_message":"Payment complete.","type":"authorized"},"paid":true,"receipt_email":"igor@angeloop.co","receipt_number":null,"refunded":false,"refunds":{"object":"list","data":[],"has_more":false,"total_count":0,"url":"/v1/charges/ch_18waYLBD8BkkA2G1Kif8sZ8y/refunds"},"review":null,"shipping":null,"source":{"id":"card_18waYJBD8BkkA2G155UBx0MR","object":"card","address_city":"New York","address_country":"US","address_line1":"100 E 77th St","address_line1_check":"pass","address_line2":null,"address_state":"NY","address_zip":"10065","address_zip_check":"pass","brand":"Visa","country":"US","customer":"cus_9F4eqLicSW11ne","cvc_check":"pass","dynamic_last4":null,"exp_month":10,"exp_year":2018,"funding":"credit","last4":"4242","metadata":{},"name":null,"tokenization_method":null},"source_transfer":null,"statement_descriptor":null,"status":"succeeded"}', headers: {})
    stub_request(:post, 'https://api.stripe.com/v1/customers').to_return(status: 200, body: '{"id":"jj90dj293jd9302jd9230dj39","object":"customer","account_balance":0,"created":1482189269,"currency":"usd","default_source":null,"delinquent":false,"description":"Ticketscloud","discount":null,"email":"egor@ticketscloud.org","livemode":false,"metadata":{},"shipping":null,"sources":{"object":"list","data":[],"has_more":false,"total_count":0,"url":"/v1/customers/jj90dj293jd9302jd9230dj39/sources"},"subscriptions":{"object":"list","data":[],"has_more":false,"total_count":0,"url":"/v1/customers/jj90dj293jd9302jd9230dj39/subscriptions"}}', headers: {})
    stub_request(:post, /https\:\/\/api.stripe.com\/v1\/customers\/.{1,}\/sources/).to_return(status: 200, body: '{"id":"card_19Sa6SBD8BkkA2G1iG9NPdYM","object":"card","address_city":"Moscow","address_country":"RU","address_line1":"burakova st. 25a, 22","address_line1_check":"unchecked","address_line2":null,"address_state":"","address_zip":"105118","address_zip_check":"unchecked","brand":"MasterCard","country":"RU","customer":"cus_9m8cD7GRFNkfGw","cvc_check":"unchecked","dynamic_last4":null,"exp_month":11,"exp_year":2020,"funding":"credit","last4":"7709","metadata":{},"name":null,"tokenization_method":null}', headers: {})
    stub_request(:get, /https\:\/\/api.stripe.com\/v1\/customers\/.{1,}/).to_return(status: 200, body: '{"id":"jj90dj293jd9302jd9230dj39","object":"customer","account_balance":0,"created":1482189269,"currency":"usd","default_source":null,"delinquent":false,"description":"Ticketscloud","discount":null,"email":"egor@ticketscloud.org","livemode":false,"metadata":{},"shipping":null,"sources":{"object":"list","data":[],"has_more":false,"total_count":0,"url":"/v1/customers/jj90dj293jd9302jd9230dj39/sources"},"subscriptions":{"object":"list","data":[],"has_more":false,"total_count":0,"url":"/v1/customers/jj90dj293jd9302jd9230dj39/subscriptions"}}', headers: {})
    stub_request(:get, /https\:\/\/api.stripe.com\/v1\/accounts\/.{1,}/).to_return(status: 200, body: '{"id":"acct_19aD50IysK9GLATM","object":"account","business_logo":null,"business_name":"Brasden Inc","business_url":"https://www.octad.ch","charges_enabled":true,"country":"US","debit_negative_balances":true,"decline_charge_on":{"avs_failure":false,"cvc_failure":true},"default_currency":"usd","details_submitted":true,"display_name":"Brasden","email":"tyler@octad.ch","external_accounts":{"object":"list","data":[{"id":"ba_19aDJlIysK9GLATMeybUq2vT","object":"bank_account","account":"acct_19aD50IysK9GLATM","account_holder_name":null,"account_holder_type":null,"bank_name":"JPMORGAN CHASE","country":"US","currency":"usd","default_for_currency":true,"fingerprint":"4xus2sRV6MdegyCT","last4":"3387","metadata":{},"routing_number":"322271627","status":"new"}],"has_more":false,"total_count":1,"url":"/v1/accounts/acct_19aD50IysK9GLATM/external_accounts"},"legal_entity":{"address":{"city":"Santa Cruz","country":"US","line1":"PO Box 3670","line2":null,"postal_code":"95063","state":"CA"},"business_name":"Brasden Inc","business_tax_id_provided":true,"dob":{"day":15,"month":1,"year":1984},"first_name":"Tyler","last_name":"Horan","personal_address":{"city":null,"country":"US","line1":null,"line2":null,"postal_code":null,"state":null},"personal_id_number_provided":true,"ssn_last_4_provided":true,"type":"corporation","verification":{"details":null,"details_code":null,"document":null,"status":"verified"}},"managed":true,"metadata":null,"product_description":"Online Education","statement_descriptor":"WWW.BRASDEN.COM","support_email":"support@octad.ch","support_phone":"4154486798","support_url":"https://www.octad.ch/support","timezone":"America/Los_Angeles","tos_acceptance":{"date":1484007409,"ip":"73.223.247.27","user_agent":null},"transfer_schedule":{"delay_days":2,"interval":"manual"},"transfer_statement_descriptor":null,"transfers_enabled":true,"verification":{"disabled_reason":null,"due_by":null,"fields_needed":[]}}', headers: {})
    stub_request(:post, /https\:\/\/api.stripe.com\/v1\/accounts\/.{1,}\/external_accounts/).to_return(status: 200, body: '{"id":"card_19Sa6SBD8BkkA2G1iG9NPdYM","object":"card","address_city":"Moscow","address_country":"RU","address_line1":"burakova st. 25a, 22","address_line1_check":"unchecked","address_line2":null,"address_state":"","address_zip":"105118","address_zip_check":"unchecked","brand":"MasterCard","country":"RU","cvc_check":"unchecked","dynamic_last4":null,"exp_month":11,"exp_year":2020,"funding":"credit","last4":"7709","metadata":{},"name":null,"tokenization_method":null,"account":"acct_18WVITBD8BkkA2G1"}', headers: {})
    stub_request(:post, /https\:\/\/api.stripe.com\/v1\/accounts\/.{1,}/).to_return(status: 200, body: '{"id":"acct_19aD50IysK9GLATM","object":"account","business_logo":null,"business_name":"Brasden Inc","business_url":"https://www.octad.ch","charges_enabled":true,"country":"US","debit_negative_balances":true,"decline_charge_on":{"avs_failure":false,"cvc_failure":true},"default_currency":"usd","details_submitted":true,"display_name":"Brasden","email":"tyler@octad.ch","external_accounts":{"object":"list","data":[{"id":"ba_19aDJlIysK9GLATMeybUq2vT","object":"bank_account","account":"acct_19aD50IysK9GLATM","account_holder_name":null,"account_holder_type":null,"bank_name":"JPMORGAN CHASE","country":"US","currency":"usd","default_for_currency":true,"fingerprint":"4xus2sRV6MdegyCT","last4":"3387","metadata":{},"routing_number":"322271627","status":"new"}],"has_more":false,"total_count":1,"url":"/v1/accounts/acct_19aD50IysK9GLATM/external_accounts"},"legal_entity":{"address":{"city":"Santa Cruz","country":"US","line1":"PO Box 3670","line2":null,"postal_code":"95063","state":"CA"},"business_name":"Brasden Inc","business_tax_id_provided":true,"dob":{"day":15,"month":1,"year":1984},"first_name":"Tyler","last_name":"Horan","personal_address":{"city":null,"country":"US","line1":null,"line2":null,"postal_code":null,"state":null},"personal_id_number_provided":true,"ssn_last_4_provided":true,"type":"corporation","verification":{"details":null,"details_code":null,"document":null,"status":"verified"}},"managed":true,"metadata":null,"product_description":"Online Education","statement_descriptor":"WWW.BRASDEN.COM","support_email":"support@octad.ch","support_phone":"4154486798","support_url":"https://www.octad.ch/support","timezone":"America/Los_Angeles","tos_acceptance":{"date":1484007409,"ip":"73.223.247.27","user_agent":null},"transfer_schedule":{"delay_days":2,"interval":"manual"},"transfer_statement_descriptor":null,"transfers_enabled":true,"verification":{"disabled_reason":null,"due_by":null,"fields_needed":[]}}', headers: {})
    stub_request(:post, 'https://api.stripe.com/v1/transfers').to_return(status: 200, body: '{"id":"tr_18yjomBD8BkkA2G1YsLnt7DX","object":"transfer","amount":45413,"amount_reversed":0,"application_fee":null,"balance_transaction":"txn_18denSBD8BkkA2G1gV2QAiuM","bank_account":{"id":"ba_18WVMtBD8BkkA2G1YGL6T99I","object":"bank_account","account_holder_name":null,"account_holder_type":null,"bank_name":"BANK OF AMERICA, N.A.","country":"US","currency":"usd","fingerprint":"iATrOxerEEBnANXG","last4":"7358","routing_number":"081904808","status":"new"},"created":1475075876,"currency":"usd","date":1475107200,"description":"STRIPE TRANSFER","destination":"ba_18WVMtBD8BkkA2G1YGL6T99I","failure_code":null,"failure_message":null,"livemode":false,"metadata":{},"method":"standard","recipient":null,"reversals":{"object":"list","data":[],"has_more":false,"total_count":0,"url":"/v1/transfers/tr_18yjomBD8BkkA2G1YsLnt7DX/reversals"},"reversed":false,"source_transaction":null,"source_type":"card","statement_descriptor":null,"status":"paid","type":"bank_account"}', headers: {})
    stub_request(:post, 'https://api.stripe.com/v1/accounts').to_return(status: 200, body: '{"id":"acct_19aD50IysK9GLATM","object":"account","business_logo":null,"business_name":"Brasden Inc","business_url":"https://www.octad.ch","charges_enabled":true,"country":"US","debit_negative_balances":true,"decline_charge_on":{"avs_failure":false,"cvc_failure":true},"default_currency":"usd","details_submitted":true,"display_name":"Brasden","email":"tyler@octad.ch","external_accounts":{"object":"list","data":[],"has_more":false,"total_count":1,"url":"/v1/accounts/acct_19aD50IysK9GLATM/external_accounts"},"legal_entity":{"address":{"city":"Santa Cruz","country":"US","line1":"PO Box 3670","line2":null,"postal_code":"95063","state":"CA"},"business_name":"Brasden Inc","business_tax_id_provided":true,"dob":{"day":15,"month":1,"year":1984},"first_name":"Tyler","last_name":"Horan","personal_address":{"city":null,"country":"US","line1":null,"line2":null,"postal_code":null,"state":null},"personal_id_number_provided":true,"ssn_last_4_provided":true,"type":"corporation","verification":{"details":null,"details_code":null,"document":null,"status":"verified"}},"managed":true,"metadata":null,"product_description":"Online Education","statement_descriptor":"WWW.BRASDEN.COM","support_email":null,"support_phone":"415-448-6798","timezone":"America/Los_Angeles","transfer_schedule":{"delay_days":2,"interval":"manual"},"transfer_statement_descriptor":null,"transfers_enabled":true,"verification":{"disabled_reason":null,"due_by":null,"fields_needed":[]}}', headers: {})
    stub_request(:get, /https:\/\/app.close.io\/api\/v1\/lead\/.{1,}/).to_return(status: 200, body: '{"has_more":false,"total_results":1,"data":[{"status_id":"stat_1ZdiZqcSIkoGVnNOyxiEY58eTGQmFNG3LPlEVQ4V7Nk","status_label":"Potential","tasks":[],"display_name":"Wayne Enterprises (Sample Lead)","addresses":[],"contacts":[{"name":"Bruce Wayne","title":"The Dark Knight","date_updated":"2013-02-06T20:53:01.954000+00:00","phones":[{"phone":"+16503334444","phone_formatted":"+1 650-333-4444","type":"office"}],"created_by":null,"id":"cont_o0kP3Nqyq0wxr5DLWIEm8mVr6ZpI0AhonKLDG0V5Qjh","organization_id":"orga_bwwWG475zqWiQGur0thQshwVXo8rIYecQHDWFanqhen","date_created":"2013-02-01T00:54:51.331000+00:00","emails":[{"type":"office","email_lower":"thedarkknight@close.io","email":"thedarkknight@close.io"}],"updated_by":"user_04EJPREurd0b3KDozVFqXSRbt2uBjw3QfeYa7ZaGTwI"}],"custom.lcf_ORxgoOQ5YH1p7lDQzFJ88b4z0j7PLLTRaG66m8bmcKv":"Website contact form","date_updated":"2013-02-06T20:53:01.977000+00:00","description":"","html_url":"https://app.close.io/lead/lead_IIDHIStmFcFQZZP0BRe99V1MCoXWz2PGCm6EDmR9v2O/","created_by":null,"organization_id":"orga_bwwWG475zqWiQGur0thQshwVXo8rIYecQHDWFanqhen","url":null,"opportunities":[{"id":"oppo_8eB77gAdf8FMy6GsNHEy84f7uoeEWv55slvUjKQZpJt","organization_id":"orga_bwwWG475zqWiQGur0thQshwVXo8rIYecQHDWFanqhen","lead_id":"lead_IIDHIStmFcFQZZP0BRe99V1MCoXWz2PGCm6EDmR9v2O","lead_name":"Wayne Enterprises (Sample Lead)","status_id":"stat_4ZdiZqcSIkoGVnNOyxiEY58eTGQmFNG3LPlEVQ4V7Nk","status_label":"Active","status_type":"active","value":50000,"value_period":"one_time","value_formatted":"$500","value_currency":"USD","date_won":null,"confidence":75,"note":"Bruce needs new software for the Bat Cave.","user_id":"user_scOgjLAQD6aBSJYBVhIeNr6FJDp8iDTug8Mv6VqYoFn","user_name":"P F","contact_id":null,"created_by":null,"updated_by":null,"date_created":"2013-02-01T00:54:51.337000+00:00","date_updated":"2013-02-01T00:54:51.337000+00:00"},{"id":"oppo_klajsdflf8FMy6GsNHEy84f7uoeEWv55slvUjKQZpJt","organization_id":"orga_bwwWG475zqWiQGur0thQshwVXo8rIYecQHDWFanqhen","lead_id":"lead_IIDHIStmFcFQZZP0BRe99V1MCoXWz2PGCm6EDmR9v2O","lead_name":"Wayne Enterprises (Sample Lead)","status_id":"stat_4ZdiZqcSIkoGVnNOyxiEY58eTGQmFNG3LPlEVQ4V7Nk","status_label":"Active","status_type":"active","value":5000,"value_period":"monthly","value_formatted":"$50 monthly","value_currency":"USD","date_won":null,"confidence":75,"note":"Bat Cave monthly maintenance cost","user_id":"user_scOgjLAQD6aBSJYBVhIeNr6FJDp8iDTug8Mv6VqYoFn","user_name":"P F","contact_id":null,"created_by":null,"updated_by":null,"date_created":"2013-02-01T00:54:51.337000+00:00","date_updated":"2013-02-01T00:54:51.337000+00:00"}],"updated_by":"user_04EJPREurd0b3KDozVFqXSRbt2uBjw3QfeYa7ZaGTwI","date_created":"2013-02-01T00:54:51.333000+00:00","id":"lead_IIDHIStmFcFQZZP0BRe99V1MCoXWz2PGCm6EDmR9v2O","name":"Wayne Enterprises (Sample Lead)"}]}', headers: {})
    stub_request(:post, 'https://app.close.io/api/v1/opportunity/').to_return(status: 200, body: '{"status_id":"stat_4ZdiZqcSIkoGVnNOyxiEY58eTGQmFNG3LPlEVQ4V7Nk","status_label":"Active","status_type":"active","date_won":null,"confidence":75,"user_id":"user_scOgjLAQD6aBSJYBVhIeNr6FJDp8iDTug8Mv6VqYoFn","contact_id":null,"updated_by":null,"date_updated":"2013-02-01T00:54:51.337000+00:00","created_by":null,"lead_id":"lead_IIDHIStmFcFQZZP0BRe99V1MCoXWz2PGCm6EDmR9v2O","note":"Bruce needs new software for the Bat Cave.","value":50000,"value_period":"one_time","value_formatted":"$500","value_currency":"USD","date_created":"2013-02-01T00:54:51.337000+00:00","organization_id":"orga_bwwWG475zqWiQGur0thQshwVXo8rIYecQHDWFanqhen","lead_name":"Wayne Enterprises (Sample Lead)","user_name":"P F","id":"oppo_8eB77gAdf8FMy6GsNHEy84f7uoeEWv55slvUjKQZpJt"}', headers: {})
    stub_request(:put, /https:\/\/app.close.io\/api\/v1\/opportunity.{1,}/).to_return(status: 200, body: '{"status_id":"stat_4ZdiZqcSIkoGVnNOyxiEY58eTGQmFNG3LPlEVQ4V7Nk","status_label":"Active","status_type":"active","date_won":null,"confidence":75,"user_id":"user_scOgjLAQD6aBSJYBVhIeNr6FJDp8iDTug8Mv6VqYoFn","contact_id":null,"updated_by":null,"date_updated":"2013-02-01T00:54:51.337000+00:00","created_by":null,"lead_id":"lead_IIDHIStmFcFQZZP0BRe99V1MCoXWz2PGCm6EDmR9v2O","note":"Bruce needs new software for the Bat Cave.","value":50000,"value_period":"one_time","value_formatted":"$500","value_currency":"USD","date_created":"2013-02-01T00:54:51.337000+00:00","organization_id":"orga_bwwWG475zqWiQGur0thQshwVXo8rIYecQHDWFanqhen","lead_name":"Wayne Enterprises (Sample Lead)","user_name":"P F","id":"oppo_8eB77gAdf8FMy6GsNHEy84f7uoeEWv55slvUjKQZpJt"}', headers: {})
    stub_request(:put, 'https://app.close.io/api/v1/lead/lead_IIDHIStmFcFQZZP0BRe99V1MCoXWz2PGCm6EDmR9v2O/').to_return(status: 200, body: '{"status_id":"stat_1ZdiZqcSIkoGVnNOyxiEY58eTGQmFNG3LPlEVQ4V7Nk","status_label":"Potential","tasks":[],"display_name":"Wayne Enterprises (Sample Lead)","addresses":[],"name":"Wayne Enterprises (Sample Lead)","contacts":[{"name":"Bruce Wayne","title":"The Dark Knight","date_updated":"2013-02-06T20:53:01.954000+00:00","phones":[{"phone":"+16503334444","phone_formatted":"+1 650-333-4444","type":"office"}],"created_by":null,"id":"cont_o0kP3Nqyq0wxr5DLWIEm8mVr6ZpI0AhonKLDG0V5Qjh","organization_id":"orga_bwwWG475zqWiQGur0thQshwVXo8rIYecQHDWFanqhen","date_created":"2013-02-01T00:54:51.331000+00:00","emails":[{"type":"office","email_lower":"thedarkknight@close.io","email":"thedarkknight@close.io"}],"updated_by":"user_04EJPREurd0b3KDozVFqXSRbt2uBjw3QfeYa7ZaGTwI"}],"custom.lcf_ORxgoOQ5YH1p7lDQzFJ88b4z0j7PLLTRaG66m8bmcKv":"Website contact form","date_updated":"2013-02-06T20:53:01.977000+00:00","html_url":"https://app.close.io/lead/lead_IIDHIStmFcFQZZP0BRe99V1MCoXWz2PGCm6EDmR9v2O/","created_by":null,"organization_id":"orga_bwwWG475zqWiQGur0thQshwVXo8rIYecQHDWFanqhen","url":null,"opportunities":[{"status_id":"stat_4ZdiZqcSIkoGVnNOyxiEY58eTGQmFNG3LPlEVQ4V7Nk","status_label":"Active","status_type":"active","date_won":null,"confidence":75,"user_id":"user_scOgjLAQD6aBSJYBVhIeNr6FJDp8iDTug8Mv6VqYoFn","contact_id":null,"updated_by":null,"date_updated":"2013-02-01T00:54:51.337000+00:00","value_period":"one_time","created_by":null,"note":"Bruce needs new software for the Bat Cave.","value":50000,"value_formatted":"$500","value_currency":"USD","lead_name":"Wayne Enterprises (Sample Lead)","organization_id":"orga_bwwWG475zqWiQGur0thQshwVXo8rIYecQHDWFanqhen","date_created":"2013-02-01T00:54:51.337000+00:00","user_name":"P F","id":"oppo_8eB77gAdf8FMy6GsNHEy84f7uoeEWv55slvUjKQZpJt","lead_id":"lead_IIDHIStmFcFQZZP0BRe99V1MCoXWz2PGCm6EDmR9v2O"},{"id":"oppo_klajsdflf8FMy6GsNHEy84f7uoeEWv55slvUjKQZpJt","organization_id":"orga_bwwWG475zqWiQGur0thQshwVXo8rIYecQHDWFanqhen","lead_id":"lead_IIDHIStmFcFQZZP0BRe99V1MCoXWz2PGCm6EDmR9v2O","lead_name":"Wayne Enterprises (Sample Lead)","status_id":"stat_4ZdiZqcSIkoGVnNOyxiEY58eTGQmFNG3LPlEVQ4V7Nk","status_label":"Active","status_type":"active","value":5000,"value_period":"monthly","value_formatted":"$50 monthly","value_currency":"USD","date_won":null,"confidence":75,"note":"Bat Cave monthly maintenance cost","user_id":"user_scOgjLAQD6aBSJYBVhIeNr6FJDp8iDTug8Mv6VqYoFn","user_name":"P F","contact_id":null,"created_by":null,"updated_by":null,"date_created":"2013-02-01T00:54:51.337000+00:00","date_updated":"2013-02-01T00:54:51.337000+00:00"}],"updated_by":"user_04EJPREurd0b3KDozVFqXSRbt2uBjw3QfeYa7ZaGTwI","date_created":"2013-02-01T00:54:51.333000+00:00","id":"lead_IIDHIStmFcFQZZP0BRe99V1MCoXWz2PGCm6EDmR9v2O","description":""}', headers: {})
  end
end
