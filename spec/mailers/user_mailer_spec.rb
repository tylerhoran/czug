# frozen_string_literal: true

require 'rails_helper'

RSpec.describe UserMailer, type: :mailer do
  describe 'acceptance_mail' do
    let(:user) { build(:user) }
    let(:mail) { UserMailer.acceptance_mail(user) }

    it 'renders the headers' do
      mail_array = [mail.subject, mail.to, mail.from]
      expect(mail_array).to eq(
        [
          "Congratulations! You've been accepted into The Brasden College",
          [user.email],
          ['site@octad.ch'],
        ]
      )
    end

    it 'renders the body' do
      expect(mail.body.encoded).to match('Congratulations!')
    end
  end

  describe 'rejection_mail' do
    let(:user) { build(:user) }
    let(:mail) { UserMailer.rejection_mail(user) }

    it 'renders the headers' do
      mail_array = [mail.subject, mail.to, mail.from]
      expect(mail_array).to eq(
        [
          'Information on your application to The Brasden College',
          [user.email],
          ['site@octad.ch'],
        ]
      )
    end

    it 'renders the body' do
      expect(mail.body.encoded).to match('We regret that we cannot offer you admission to this program')
    end
  end

  describe 'professor_acceptance_mail' do
    let(:user) { create(:onboarded_professor) }

    let(:mail) { UserMailer.professor_acceptance_mail(user) }

    it 'renders the headers' do
      mail_array = [mail.subject, mail.to, mail.from]
      expect(mail_array).to eq(
        [
          "Congratulations! You're application to The Brasden College has been approved",
          [user.email],
          ['site@octad.ch'],
        ]
      )
    end

    it 'renders the body' do
      expect(mail.body.encoded).to match('I am pleased to extend a formal offer')
    end
  end

  describe 'professor_rejection_mail' do
    let(:user) { create(:onboarded_professor) }
    let(:mail) { UserMailer.professor_rejection_mail(user) }

    it 'renders the headers' do
      mail_array = [mail.subject, mail.to, mail.from]
      expect(mail_array).to eq(
        [
          'Information on your application to The Brasden College',
          [user.email],
          ['site@octad.ch'],
        ]
      )
    end

    it 'renders the body' do
      expect(mail.body.encoded).to match('decided not to move forward with your admission')
    end
  end

  describe 'course_approved' do
    let(:user) { create(:onboarded_professor) }
    let(:course) { build(:course) }

    let(:mail) { UserMailer.course_approved(course) }

    it 'renders the headers' do
      mail_array = [mail.subject, mail.to, mail.from]
      expect(mail_array).to eq(
        [
          'Congratulations! Your course has been approved',
          [course.proposer.email],
          ['site@octad.ch'],
        ]
      )
    end

    it 'renders the body' do
      expect(mail.body.encoded).to match('Congratulations!')
    end
  end

  describe 'course_rejected' do
    let(:user) { create(:onboarded_professor) }
    let(:course) { build(:course) }

    let(:mail) { UserMailer.course_rejected(course) }

    it 'renders the headers' do
      mail_array = [mail.subject, mail.to, mail.from]
      expect(mail_array).to eq(
        [
          'Information on your course proposal at The Brasden College',
          [course.proposer.email],
          ['site@octad.ch'],
        ]
      )
    end

    it 'renders the body' do
      expect(mail.body.encoded).to match(
        'We regret that we cannot offer you permission to teach this course at this time.'
      )
    end
  end

  describe 'receipt_mail' do
    let(:user) { build(:user) }
    let(:charge) { build(:charge_with_enrollments) }

    let(:mail) { UserMailer.receipt_mail(charge) }

    it 'renders the headers' do
      mail_array = [mail.subject, mail.to, mail.from]
      expect(mail_array).to eq(
        [
          'Receipt for your course enrollments at The Brasden College',
          [charge.card.user.email], ['site@octad.ch']
        ]
        )
    end

    it 'renders the body' do
      expect(mail.body.encoded).to match('COURSES RECEIPT')
    end
  end

  describe 'refund_created' do
    let(:user) { create(:user) }
    let(:enrollment) { create(:enrollment, user: user) }
    let(:refund) { create(:refund, user: user) }

    let(:mail) { UserMailer.refund_created(enrollment, refund) }

    it 'renders the headers' do
      mail_array = [mail.subject, mail.to, mail.from]
      expect(mail_array).to eq(
        [
          "Receipt for your refund for #{enrollment.instance.course_name}",
          [user.email],
          ['site@octad.ch'],
        ]
      )
    end

    it 'renders the body' do
      expect(mail.body.encoded).to match('REFUND RECEIPT')
    end
  end

  describe 'reservation_removed' do
    let(:user) { build(:user) }
    let(:availability) { build(:availability, user: user) }

    let(:mail) { UserMailer.reservation_removed(availability, user) }

    it 'renders the headers' do
      mail_array = [mail.subject, mail.to, mail.from]
      expect(mail_array).to eq(
        [
          "Notification of withdrawal from #{availability.course_name} at The Brasden College",
          [user.email],
          ['site@octad.ch'],
        ]
      )
    end

    it 'renders the body' do
      expect(mail.body.encoded).to match('has been withdrawn')
    end
  end

  describe 'user_enrolled' do
    let(:user) { build(:onboarded_professor) }
    let(:student) { build(:user) }
    let(:instance) { build(:instance, user: user) }
    let(:enrollment) { build(:enrollment, user: student, instance: instance) }

    let(:mail) { UserMailer.user_enrolled(enrollment) }

    it 'renders the headers' do
      mail_array = [mail.subject, mail.to, mail.from]
      expect(mail_array).to eq(
        [
          "#{student.name} has enrolled to your course #{enrollment.instance.course_name}",
          [user.email],
          ['site@octad.ch'],
        ]
      )
    end

    it 'renders the body' do
      expect(mail.body.encoded).to match('has enrolled to your course')
    end
  end

  describe 'grade_submitted' do
    let(:enrollment) { build(:enrollment, instance: create(:instance), grade: 98) }

    let(:mail) { UserMailer.grade_submitted(enrollment) }

    it 'renders the headers' do
      mail_array = [mail.subject, mail.to, mail.from]
      expect(mail_array).to eq(
        [
          "You've received a grade for: #{enrollment.instance.course_name}",
          [enrollment.user.email],
          ['site@octad.ch'],
        ]
      )
    end

    it 'renders the body' do
      expect(mail.body.encoded).to match('Please log in below to review the grades for the course.')
    end
  end

  describe 'instance_post' do
    let(:instance) { build(:instance) }
    let(:post) { build(:post, instance: instance) }

    let(:mail) { UserMailer.instance_post(post, instance.user) }

    it 'renders the headers' do
      mail_array = [mail.subject, mail.to, mail.from]
      expect(mail_array).to eq(
        [
          "#{post.user.name} added a post in the #{post.instance.course_name} messageboard",
          [instance.user.email],
          ['site@octad.ch'],
        ]
      )
    end

    it 'renders the body' do
      expect(mail.body.encoded).to match('Log in to reply.')
    end
  end

  describe 'transfer_created' do
    let(:user) { create(:onboarded_professor) }
    let(:instances) { create_list(:instance, 3, user: user) }
    let(:enrollments) { create_list(:enrollment, 3, instance: instances.sample) }
    let(:transfer) { create(:transfer, card: create(:card, user: user), amount: 400) }
    let(:mail) { UserMailer.transfer_created(instances, transfer, transfer.amount) }

    it 'renders the headers' do
      mail_array = [mail.subject, mail.to, mail.from]
      expect(mail_array).to eq(
        [
          "Brasden College: Transfer #{Time.zone.today}",
          [user.email],
          ['site@octad.ch'],
        ]
      )
    end

    it 'renders the body' do
      expect(mail.body.encoded).to match('TRANSFER RECEIPT')
    end
  end
end
