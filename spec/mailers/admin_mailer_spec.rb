# frozen_string_literal: true

require 'rails_helper'

RSpec.describe AdminMailer, type: :mailer do
  let(:user) { create(:user) }

  describe 'new_user_waiting_for_approval' do
    let(:mail) { AdminMailer.new_user_waiting_for_approval(user) }

    it 'renders the headers' do
      mail_array = [mail.subject, mail.to, mail.from]
      expect(mail_array).to eq(
        [
          'New User waiting for approval',
          [ENV['ADMIN_EMAIL']],
          ['site@octad.ch'],
        ]
      )
    end

    it 'renders the body' do
      expect(mail.body.encoded).to match('Their previous transript is attached')
    end
  end

  describe 'course_proposal' do
    let(:mail) { AdminMailer.course_proposal(user) }

    it 'renders the headers' do
      mail_array = [mail.subject, mail.to, mail.from]
      expect(mail_array).to eq(
        [
          'New Course Proposal',
          [ENV['ADMIN_EMAIL']],
          ['site@octad.ch'],
        ]
      )
    end

    it 'renders the body' do
      expect(mail.body.encoded).to match('New Course Proposal')
    end
  end
end
