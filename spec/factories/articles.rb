# frozen_string_literal: true

FactoryGirl.define do
  factory :article do
    name { Faker::Lorem.sentence }
    body { Faker::Lorem.paragraph }
    attachment_file_name 'image.png'
    attachment_file_size { 1.megabyte }
  end
end
