# frozen_string_literal: true

FactoryGirl.define do
  factory :bank do
    user nil
    name 'MyString'
    country 'MyString'
    token 'MyString'
  end
end
