# frozen_string_literal: true

FactoryGirl.define do
  factory :availability do
    association :user
    association :course
    description { Faker::Lorem.paragraph }
  end
end
