# frozen_string_literal: true

FactoryGirl.define do
  factory :card do
    association :user
    card_type 'credit'
    brand 'visa'
    last_four '3789'
    token 'jj90dj293jd9302jd9230dj39'
    stripe_card_token 'jj90dj293jd9302jd9230dj39'
    expiration_month '01'
    expiration_year '2019'
    default false
  end
end
