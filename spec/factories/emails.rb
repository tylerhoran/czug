# frozen_string_literal: true

FactoryGirl.define do
  factory :email do
    name { Faker::Internet.email }
    status 0
  end
end
