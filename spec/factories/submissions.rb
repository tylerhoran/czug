# frozen_string_literal: true

FactoryGirl.define do
  factory :submission do
    association :assignment
    association :user
    body { Faker::Lorem.paragraph }
  end
end
