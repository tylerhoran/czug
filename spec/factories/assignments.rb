# frozen_string_literal: true

FactoryGirl.define do
  factory :assignment do
    association :instance
    name { Faker::Lorem.sentence }
    body { Faker::Lorem.paragraph }
  end
end
