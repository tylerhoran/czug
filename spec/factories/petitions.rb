# frozen_string_literal: true

FactoryGirl.define do
  factory :petition do
    association :user
    association :degree
  end
end
