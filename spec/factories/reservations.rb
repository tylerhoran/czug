# frozen_string_literal: true

FactoryGirl.define do
  factory :reservation do
    association :user
    association :availability
  end
end
