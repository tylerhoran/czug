# frozen_string_literal: true

FactoryGirl.define do
  factory :enrollment do
    association :user
    association :instance
  end
end
