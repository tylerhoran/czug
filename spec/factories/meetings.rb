# frozen_string_literal: true

FactoryGirl.define do
  factory :meeting do
    association :schedule
    occurrence { Time.current }
  end
end
