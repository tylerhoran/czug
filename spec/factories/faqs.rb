# frozen_string_literal: true

FactoryGirl.define do
  factory :faq do
    faq_type 1
    title { Faker::Lorem.sentence }
    body { Faker::Lorem.paragraph }
  end
end
