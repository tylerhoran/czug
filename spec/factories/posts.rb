# frozen_string_literal: true

FactoryGirl.define do
  factory :post do
    association :user
    association :instance
    body { Faker::Lorem.paragraph }
  end
end
