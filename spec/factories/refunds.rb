# frozen_string_literal: true

FactoryGirl.define do
  factory :refund do
    association :user
    association :charge
    amount 1
    stripe_refund_token 'MyString'
  end
end
