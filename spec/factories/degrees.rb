# frozen_string_literal: true

FactoryGirl.define do
  factory :degree do
    name { "#{Faker::Company.name} #{rand(0..100)}" }
    degree_type 'bs'
    description { Faker::Lorem.paragraph }
  end
end
