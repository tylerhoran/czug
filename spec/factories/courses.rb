# frozen_string_literal: true

FactoryGirl.define do
  factory :course do
    name { Faker::Company.name }
    association :degree
    association :proposer, factory: :user
  end
end
