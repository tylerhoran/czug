# frozen_string_literal: true

FactoryGirl.define do
  factory :appointment do
    association :user
    attendee_id { create(:user).id }
    time '2017-03-01 16:51:20'
  end
end
