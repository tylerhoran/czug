# frozen_string_literal: true

FactoryGirl.define do
  factory :activity do
    association :subject, factory: :post
    name 'MyString'
    direction 'to'
    association :user
  end
end
