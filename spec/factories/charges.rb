# frozen_string_literal: true

FactoryGirl.define do
  factory :charge do
    association :card
    association :instance
    amount 1
    stripe_charge_token 'MyString'
    factory :charge_with_enrollments do
      after(:build) do |charge|
        2.times { create(:enrollment, instance: create(:instance), charge: charge) }
      end
    end
  end
end
