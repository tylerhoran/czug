# frozen_string_literal: true

FactoryGirl.define do
  factory :school do
    association :user
    name { Faker::Company.name }
    fb_id { Faker::Bitcoin.address }
    school_type 'High School'
    degree 'Optometry'
  end
end
