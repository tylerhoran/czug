# frozen_string_literal: true

FactoryGirl.define do
  factory :evaluation do
    association :enrollment
    rating 1
  end
end
