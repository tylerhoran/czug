# frozen_string_literal: true

FactoryGirl.define do
  factory :slot do
    start_time '01:26:22'
    end_time '21:26:22'
    day 1
    association :user
  end
end
