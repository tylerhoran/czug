# frozen_string_literal: true

FactoryGirl.define do
  factory :instance do
    association :user, factory: :onboarded_professor
    association :course
    association :schedule
    association :availability
    status 'active'
    after :build do |i|
      i.class.skip_callback(:create, :after)
      build(:enrollment, instance: i)
    end
    syllabus_file_name 'syllabus.docs'
    syllabus_file_size { 1.megabyte }
  end
end
