# frozen_string_literal: true

FactoryGirl.define do
  factory :prerequisite do
    association :course
    association :prior, factory: :course
  end
end
