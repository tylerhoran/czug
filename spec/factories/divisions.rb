# frozen_string_literal: true

FactoryGirl.define do
  factory :division do
    name { Faker::Company.name }
  end
end
