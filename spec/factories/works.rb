# frozen_string_literal: true

FactoryGirl.define do
  factory :work do
    association :user
    name { Faker::Company.profession }
    position 'Owner'
    fb_id { Faker::Bitcoin.address }
  end
end
