# frozen_string_literal: true

FactoryGirl.define do
  factory :user do
    email { Faker::Internet.email }
    password 'please123'
    prefix { Faker::Name.prefix }
    first_name { Faker::Name.first_name }
    middle_name { Faker::Name.first_name }
    street_address { Faker::Address.street_address }
    city { Faker::Address.city }
    region { Faker::Address.state }
    postcode { Faker::Address.zip }
    country { Faker::Address.country }
    last_name { Faker::Name.last_name }
    suffix { Faker::Name.suffix }
    stripe_customer_token 'jj90dj293jd9302jd9230dj39'
    previous_transcript_file_name 'file.pdf'
    previous_transcript_file_size { 1.megabyte }
    vita_file_name 'file.pdf'
    vita_file_size { 1.megabyte }

    association :taught_degree, factory: :degree
    factory :onboarded_professor do
      prof true
      stripe_account_token 'jj90dj293jd9302jd9230dj39'
      after(:create) do |user|
        create(:card, card_type: 'debit', user: user, default: true)
      end
    end

    factory :admin do
      status 'approved'
      role 'admin'
      password { ENV['ADMIN_PASSWORD'] }
      email { ENV['ADMIN_EMAIL'] }
      after(:build) do |user|
        class << user
          def set_role
            true
          end
        end
      end
    end

    factory :onboarded_student do
      stripe_customer_token 'jj90dj293jd9302jd9230dj39'
      after(:create) do |user|
        create(:card, card_type: 'credit', user: user, default: true)
      end
    end
  end
end
