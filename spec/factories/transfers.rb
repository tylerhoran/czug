# frozen_string_literal: true

FactoryGirl.define do
  factory :transfer do
    association :card
    amount 1
    stripe_transfer_token 'MyString'
  end
end
