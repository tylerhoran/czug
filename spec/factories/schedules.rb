# frozen_string_literal: true

FactoryGirl.define do
  factory :schedule do
    # association :instance
    start_date { Time.current }
    day_1 'sunday'
    time_1 '21:10:59'
    day_2 'monday'
    time_2 '21:10:59'
  end
end
