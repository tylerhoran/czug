# frozen_string_literal: true

require 'rails_helper'

describe 'Accessign the admin', type: :feature do
  it 'as an approved admin' do
    visit new_user_session_path
    sign_in(ENV['ADMIN_EMAIL'], ENV['ADMIN_PASSWORD'])
    visit rails_admin.dashboard_path
    expect(page).to have_content 'Site Administration'
  end

  it 'as an approved professor' do
    user = create(:user, role: 'professor', status: 'approved')
    sign_in(user.email, 'please123')
    visit rails_admin.dashboard_path
    expect(page).not_to have_content 'Site Administration'
  end

  it 'as an approved student' do
    user = create(:user, role: 'student', status: 'approved')
    sign_in(user.email, 'please123')
    visit rails_admin.dashboard_path
    expect(page).not_to have_content 'Site Administration'
  end

  it 'an an signed out user' do
    visit rails_admin.dashboard_path
    expect(page).not_to have_content 'Site Administration'
  end

  def sign_in(email, password)
    visit new_user_session_path
    fill_in 'user_email', with: email
    fill_in 'user_password', with: password
    click_button 'Log in'
  end
end
