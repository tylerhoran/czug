# frozen_string_literal: true

require 'rails_helper'

describe 'Creating activities', type: :feature do
  let(:student) { create(:onboarded_student, status: 'approved') }
  let(:professor) { create(:onboarded_professor, status: 'approved') }
  let(:instance) { create(:instance, user: professor) }
  let(:availability) { create(:availability, user: professor) }
  let(:enrollment) { create(:enrollment, instance: instance, user: student) }
  let(:assignment) { create(:assignment, instance: instance) }

  it 'Adding an assignment to an instance', callbacks: true do
    enrollment.reload
    instance.reload
    assignment.reload
    sign_in student.email, 'please123'
    expect(page).to have_content 'added an assignment to'
  end

  it 'Professor adding an availability to degree courses', callbacks: true do
    degree = student.taught_degree
    course = create(:course, degree: degree)
    degree.enrolled_users.reload
    create(:availability, course: course, user: professor)
    sign_in student.email, 'please123'
    expect(page).to have_content 'A new course was added by'
  end

  it "Student enrolling to a professor's instance", callbacks: true do
    create(:reservation, user: student, availability: availability)
    sign_in professor.email, 'please123'
    expect(page).to have_content 'has reserved a seat in your upcoming class for'
  end

  it 'Grade added to a course instance', callbacks: true do
    enrollment.update(grade: 98)
    sign_in student.email, 'please123'
    expect(page).to have_content 'You can view your grades from the transcript tab on the left.'
  end

  it 'Meeting time changed by the course professor', callbacks: true do
    meeting = instance.meetings.first
    enrollment.reload
    meeting.update(occurrence: Time.current + 1.minute)
    sign_in student.email, 'please123'
    expect(page).to have_content 'changed the meeting time to'
  end

  it 'Grauation petition rejected by the admin', callbacks: true do
    create(:user, role: 'admin')
    petition = create(:petition, user: student)
    petition.rejected!
    sign_in student.email, 'please123'
    expect(page).to have_content 'Sorry. Your petition to graduate was not approved.'
  end

  it 'Grauation petition accepted by the admin', callbacks: true do
    create(:user, role: 'admin')
    petition = create(:petition, user: student)
    petition.approved!
    sign_in student.email, 'please123'
    expect(page).to have_content 'Congratulations! Your petition to graduate for the degree of'
  end

  it 'New post added to a course message board as student', callbacks: true do
    instance.reload
    enrollment.reload
    create(:post, instance: instance, user: student)
    sign_in professor.email, 'please123'
    expect(page).to have_content 'added a post in the messageboard'
  end

  it 'New post added to a course message board as professor', callbacks: true do
    instance.reload
    enrollment.reload
    create(:post, instance: instance, user: professor)
    sign_in student.email, 'please123'
    expect(page).to have_content 'added a post in the messageboard'
  end

  it 'A transfer was processed', callbacks: true do
    create(:user, role: 'admin')
    card = professor.cards.first
    create(:transfer, card: card)
    sign_in professor.email, 'please123'
    expect(page).to have_content 'A transfer in the amount of'
  end

  def sign_in(email, password)
    visit new_user_session_path
    fill_in 'user_email', with: email
    fill_in 'user_password', with: password
    click_button 'Log in'
  end
end
