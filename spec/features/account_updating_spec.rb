# frozen_string_literal: true

require 'rails_helper'

describe 'Updating Account information', type: :feature do
  let(:professor) { create(:onboarded_professor, status: 'approved') }

  it 'with required fields', callbacks: true do
    sign_in professor.email, 'please123'
    visit edit_user_registration_path
    fill_in 'user_first_name', with: Faker::Name.first_name
    click_button 'Save'
    expect(page).to have_content 'Your account has been updated successfully.'
  end

  def sign_in(email, password)
    visit new_user_session_path
    fill_in 'user_email', with: email
    fill_in 'user_password', with: password
    click_button 'Log in'
  end
end
