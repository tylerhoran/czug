# frozen_string_literal: true

require 'rails_helper'

describe 'Student onboarding', type: :feature do
  it 'from email', callbacks: true do
    division = create(:division)
    degree = create(:degree)
    division.degrees << degree
    course = create(:course, status: 'approved')
    degree.courses << course
    user2 = create(:user, prof: true, status: 'approved')
    user = create(:user, role: 'student', status: 'pending')
    user.approved!
    availability = create(:availability, user: user2, course: course)

    open_email(user.email)
    current_email.click_link 'Get Started →'

    fill_in 'user_email', with: user.email
    fill_in 'user_password', with: 'please123'
    click_button 'Log in'
    fill_in 'First name', with: Faker::Name.first_name
    fill_in 'Middle name', with: Faker::Name.first_name
    fill_in 'Last name', with: Faker::Name.last_name
    fill_in 'Street address', with: Faker::Address.street_address
    fill_in 'City', with: Faker::Address.city
    fill_in 'Postcode', with: Faker::Address.postcode
    fill_in 'Region', with: Faker::Address.state
    select 'United States', from: 'Country'
    create(:slot, day: 'sunday', user: user)
    create(:reservation, user: user, availability: availability)
    click_button 'next'
    select course.name, from: 'user_reservations_attributes_0_availability_id'
    click_button 'next'
    fill_in 'user_slots_attributes_0_start_time', with: '09:00AM'
    fill_in 'user_slots_attributes_0_end_time', with: '05:00PM'
    click_button 'next'
    fill_in 'user_cards_attributes_0_name', with: Faker::Name.name
    fill_in 'user_cards_attributes_0_expiration_month', with: '12'
    fill_in 'user_cards_attributes_0_expiration_year', with: '2019'
    fill_in 'user_cards_attributes_0_cvc', with: '345'
    fill_in 'user_cards_attributes_0_last_four', with: '3453'
    fill_in 'user_cards_attributes_0_token', with: 'd289hjd8hd89hd3892hd39h'
    click_button 'complete'
    expect(page).to have_text 'Welcome to The Brasden College!'
  end
end
