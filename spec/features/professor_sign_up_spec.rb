# frozen_string_literal: true

require 'rails_helper'

describe 'Professor signs up', type: :feature do
  before do
    clear_emails
  end
  let(:degree) { create(:degree) }

  it 'with valid email and password' do
    sign_up_with 'valid@example.com', 'password123'
    expect(page).to have_content('Thanks')
  end

  it 'with invalid email' do
    sign_up_with 'invalid_email', 'password'
    expect(page).to have_content('is invalid')
  end

  it 'triggers admin email', callbacks: true do
    sign_up_with 'valid@example.com', 'password123'
    open_email(ENV['ADMIN_EMAIL'])
    expect(current_email).to have_content 'New professor application'
  end

  it 'can be interviewed by admin', callbacks: true do
    sign_up_with 'valid@example.com', 'password123'
    open_email(ENV['ADMIN_EMAIL'])
    current_email.click_link 'Interview'
    sign_in(ENV['ADMIN_EMAIL'], ENV['ADMIN_PASSWORD'])
    open_email('valid@example.com')
    expect(current_email).to have_content "We'd like to invite you to interview with us!"
  end

  it 'can be approved by admin', callbacks: true do
    sign_up_with 'valid@example.com', 'password123'
    open_email(ENV['ADMIN_EMAIL'])
    current_email.click_link 'Approve'
    sign_in(ENV['ADMIN_EMAIL'], ENV['ADMIN_PASSWORD'])
    open_email('valid@example.com')
    expect(current_email).to have_content 'I am pleased to extend a formal offer to you'
  end

  it 'can be rejected by admin', callbacks: true do
    sign_up_with 'valid@example.com', 'password123'
    open_email(ENV['ADMIN_EMAIL'])
    current_email.click_link 'Reject'
    sign_in(ENV['ADMIN_EMAIL'], ENV['ADMIN_PASSWORD'])
    open_email('valid@example.com')
    expect(current_email).to have_content 'decided not to move forward with your admission'
  end

  def sign_up_with(email, password)
    degree.reload
    visit new_user_registration_path(professor: true)
    fill_in 'Full name', with: Faker::Name.name
    fill_in 'Email', with: email
    fill_in 'Password', with: password
    fill_in 'user_last_degree_major', with: Faker::Company.name
    fill_in 'user_last_degree_name', with: Faker::Company.name
    attach_file 'user_vita', Rails.root + 'spec/assets/transcript.pdf'
    select degree.name, from: 'user_taught_degree_id'
    select 'Doctorate', from: 'user_level'
    click_button 'Apply'
  end

  def sign_in(email, password)
    fill_in 'user_email', with: email
    fill_in 'user_password', with: password
    click_button 'Log in'
  end
end
