# frozen_string_literal: true

require 'rails_helper'

describe 'Errors', type: :feature do
  it 'Hitting the 404' do
    expect { visit '/fj20jd32d' }.to raise_error(ActionController::RoutingError)
  end

  it 'Hitting the 500' do
    expect { visit error_path }.to raise_error(RuntimeError)
  end
end
