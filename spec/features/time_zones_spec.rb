# frozen_string_literal: true

require 'rails_helper'

describe 'Accurate times across zones', type: :feature do
  let(:student) { create(:onboarded_student, status: 'approved') }
  let(:professor) { create(:onboarded_professor, status: 'approved') }
  let(:instance) { create(:instance, user: professor) }
  let(:enrollment) { create(:enrollment, instance: instance, user: student) }

  it 'creating an instance a zone and viewing from another zone', callbacks: true do
    enrollment.reload
    visit new_user_session_path
    Time.zone = ActiveSupport::TimeZone['Hong Kong']
    sign_in(student.email, 'please123')
    click_link 'access'
    expect(page).to have_content '5:10'
  end

  def sign_in(email, password)
    fill_in 'user_email', with: email
    fill_in 'user_password', with: password
    click_button 'Log in'
  end
end
