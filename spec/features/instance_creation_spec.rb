# frozen_string_literal: true

require 'rails_helper'

describe 'Scheduling and starting a course', type: :feature do
  let(:degree) { create(:degree) }
  let(:course) { create(:course, status: 'approved') }

  before do
    clear_emails
  end

  it 'from professor reservations email', callbacks: true do
    create_active_professor('valid@example.com', course, degree)
    create_active_students(10, course.availabilities.first)
    open_email('valid@example.com')
    current_email.click_link('Start your course')
    sign_in('valid@example.com', 'please123')
    fill_in 'availability_instances_attributes_0_schedule_attributes_start_date', with: Time.zone.today
    select 'Monday', from: 'availability_instances_attributes_0_schedule_attributes_day_1'
    fill_in 'availability_instances_attributes_0_schedule_attributes_time_1', with: '09:00AM'
    select 'Tuesday', from: 'availability_instances_attributes_0_schedule_attributes_day_2'
    fill_in 'availability_instances_attributes_0_schedule_attributes_time_2', with: '09:00AM'
    attach_file 'availability_instances_attributes_0_syllabus', Rails.root + 'spec/assets/transcript.pdf'
    click_button 'Launch Course'
    click_link 'Access your course'
    expect(page).to have_content '9:00'
  end

  def create_active_professor(email, course, degree)
    degree.courses << course
    user = create(:user, prof: true, status: 'pending', email: email, taught_degree: degree)
    user.approved!
    create(:availability, course: course, user: user, description: 'j902jd03')
    create(:slot, start_time: '09:00AM', end_time: '05:00PM', day: 0, user: user)
    create(:card, card_type: 'debit', user: user)
  end

  def create_active_students(number, availability)
    number.times do
      user = create(:user, role: 'student', status: 'approved')
      create(:card, user: user, card_type: 'credit')
      create(:slot, start_time: '09:00AM', end_time: '05:00PM', day: 0, user: user)
      create(:reservation, availability: availability, user: user)
    end
  end

  def sign_in(email, password)
    fill_in 'user_email', with: email
    fill_in 'user_password', with: password
    click_button 'Log in'
  end
end
