# frozen_string_literal: true

require 'rails_helper'

describe 'Assignments on an instance', type: :feature do
  let(:student) { create(:onboarded_student, status: 'approved') }
  let(:professor) { create(:onboarded_professor, status: 'approved') }
  let(:instance) { create(:instance, user: professor) }
  let(:enrollment) { create(:enrollment, instance: instance, user: student) }
  let(:assignment) { create(:assignment, instance: instance) }

  it 'creating from the instance page' do
    instance.reload
    visit new_user_session_path
    sign_in professor.email, 'please123'
    click_link 'access'
    click_link 'Add Assignment'
    fill_in 'assignment_name', with: Faker::Lorem.sentence
    fill_in 'assignment_body', with: Faker::Lorem.paragraph
    click_button 'Create'
    expect(page).to have_content 'Your assignment was created successfully'
  end

  it 'submitting assignments from the instance page' do
    enrollment.reload
    assignment.reload
    visit new_user_session_path
    sign_in student.email, 'please123'
    click_link 'access'
    click_link 'Submit Work'
    attach_file 'submission_attachment', Rails.root + 'spec/assets/transcript.pdf'
    click_button 'Upload'
    expect(page).to have_content 'Your submission was uploaded successfully'
  end

  def sign_in(email, password)
    fill_in 'user_email', with: email
    fill_in 'user_password', with: password
    click_button 'Log in'
  end
end
