# frozen_string_literal: true

require 'rails_helper'

RSpec.describe NotifyEndingCoursesToCompleteJob, type: :job do
  include ActiveJob::TestHelper

  subject(:job) { described_class.perform_later }

  let(:student) { create(:onboarded_student) }
  let(:professor) { create(:onboarded_professor) }
  let(:instance) { create(:instance, user: professor) }
  let(:schedule) { instance.schedule }
  let(:meeting) { create(:meeting, schedule: schedule) }
  let(:enrollment) { create(:enrollment, instance: instance, user: student) }

  before do
    schedule.meetings.destroy_all
    schedule.update(start_date: 12.weeks.ago)
  end

  it 'queues the job' do
    expect { job }.to change(ActiveJob::Base.queue_adapter.enqueued_jobs, :size).by(1)
  end

  it 'is in default queue' do
    expect(NotifyEndingCoursesToCompleteJob.new.queue_name).to eq('default')
  end

  describe 'outcomes' do
    before do
      perform_enqueued_jobs { job }
      instance.reload
    end
    it 'marks the instance as complete' do
      expect(instance.status).to eq('completed')
    end

    it 'sends the email to request grades' do
      expect(ActionMailer::Base.deliveries.last.subject).to match(/Time to submit grades/)
    end
  end

  after do
    clear_enqueued_jobs
    clear_performed_jobs
  end
end
