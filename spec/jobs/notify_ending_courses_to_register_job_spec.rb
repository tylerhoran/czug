# frozen_string_literal: true

require 'rails_helper'

RSpec.describe NotifyEndingCoursesToRegisterJob, type: :job do
  include ActiveJob::TestHelper

  subject(:job) { described_class.perform_later }

  let(:student) { create(:onboarded_student) }
  let(:professor) { create(:onboarded_professor) }
  let(:instance) { create(:instance, user: professor) }
  let(:schedule) { instance.schedule }
  let(:meeting) { create(:meeting, schedule: schedule) }
  let(:enrollment) { create(:enrollment, instance: instance, user: student) }

  it 'queues the job' do
    expect { job }.to change(ActiveJob::Base.queue_adapter.enqueued_jobs, :size).by(1)
  end

  it 'is in default queue' do
    expect(NotifyEndingCoursesToRegisterJob.new.queue_name).to eq('default')
  end

  it 'prompts students to register for new classes' do
    enrollment.reload
    schedule.meetings.destroy_all
    schedule.update(start_date: 10.weeks.ago)
    instance.create_meetings
    perform_enqueued_jobs { job }
    expect(ActionMailer::Base.deliveries.last.subject).to match(/Time to enroll in another/)
  end

  after do
    clear_enqueued_jobs
    clear_performed_jobs
  end
end
