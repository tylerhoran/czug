# frozen_string_literal: true

require 'rails_helper'

RSpec.describe ProcessPayoutsJob, type: :job do
  include ActiveJob::TestHelper
  subject(:job) { described_class.perform_later }

  let(:user) { create(:onboarded_professor) }

  it 'queues the job' do
    expect { job }.to change(ActiveJob::Base.queue_adapter.enqueued_jobs, :size).by(1)
  end

  it 'is in default queue' do
    expect(ProcessPayoutsJob.new.queue_name).to eq('default')
  end

  describe 'With a professor teaching a single course with 1 student enrolled this last week' do
    let(:instance) { create(:instance, user: user) }
    let(:enrollments) { create_list(:enrollment, 1, instance: instance, status: 'active') }

    before do
      create(:meeting, schedule: instance.schedule)
      enrollments.map(&:reload)
      Instance.where.not(id: instance.id).destroy_all
      perform_enqueued_jobs { job }
    end

    it 'transfers the correct amount' do
      expect(user.cards.first.transfers.last.amount).to eq(1441)
    end

    it 'delivers an accurate receipt' do
      expect(ActionMailer::Base.deliveries.last.body).to include('€14.41')
    end

    it 'delivers an accurate admin receipt' do
      expect(ActionMailer::Base.deliveries.last(2).first.body).to include('€14.42')
    end

    it 'updates the enrollment with a transfer amount' do
      enrollments.map(&:reload)
      expect(enrollments.map(&:transfer_amount).uniq).to eq([17])
    end
  end

  describe 'With a professor teaching a two courses with 1 students enrolled in either last week' do
    let(:instances) { create_list(:instance, 2, user: user) }
    let(:enrollments) { create_list(:enrollment, 2, instance: instances.sample, status: 'active') }

    before do
      create(:meeting, schedule: instances.first.schedule)
      create(:meeting, schedule: instances.last.schedule)
      enrollments.map(&:reload)
      Instance.where.not(id: instances.pluck(:id)).destroy_all
      perform_enqueued_jobs { job }
    end

    it 'transfers the correct amount' do
      expect(user.cards.first.transfers.last.amount).to eq(2883)
    end

    it 'delivers an accurate receipt' do
      expect(ActionMailer::Base.deliveries.last.body).to include('€28.83')
    end

    it 'delivers an accurate admin receipt' do
      expect(ActionMailer::Base.deliveries.last(2).first.body).to include('€28.84')
    end

    it 'updates the enrollment with a transfer amount' do
      enrollments.map(&:reload)
      expect(enrollments.map(&:transfer_amount).uniq).to eq([17])
    end
  end

  describe 'With a professor teaching four courses with 1 students enrolled in each last week' do
    let(:instances) { create_list(:instance, 4, user: user) }
    let(:enrollments) { create_list(:enrollment, 4, instance: instances.sample, status: 'active') }

    before do
      instances.map { |i| create(:meeting, schedule: i.schedule) }
      enrollments.map(&:reload)
      Instance.where.not(id: instances.pluck(:id)).destroy_all
      perform_enqueued_jobs { job }
    end

    it 'transfers the correct amount' do
      expect(user.cards.first.transfers.last.amount).to eq(5767)
    end

    it 'delivers an accurate receipt' do
      expect(ActionMailer::Base.deliveries.last.body).to include('€57.67')
    end

    it 'delivers an accurate admin receipt' do
      expect(ActionMailer::Base.deliveries.last(2).first.body).to include('€57.67')
    end

    it 'updates the enrollment with a transfer amount' do
      enrollments.map(&:reload)
      expect(enrollments.map(&:transfer_amount).uniq).to eq([17])
    end
  end

  describe 'With two professors teaching 1 course with 1 students inrolled in each last week' do
    let(:user2) { create(:onboarded_professor) }
    let(:instances1) { create_list(:instance, 1, user: user) }
    let(:instances2) { create_list(:instance, 1, user: user2) }
    let(:enrollments1) { create_list(:enrollment, 1, instance: instances1.sample, status: 'active') }
    let(:enrollments2) { create_list(:enrollment, 1, instance: instances2.sample, status: 'active') }

    before do
      instances1.each do |i|
        create(:meeting, schedule: i.schedule)
      end

      instances2.each do |i|
        create(:meeting, schedule: i.schedule)
      end
      enrollments1.map(&:reload)
      enrollments2.map(&:reload)
      Instance.where.not(id: (instances1.pluck(:id) + instances2.pluck(:id)).flatten).destroy_all
      perform_enqueued_jobs { job }
    end

    it 'transfers the correct amount' do
      expect(user.cards.first.transfers.last.amount).to eq(1441)
    end

    it 'delivers an accurate receipt' do
      expect(ActionMailer::Base.deliveries.last(2).first.body).to include('€14.42')
    end

    it 'delivers an accurate admin receipt' do
      expect(ActionMailer::Base.deliveries.last.body).to include('€28.82')
    end

    it 'updates the enrollment with a transfer amount' do
      enrollments1.map(&:reload)
      expect(enrollments1.map(&:transfer_amount).uniq).to eq([17])
    end
  end

  after do
    clear_enqueued_jobs
    clear_performed_jobs
  end
end
