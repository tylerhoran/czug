# frozen_string_literal: true

require 'rails_helper'

RSpec.describe RemindEmailJob, type: :job do
  include ActiveJob::TestHelper

  subject(:job) { described_class.perform_later }

  let(:email) { create(:email) }
  let(:onboarded_email) { create(:email) }
  let(:user) { create(:onboarded_student, email: onboarded_email.name) }

  it 'queues the job' do
    expect { job }.to change(ActiveJob::Base.queue_adapter.enqueued_jobs, :size).by(1)
  end

  it 'is in default queue' do
    expect(RemindEmailJob.new.queue_name).to eq('default')
  end

  it 'prompts email to complete application' do
    email.reload
    user.reload
    perform_enqueued_jobs { job }
    expect(ActionMailer::Base.deliveries.last.subject).to match(
      /Your application to The Brasden College is almost complete/
    )
  end
end
