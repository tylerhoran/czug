# frozen_string_literal: true
# require 'rails_helper'
#
# RSpec.describe UpdateAdsRatioJob, type: :job do
#   include ActiveJob::TestHelper
#
#   subject(:job) { described_class.perform_later }
#
#   it 'queues the job' do
#     expect { job }.to change(ActiveJob::Base.queue_adapter.enqueued_jobs, :size).by(1)
#   end
#
#   it 'is in default queue' do
#     expect(UpdateAdsRatioJob.new.queue_name).to eq('default')
#   end
#
#   it 'Calculates ratios' do
#     degree = create(:degree)
#     active_professor = create(:onboarded_professor, taught_degree: degree)
#     active_student = create(:onboarded_student, taught_degree: degree)
#     active_professor = create(:user, prof: true, taught_degree: degree)
#     active_student = create(:user, role: 'student', taught_degree: degree)
#     perform_enqueued_jobs { job }
#   end
# end
