# frozen_string_literal: true

require 'rails_helper'

RSpec.describe PromptEvaluationsJob, type: :job do
  include ActiveJob::TestHelper

  subject(:job) { described_class.perform_later }

  let(:student) { create(:onboarded_student) }
  let(:enrollment) { create(:enrollment, user: student) }

  it 'queues the job' do
    expect { job }.to change(ActiveJob::Base.queue_adapter.enqueued_jobs, :size).by(1)
  end

  it 'is in default queue' do
    expect(PromptEvaluationsJob.new.queue_name).to eq('default')
  end

  it 'prompts students to register for new classes' do
    enrollment.reload
    perform_enqueued_jobs { job }
    expect(ActionMailer::Base.deliveries.last.subject).to match(/Please provide feedback on your course/)
  end
end
