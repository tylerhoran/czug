# frozen_string_literal: true

require 'rails_helper'

describe CoursesController do
  let(:student) { create(:onboarded_student) }
  let(:professor) { create(:onboarded_professor) }

  describe 'GET #index' do
    describe 'as a student' do
      it 'redirects me to the dashboard' do
        login_with student
        get :index
        expect(response).to redirect_to root_path
      end
    end

    describe 'as a professor' do
      it 'renders the index' do
        login_with professor
        get :index
        expect(response).to render_template 'index'
      end
    end
  end

  describe 'GET #show' do
    describe 'as a visitor' do
      it 'redirects me to the dashboard' do
        course = create(:course, proposer: professor)
        get :show, params: { id: course.id }
        expect(response).to render_template 'show'
      end
    end
  end

  describe 'GET #new' do
    describe 'as a student' do
      it 'redirects me to the dashboard' do
        login_with student
        get :new
        expect(response).to redirect_to root_path
      end
    end
    describe 'as a professor' do
      it 'renders the new template' do
        login_with professor
        get :new
        expect(response).to render_template 'new'
      end
    end
  end

  describe 'POST #create' do
    describe 'as a student' do
      it 'redirects me to the dashboard' do
        login_with student
        post :create
        expect(response).to redirect_to root_path
      end
    end
    describe 'as a professor' do
      it 'creates the proposed course' do
        login_with professor
        course_attributes = { name: 'Test', description: 'Test', degree_id: create(:degree).id }
        expect { post :create, params: { course: course_attributes } }.to change { Course.count }.by(1)
      end
    end
  end

  describe 'DELETE #destroy' do
    describe 'as a student' do
      it 'redirects me to the dashboard' do
        login_with student
        course = create(:course, proposer: professor)
        delete :destroy, params: { id: course.id }
        expect(response).to redirect_to root_path
      end
    end
    describe 'as the proposer of the course' do
      it 'deletes the course' do
        login_with professor
        course = create(:course, proposer: professor)
        expect { delete :destroy, params: { id: course.id } }.to change { Course.count }.by(-1)
      end
    end
  end
end
