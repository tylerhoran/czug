# frozen_string_literal: true

require 'rails_helper'

describe SlotsController do
  let(:student) { create(:onboarded_student) }
  let(:professor) { create(:onboarded_professor) }

  describe 'GET #index' do
    describe 'as a professor' do
      it 'renders the user slots' do
        login_with student
        get :index
        expect(response).to render_template 'index'
      end
    end

    describe 'as a student' do
      it 'renders the user slots' do
        login_with professor
        get :index
        expect(response).to render_template 'index'
      end
    end
  end
end
