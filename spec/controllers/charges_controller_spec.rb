# frozen_string_literal: true

require 'rails_helper'

describe ChargesController do
  let(:professor) { create(:onboarded_professor) }
  let(:student) { create(:onboarded_student) }

  describe '#GET index' do
    describe 'as a professor' do
      it 'redirects to the homepage' do
        login_with professor
        get :index
        expect(response).to redirect_to root_path
      end
    end

    describe 'as a student' do
      it 'renders the index' do
        login_with student
        get :index
        expect(response).to render_template 'index'
      end
    end
  end
end
