# frozen_string_literal: true

require 'rails_helper'

describe EnrollmentsController do
  let(:student) { create(:onboarded_student) }
  let(:professor) { create(:onboarded_professor) }
  let(:instance) { create(:instance, user: professor) }
  let(:enrollment) { create(:enrollment, user: student, instance: instance) }

  describe 'GET #index' do
    describe 'as a student' do
      it 'renders the index template' do
        login_with student
        get :index
        expect(response).to render_template 'index'
      end
    end

    describe 'as a professor' do
      it 'renders the index template' do
        login_with student
        get :index
        expect(response).to render_template 'index'
      end
    end
  end

  describe 'PATCH #update' do
    describe 'as a professor of the course enrollment' do
      describe 'with valid fields' do
        it 'updates the grade' do
          login_with professor
          patch :update, format: :json, params: { id: enrollment.id, enrollment: { grade: 98 } }
          enrollment.reload
          expect(enrollment.grade).to eq(98)
        end
      end
      describe 'with invalid fields' do
        it 'renders errors' do
          login_with professor
          patch :update, format: :json, params: { id: enrollment.id, enrollment: { grade: 'jd3' } }
          expect(response.body).to eq('["Grade is not included in the list"]')
        end
      end
    end

    describe 'as a professor who did not create the course' do
      it 'redirects to the home page' do
        prof2 = create(:onboarded_professor)
        login_with prof2
        patch :update, params: { id: enrollment.id, enrollment: { grade: 98 } }
        expect(response).to redirect_to root_path
      end
    end

    describe 'as a student' do
      it 'redirects to the home page' do
        login_with student
        patch :update, params: { id: enrollment.id, enrollment: { grade: 98 } }
        expect(response).to redirect_to root_path
      end
    end
  end
end
