# frozen_string_literal: true

require 'rails_helper'

RSpec.describe ErrorsController, type: :controller do
  before do
    Rails.application.config.consider_all_requests_local = false
    Rails.application.config.action_dispatch.show_exceptions = true
  end

  after do
    Rails.application.config.consider_all_requests_local = true
    Rails.application.config.action_dispatch.show_exceptions = false
  end

  describe 'GET #not_found' do
    it 'returns http success' do
      get :not_found
      expect(response).to have_http_status(404)
    end
  end

  describe 'GET #internal_server_error' do
    it 'returns http success' do
      get :internal_server_error
      expect(response).to have_http_status(500)
    end
  end
end
