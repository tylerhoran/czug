# frozen_string_literal: true

require 'rails_helper'

describe DivisionsController do
  describe 'GET #index' do
    it 'populates an array of divisions' do
      division = create(:division)
      get :index
      expect(assigns(:divisions)).to include(division)
    end

    it 'renders the :index view' do
      get :index
      expect(response).to render_template :index
    end
  end
end
