# frozen_string_literal: true

require 'rails_helper'

describe AssignmentsController do
  let(:professor) { create(:onboarded_professor) }
  let(:instance) { create(:instance, user: professor) }
  let(:assignment) { create(:assignment) }
  let(:student) { create(:onboarded_student) }
  let(:enrollment) { create(:enrollment, instance: instance, user: student) }

  describe 'GET #new' do
    describe 'as a student' do
      it 'redirects to the homepage' do
        login_with student
        get :new, params: { instance_id: instance.id }
        expect(response).to redirect_to root_path
      end
    end

    describe 'as a professor who created the course' do
      it 'renders the new template' do
        login_with professor
        get :new, params: { instance_id: instance.id }
        expect(response).to render_template 'new'
      end
    end

    describe 'as a professor who not in the course' do
      it 'redirects to the homepage' do
        user = create(:onboarded_professor)
        login_with user
        get :new, params: { instance_id: instance.id }
        expect(response).to redirect_to root_path
      end
    end
  end

  describe 'GET #show' do
    let(:assignment) { create(:assignment, instance: instance) }

    describe 'as a student' do
      describe 'enrolled in the course' do
        it 'redirects to the homepage' do
          enrollment.reload
          login_with student
          get :show, params: { instance_id: instance.id, id: assignment.id }
          expect(response).to redirect_to root_path
        end
      end

      describe 'not enrolled in the course' do
        it 'redirects to the homepage' do
          user = create(:onboarded_student)
          login_with user
          get :show, params: { instance_id: instance.id, id: assignment.id }
          expect(response).to redirect_to root_path
        end
      end
    end

    describe 'as a professor who created the course' do
      it 'renders the new template' do
        login_with professor
        get :show, params: { instance_id: instance.id, id: assignment.id }
        expect(response).to render_template 'show'
      end
    end

    describe 'as a professor who not in the course' do
      it 'redirects to the homepage' do
        user = create(:onboarded_professor)
        login_with user
        get :show, params: { instance_id: instance.id, id: assignment.id }
        expect(response).to redirect_to root_path
      end
    end
  end

  describe 'GET #edit' do
    let(:assignment) { create(:assignment, instance: instance) }

    describe 'as a student' do
      it 'redirects to the homepage' do
        login_with student
        get :edit, params: { instance_id: instance.id, id: assignment.id }
        expect(response).to redirect_to root_path
      end
    end

    describe 'as a professor who created the course' do
      it 'renders the new template' do
        login_with professor
        get :edit, params: { instance_id: instance.id, id: assignment.id }
        expect(response).to render_template 'edit'
      end
    end

    describe 'as a professor who not in the course' do
      it 'redirects to the homepage' do
        user = create(:onboarded_professor)
        login_with user
        get :edit, params: { instance_id: instance.id, id: assignment.id }
        expect(response).to redirect_to root_path
      end
    end
  end

  describe 'POST #create' do
    let(:assignment_params) { build(:assignment, instance: instance).attributes }

    describe 'as a student' do
      it 'redirects to the homepage' do
        login_with student
        post :create, params: { instance_id: instance.id, assignment: assignment_params }
        expect(response).to redirect_to root_path
      end
    end

    describe 'as a professor who created the course' do
      it 'renders the new template' do
        login_with professor
        expect do
          post :create, params: {
            instance_id: instance.id,
            assignment: assignment_params,
          }
        end.to change { Assignment.count }.by(1)
      end
    end

    describe 'as a professor who not in the course' do
      it 'redirects to the homepage' do
        user = create(:onboarded_professor)
        login_with user
        post :create, params: { instance_id: instance.id, assignment: assignment_params }
        expect(response).to redirect_to root_path
      end
    end
  end

  describe 'PATCH #update' do
    let(:assignment) { create(:assignment, instance: instance) }

    describe 'as a student' do
      it 'redirects to the homepage' do
        login_with student
        patch :update, params: {
          instance_id: instance.id,
          id: assignment.id,
          assignment: {
            name: 'you',
          },
        }
        assignment.reload
        expect(assignment.name).not_to eq('you')
      end
    end

    describe 'as a professor who created the course' do
      it 'renders the new template' do
        login_with professor
        patch :update, params: {
          instance_id: instance.id,
          id: assignment.id,
          assignment: {
            name: 'you',
          },
        }
        assignment.reload
        expect(assignment.name).to eq('you')
      end
    end

    describe 'as a professor who not in the course' do
      it 'redirects to the homepage' do
        user = create(:onboarded_professor)
        login_with user
        patch :update, params: {
          instance_id: instance.id,
          id: assignment.id,
          assignment: {
            name: 'you',
          },
        }
        assignment.reload
        expect(assignment.name).not_to eq('you')
      end
    end
  end

  describe 'delete #destroy' do
    let(:assignment) { create(:assignment, instance: instance) }

    describe 'as a student' do
      it 'redirects to the homepage' do
        login_with student
        assignment.reload
        expect do
          delete :destroy, params: {
            instance_id: instance.id,
            id: assignment.id,
          }
        end.to change { Assignment.count }.by(0)
      end
    end

    describe 'as a professor who created the course' do
      it 'deletes the assignment' do
        login_with professor
        assignment.reload
        expect do
          delete :destroy, params: {
            instance_id: instance.id,
            id: assignment.id,
          }
        end.to change { Assignment.count }.by(-1)
      end
    end

    describe 'as a professor who not in the course' do
      it 'redirects to the homepage' do
        user = create(:onboarded_professor)
        login_with user
        assignment.reload
        expect do
          delete :destroy, params: {
            instance_id: instance.id,
            id: assignment.id,
          }
        end.to change { Assignment.count }.by(0)
      end
    end
  end
end
