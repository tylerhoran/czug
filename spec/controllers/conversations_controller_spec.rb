# frozen_string_literal: true

require 'rails_helper'

describe ConversationsController do
  let(:user) { create(:onboarded_professor) }

  describe 'GET #index' do
    it 'renders the index template' do
      login_with user
      get :index
      expect(response).to render_template 'index'
    end
  end

  describe 'GET #show' do
    it 'renders the conversation template' do
      user.send_message(create(:user), 'testing', 'testing')
      login_with user
      get :show, params: { id: Mailboxer::Conversation.last.id }
      expect(response).to render_template 'show'
    end
  end

  describe 'POST #reply' do
    it 'creates a reply' do
      user.send_message(create(:user), 'testing', 'testing')
      login_with user
      expect do
        post :reply, params: {
          id: Mailboxer::Conversation.last.id,
          body: 'Hello',
        }
      end.to change { Mailboxer::Message.count }.by(1)
    end
  end
end
