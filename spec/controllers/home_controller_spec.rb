# frozen_string_literal: true

require 'rails_helper'

describe HomeController do
  describe 'GET #index' do
    it 'renders the :index view' do
      get :index
      expect(response).to render_template :index
    end
  end

  describe 'GET #about' do
    it 'renders the :about view' do
      get :about
      expect(response).to render_template :about
    end
  end

  describe 'GET #privacy' do
    it 'renders the :privacy view' do
      get :privacy
      expect(response).to render_template :privacy
    end
  end

  describe 'GET #terms' do
    it 'renders the :terms view' do
      get :terms
      expect(response).to render_template :terms
    end
  end

  describe 'GET #faq' do
    it 'populates an array of faqs' do
      faq = create(:faq)
      get :faq
      expect(assigns(:faqs)).to include(faq)
    end

    it 'renders the :faq view' do
      get :faq
      expect(response).to render_template :faq
    end
  end

  describe 'GET #tuition' do
    it 'renders the :tuition view' do
      get :tuition
      expect(response).to render_template :tuition
    end
  end

  describe 'GET #academics' do
    it 'renders the :academics view' do
      get :academics
      expect(response).to render_template :academics
    end
  end

  describe 'GET #thanks' do
    it 'renders the :thanks view' do
      get :thanks
      expect(response).to render_template :thanks
    end
  end

  describe 'GET #blog' do
    it 'renders the :blog view' do
      get :blog
      expect(response).to render_template :blog
    end
  end

  describe 'GET #cookies_policy' do
    it 'renders the :cookies_policy view' do
      get :cookies_policy
      expect(response).to render_template :cookies_policy
    end
  end
end
