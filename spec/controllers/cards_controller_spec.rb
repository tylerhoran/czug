# frozen_string_literal: true

require 'rails_helper'

describe CardsController do
  let(:professor) { create(:user, prof: true, status: 'approved') }
  let(:student) { create(:user, role: 'student', status: 'approved') }
  let(:student_card) { create(:card, user: student, card_type: 'credit') }
  let(:professor_card) { create(:card, user: professor, card_type: 'debit') }

  describe 'GET #new' do
    describe 'as a student' do
      it 'renders the new template' do
        login_with student
        get :new
        expect(response).to render_template 'new'
      end
    end

    describe 'as a professor' do
      it 'renders the new template' do
        login_with professor
        get :new
        expect(response).to render_template 'new'
      end
    end
  end

  describe 'POST #create' do
    describe 'as a student' do
      let(:card_attributes) { build(:card, card_type: 'credit').attributes }

      describe 'with valid fields' do
        it 'creates a new card' do
          login_with student
          expect { post :create, params: { card: card_attributes } }.to change { Card.count }.by(1)
        end
      end
      describe 'with invalid fields' do
        it 'does not create the card' do
          login_with student
          card_attributes['last_four'] = nil
          expect { post :create, params: { card: card_attributes } }.to change { Card.count }.by(0)
        end
      end
    end

    describe 'as a professor' do
      let(:card_attributes) { build(:card, card_type: 'debit').attributes }

      describe 'with valid fields' do
        it 'creates a new card' do
          login_with professor
          expect { post :create, params: { card: card_attributes } }.to change { Card.count }.by(1)
        end
      end
      describe 'with invalid fields' do
        it 'does not create the card' do
          login_with professor
          card_attributes['last_four'] = nil
          expect { post :create, params: { card: card_attributes } }.to change { Card.count }.by(0)
        end
      end
    end
  end

  describe 'DELETE #destroy' do
    describe 'as a student' do
      it 'deletes the card' do
        login_with student
        delete :destroy, params: { id: student_card.id }
        student_card.reload
        expect(student_card.status).to eq('removed')
      end
    end

    describe 'as a professor' do
      it 'deletes the card' do
        login_with professor
        delete :destroy, params: { id: professor_card.id }
        professor_card.reload
        expect(professor_card.status).to eq('removed')
      end
    end
  end
end
