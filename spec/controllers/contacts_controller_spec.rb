# frozen_string_literal: true

require 'rails_helper'

describe ContactsController do
  describe 'GET #new' do
    it 'populates a new contact' do
      get :new
      expect(assigns(:contact)).not_to be_nil
    end

    it 'renders the :new view' do
      get :new
      expect(response).to render_template :new
    end
  end

  describe 'POST #create' do
    describe 'with valid fields' do
      before do
        post :create, params: { contact: {
          first_name: Faker::Name.first_name,
          last_name: Faker::Name.last_name,
          email: Faker::Internet.email,
          subject: Faker::Lorem.sentence,
          message: Faker::Lorem.paragraph,
        } }
      end
      it 'sends a contact mail' do
        expect(ActionMailer::Base.deliveries.last.body).not_to be_nil
      end

      it 'rendirect to the homepage' do
        expect(response).to redirect_to root_path
      end
    end

    describe 'with invalid fields' do
      before do
        post :create, params: { contact: {
          first_name: Faker::Name.first_name,
          last_name: Faker::Name.last_name,
        } }
      end

      it 'renders the :new view' do
        get :new
        expect(response).to render_template :new
      end
    end
  end
end
