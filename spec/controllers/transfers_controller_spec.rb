# frozen_string_literal: true

require 'rails_helper'

describe TransfersController do
  let(:professor) { create(:onboarded_professor) }
  let(:student) { create(:onboarded_student) }

  describe 'GET #index' do
    describe 'as a professor' do
      it 'renders the index' do
        login_with professor
        get :index
        expect(response).to render_template 'index'
      end
    end

    describe 'as a student' do
      it 'redirects to the homepage' do
        login_with student
        get :index
        expect(response).to redirect_to root_path
      end
    end
  end
end
