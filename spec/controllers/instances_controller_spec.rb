# frozen_string_literal: true

require 'rails_helper'

describe InstancesController do
  let(:student) { create(:onboarded_student) }
  let(:professor) { create(:onboarded_professor) }
  let(:instance) { create(:instance, user: professor) }
  let(:enrollment) { create(:enrollment, instance: instance, user: student) }

  describe 'GET #show' do
    describe 'as a student not in the course' do
      it 'redirects to the home page' do
        login_with student
        get :show, params: { id: instance.id }
        expect(response).to redirect_to root_path
      end
    end

    describe 'as a student in the course' do
      it 'renders the show template' do
        enrollment.reload
        login_with student
        get :show, params: { id: instance.id }
        expect(response).to render_template 'show'
      end
    end

    describe 'as a professor who created the course' do
      it 'renders the show template' do
        login_with professor
        get :show, params: { id: instance.id }
        expect(response).to render_template 'show'
      end
    end

    describe 'as a professor who did not create the course' do
      it 'redirects to the home page' do
        prof2 = create(:onboarded_professor)
        login_with prof2
        get :show, params: { id: instance.id }
        expect(response).to redirect_to root_path
      end
    end
  end

  describe 'PATCH #update' do
    describe 'as a student not in the course' do
      it 'redirects to the home page' do
        login_with student
        patch :update, format: :json, params: { id: instance.id, instance: { description: 'j902d3' } }
        expect(response.status).to eq(403)
      end
    end

    describe 'as a student in the course' do
      it 'redirects to the home page' do
        login_with student
        patch :update, format: :json, params: { id: instance.id, instance: { description: 'j902d3' } }
        expect(response.status).to eq(403)
      end
    end

    describe 'as a professor who created the course' do
      describe 'with valid fields' do
        it 'updates the course' do
          login_with professor
          patch :update, format: :json, params: { id: instance.id, instance: { description: 'j902d3' } }
          instance.reload
          expect(instance.description).to eq('j902d3')
        end
      end
    end

    describe 'as a professor who did not create the course' do
      it 'redirects to the home page' do
        login_with student
        patch :update, format: :json, params: { id: instance.id, instance: { description: 'j902d3' } }
        expect(response.status).to eq(403)
      end
    end
  end
end
