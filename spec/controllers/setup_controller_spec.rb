# frozen_string_literal: true

require 'rails_helper'

describe SetupController do
  let(:student) do
    create(:user,
      role: 'student',
      status: 'approved',
      first_name: nil,
      middle_name: nil,
      last_name: nil,
      street_address: nil,
      prefix: nil,
      city: nil,
      region: nil,
      postcode: nil,
      country: nil,
      suffix: nil)
  end
  let(:professor) do
    create(:user,
      prof: true,
      status: 'approved',
      stripe_account_token: 'j920dj309jdd3',
      first_name: nil,
      middle_name: nil,
      last_name: nil,
      street_address: nil,
      prefix: nil,
      city: nil,
      region: nil,
      postcode: nil,
      country: nil,
      suffix: nil,
      last_sign_in_ip: '192.168.1.1')
  end

  describe 'GET #show' do
    describe 'as a student' do
      describe 'on the profile step' do
        it 'renders the profile page' do
          login_with student
          get :show, params: { id: 'profile' }
          expect(response).to render_template 'profile'
        end
      end

      describe 'on the first courses step' do
        it 'renders the first courses page' do
          login_with student
          get :show, params: { id: 'first_courses' }
          expect(response).to render_template 'first_courses'
        end
      end

      describe 'on the teachable course step' do
        it 'redirects to the availability page' do
          login_with student
          get :show, params: { id: 'teachable_courses' }
          expect(response).to redirect_to setup_path(id: 'availability')
        end
      end

      describe 'on the availability step' do
        it 'renders the availability page' do
          login_with student
          get :show, params: { id: 'profile' }
          expect(response).to render_template 'profile'
        end
      end

      describe 'on the payment step' do
        it 'renders the payment page' do
          login_with student
          get :show, params: { id: 'payment' }
          expect(response).to render_template 'payment'
        end
      end

      describe 'on the transfer step' do
        it 'redirects to the root page' do
          login_with student
          get :show, params: { id: 'transfer' }
          expect(response).to redirect_to setup_path(id: 'wicked_finish')
        end
      end
    end

    describe 'as a professor' do
      describe 'on the profile step' do
        it 'renders the profile page' do
          login_with professor
          get :show, params: { id: 'profile' }
          expect(response).to render_template 'profile'
        end
      end

      describe 'on the first courses step' do
        it 'redirects to the teachable courses step' do
          login_with professor
          get :show, params: { id: 'first_courses' }
          expect(response).to redirect_to setup_path(id: 'teachable_courses')
        end
      end

      describe 'on the teachable course step' do
        it 'redirects to the availability page' do
          login_with professor
          get :show, params: { id: 'teachable_courses' }
          expect(response).to render_template 'teachable_courses'
        end
      end

      describe 'on the availability step' do
        it 'renders the availability page' do
          login_with professor
          get :show, params: { id: 'profile' }
          expect(response).to render_template 'profile'
        end
      end

      describe 'on the payment step' do
        it 'redirects to the transfers step' do
          login_with professor
          get :show, params: { id: 'payment' }
          expect(response).to redirect_to setup_path(id: 'transfer')
        end
      end

      describe 'on the transfer step' do
        it 'redirects to the root page' do
          login_with professor
          get :show, params: { id: 'transfer' }
          expect(response).to render_template 'transfer'
        end
      end
    end
  end

  describe 'GET #update' do
    describe 'as a student' do
      describe 'on the profile step' do
        it 'with valid fields updates the user and move to the next step' do
          login_with student
          user_params = {
            first_name: Faker::Name.first_name,
            middle_name: 'J',
            last_name: Faker::Name.last_name,
            photo: fixture_file_upload(Rails.root.join('spec', 'assets', 'avatar.jpg'), 'image/jpeg'),
            street_address: Faker::Address.street_address,
            city: Faker::Address.city,
            region: 'MA',
            postcode: '923d9j',
            country: 'US',
            step: 'profile',
          }
          patch :update, params: { id: 'profile', user: user_params }
          expect(response).to redirect_to setup_path(id: 'first_courses')
        end

        it 'with invalid fields does not update the user and render the same step' do
          login_with student
          user_params = { first_name: Faker::Name.first_name, step: 'profile' }
          patch :update, params: { id: 'profile', user: user_params }
          expect(response).to render_template 'profile'
        end
      end

      describe 'on the first courses step' do
        let(:course) { create(:course, degree: student.taught_degree) }
        let(:availability) { create(:availability, user: professor, course: course) }

        it 'with valid fields updates the user and move to the next step' do
          login_with student
          user_params = {
            reservations_attributes: [{ availability_id: availability.id }],
            step: 'first_courses',
          }
          patch :update, params: { id: 'first_courses', user: user_params }
          expect(response).to redirect_to setup_path(id: 'teachable_courses')
        end

        it 'with invalid fields does not update the user and render the same step' do
          login_with student
          patch :update, params: { id: 'first_courses', user: { first_name: '2903d', step: 'first_courses' } }
          expect(response).to render_template 'first_courses'
        end
      end

      describe 'on the availability step' do
        it 'with valid fields updates the user and move to the next step' do
          login_with student
          user_params = {
            step: 'availability',
            slots_attributes: { '0' => {
              start_time: Time.current + 1.hour,
              end_time: Time.current + 4.hours,
              day: 'sunday',
            }, '1' => {
              start_time: Time.current + 1.hour,
              end_time: Time.current + 4.hours,
              day: 'monday',
            } },
          }
          patch :update, params: { id: 'availability', user: user_params }
          expect(response).to redirect_to setup_path(id: 'payment')
        end

        it 'with invalid fields does not update the user and render the same step' do
          login_with student
          user_params = { first_name: 'jf93', step: 'availability' }
          patch :update, params: { id: 'availability', user: user_params }
          expect(response).to render_template 'availability'
        end
      end

      describe 'on the payment step' do
        it 'with valid fields updates the user and move to the final step' do
          login_with student
          user_params = {
            step: 'payment',
            cards_attributes: [{
              name: Faker::Name.name,
              token: '902jd90j3902jd930j29d032',
              card_type: 'credit',
              currency: 'US',
              expiration: '',
              expiration_month: '12',
              last_four: '9898',
              expiration_year: '2065',
              cvc: '393',
            }],
          }
          patch :update, params: { id: 'payment', user: user_params }
          expect(response).to redirect_to setup_path(id: 'transfer')
        end

        it 'with invalid fields does not update the user and render the same step' do
          login_with student
          user_params = { first_name: 'jf93', step: 'payment' }
          patch :update, params: { id: 'payment', user: user_params }
          expect(response).to render_template 'payment'
        end
      end
    end

    describe 'as a professor' do
      describe 'on the profile step' do
        it 'with valid fields updates the user and move to the next step' do
          login_with professor
          user_params = {
            first_name: Faker::Name.first_name,
            middle_name: 'J',
            last_name: Faker::Name.last_name,
            photo: fixture_file_upload(Rails.root.join('spec', 'assets', 'avatar.jpg'), 'image/jpeg'),
            street_address: Faker::Address.street_address,
            city: Faker::Address.city,
            region: 'MA',
            postcode: '923d9j',
            country: 'US',
            step: 'profile',
          }
          patch :update, params: { id: 'profile', user: user_params }
          expect(response).to redirect_to setup_path(id: 'first_courses')
        end

        it 'with invalid fields does not update the user and render the same step' do
          login_with professor
          user_params = { first_name: Faker::Name.first_name, step: 'profile' }
          patch :update, params: { id: 'profile', user: user_params }
          expect(response).to render_template 'profile'
        end
      end

      describe 'on the teachable course step' do
        it 'with valid fields updates the user and move to the next step' do
          course = create(:course)
          login_with professor
          user_params = {
            step: 'teachable_courses',
            availabilities_attributes: [{
              course_id: course.id,
            }],
          }
          patch :update, params: { id: 'teachable_courses', user: user_params }
          expect(response).to redirect_to setup_path(id: 'availability')
        end

        it 'with invalid fields does not update the user and render the same step' do
          create(:course)
          login_with professor
          user_params = {
            step: 'teachable_courses',
            firs_name: '3j9',
          }
          patch :update, params: { id: 'teachable_courses', user: user_params }
          expect(response).to render_template 'teachable_courses'
        end
      end

      describe 'on the availability step' do
        it 'with valid fields updates the user and move to the next step' do
          login_with professor
          user_params = {
            step: 'availability',
            slots_attributes: { '0' => {
              start_time: Time.current + 1.hour,
              end_time: Time.current + 4.hours,
              day: 'sunday',
            }, '1' => {
              start_time: Time.current + 1.hour,
              end_time: Time.current + 4.hours,
              day: 'monday',
            } },
          }
          patch :update, params: { id: 'availability', user: user_params }
          expect(response).to redirect_to setup_path(id: 'payment')
        end

        it 'with invalid fields does not update the user and render the same step' do
          login_with professor
          user_params = { first_name: 'jf93', step: 'availability' }
          patch :update, params: { id: 'availability', user: user_params }
          expect(response).to render_template 'availability'
        end
      end

      describe 'on the transfer step' do
        it 'with valid fields updates the user and move to the final step' do
          login_with professor
          user_params = {
            step: 'transfer',
            cards_attributes: [{
              name: Faker::Name.name,
              token: '902jd90j3902jd930j29d032',
              card_type: 'debit',
              currency: 'US',
              expiration: '',
              expiration_month: '12',
              last_four: '9898',
              expiration_year: '2065',
              cvc: '393',
            }],
          }
          patch :update, params: { id: 'transfer', user: user_params }
          expect(response).to redirect_to setup_path(id: 'wicked_finish')
        end

        it 'with invalid fields does not update the user and render the same step' do
          login_with professor
          user_params = { first_name: 'jf93', step: 'transfer' }
          patch :update, params: { id: 'transfer', user: user_params }
          expect(response).to render_template 'transfer'
        end
      end
    end
  end
end
