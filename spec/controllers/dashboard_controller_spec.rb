# frozen_string_literal: true

require 'rails_helper'

describe DashboardController do
  let(:user) { create(:onboarded_student) }

  describe 'GET #index' do
    it 'renders the activities and index' do
      login_with user
      get :index
      expect(response).to render_template 'index'
    end
  end
end
