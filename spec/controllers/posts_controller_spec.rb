# frozen_string_literal: true

require 'rails_helper'

describe PostsController do
  let(:student) { create(:onboarded_student) }
  let(:professor) { create(:onboarded_professor) }
  let(:instance) { create(:instance, user: professor) }
  let(:enrollment) { create(:enrollment, user: student, instance: instance) }

  describe 'POST #create' do
    describe 'as the professor of the course' do
      it 'creates the post' do
        login_with professor
        expect do
          post :create, params: {
            instance_id: instance.id,
            post: {
              body: 'Hello!',
            },
          }
        end.to change { Post.count }.by(1)
      end
    end

    describe 'as a student in the course' do
      it 'creates the post' do
        login_with student
        enrollment.reload
        expect do
          post :create, params: {
            instance_id: instance.id,
            post: {
              body: 'Hello!',
            },
          }
        end.to change { Post.count }.by(1)
      end
    end

    describe 'as a student not in the course' do
      it 'redirect to the root path' do
        login_with student
        post :create, params: { instance_id: instance.id, post: { body: 'Hello!' } }
        expect(response).to redirect_to root_path
      end
    end

    describe 'as a professor not of the course' do
      it 'redirect to the root path' do
        prof2 = create(:onboarded_professor)
        login_with prof2
        post :create, params: { instance_id: instance.id, post: { body: 'Hello!' } }
        expect(response).to redirect_to root_path
      end
    end
  end
end
