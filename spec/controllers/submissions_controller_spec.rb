# frozen_string_literal: true

require 'rails_helper'

describe SubmissionsController do
  let(:student) { create(:onboarded_student) }
  let(:professor) { create(:onboarded_professor) }
  let(:instance) { create(:instance, user: professor) }
  let(:assignment) { create(:assignment, instance: instance) }
  let(:enrollment) { create(:enrollment, user: student, instance: instance) }

  describe 'GET #new' do
    describe 'as a professor of the course' do
      it 'renders the new template' do
        login_with professor
        get :new, params: { instance_id: instance.id, assignment_id: assignment.id }
        expect(response).to render_template 'new'
      end
    end

    describe 'as a professor not of the course' do
      it 'redirects to the homepage' do
        professor2 = create(:onboarded_professor)
        login_with professor2
        get :new, params: { instance_id: instance.id, assignment_id: assignment.id }
        expect(response).to redirect_to root_path
      end
    end

    describe 'as a student in the course' do
      it 'renders the new template' do
        enrollment.save
        login_with student
        get :new, params: { instance_id: instance.id, assignment_id: assignment.id }
        expect(response).to render_template 'new'
      end
    end

    describe 'as a student not in the course' do
      it 'redirects to the homepage' do
        login_with student
        get :new, params: { instance_id: instance.id, assignment_id: assignment.id }
        expect(response).to redirect_to root_path
      end
    end
  end

  describe 'POST #create' do
    describe 'as a professor of the course' do
      it 'creates the submission' do
        login_with professor
        submission_params = {
          body: 'here is it!',
          attachment: fixture_file_upload(Rails.root.join('spec', 'assets', 'avatar.jpg'), 'image/jpeg'),
        }
        expect do
          post :create, params: {
            instance_id: instance.id,
            assignment_id: assignment.id,
            submission: submission_params,
          }
        end.to change { Submission.count }.by(1)
      end
    end

    describe 'as a professor not of the course' do
      it 'redirects to the homepage' do
        professor2 = create(:onboarded_professor)
        login_with professor2
        submission_params = {
          body: 'here is it!',
          attachment: fixture_file_upload(Rails.root.join('spec', 'assets', 'avatar.jpg'), 'image/jpeg'),
        }
        post :create, params: {
          instance_id: instance.id,
          assignment_id: assignment.id,
          submission: submission_params,
        }
        expect(response).to redirect_to root_path
      end
    end

    describe 'as a student in the course' do
      it 'creates the submission' do
        enrollment.save
        login_with student
        submission_params = {
          body: 'here is it!',
          attachment: fixture_file_upload(Rails.root.join('spec', 'assets', 'avatar.jpg'), 'image/jpeg'),
        }
        expect do
          post :create, params: {
            instance_id: instance.id,
            assignment_id: assignment.id,
            submission: submission_params,
          }
        end.to change { Submission.count }.by(1)
      end
    end

    describe 'as a student not in the course' do
      it 'redirects to the homepage' do
        login_with student
        submission_params = {
          body: 'here is it!',
          attachment: fixture_file_upload(Rails.root.join('spec', 'assets', 'avatar.jpg'), 'image/jpeg'),
        }
        post :create, params: {
          instance_id: instance.id,
          assignment_id: assignment.id,
          submission: submission_params,
        }
        expect(response).to redirect_to root_path
      end
    end
  end
end
