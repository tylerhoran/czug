# frozen_string_literal: true

require 'rails_helper'

describe MeetingsController do
  let(:student) { create(:onboarded_student) }
  let(:professor) { create(:onboarded_professor) }
  let(:instance) { create(:instance, user: professor) }
  let(:schedule) { instance.schedule }
  let(:meeting) { create(:meeting, schedule: schedule) }
  let(:enrollment) { create(:enrollment, instance: instance, user: student) }

  describe 'GET #show' do
    describe 'as a student in the course' do
      it 'renders the show template' do
        enrollment.reload
        login_with student
        get :show, params: { instance_id: instance.id, id: meeting.id }
        expect(response).to render_template 'show'
      end
    end

    describe 'as a student not in the course' do
      it 'redirecs to the root path' do
        login_with student
        get :show, params: { instance_id: instance.id, id: meeting.id }
        expect(response).to redirect_to root_path
      end
    end

    describe 'as a professor of the course' do
      it 'renders the show template' do
        login_with professor
        get :show, params: { instance_id: instance.id, id: meeting.id }
        expect(response).to render_template 'show'
      end
    end

    describe 'as a professor not in the course' do
      it 'redirects to the root path' do
        prof2 = create(:onboarded_professor)
        login_with prof2
        get :show, params: { instance_id: instance.id, id: meeting.id }
        expect(response).to redirect_to root_path
      end
    end
  end

  describe 'GET #edit' do
    describe 'as a student in the course' do
      it 'redirects to the root path' do
        enrollment.reload
        login_with student
        get :edit, params: { instance_id: instance.id, id: meeting.id }
        expect(response).to redirect_to root_path
      end
    end

    describe 'as a student not in the course' do
      it 'redirects to the root path' do
        login_with student
        get :edit, params: { instance_id: instance.id, id: meeting.id }
        expect(response).to redirect_to root_path
      end
    end

    describe 'as a professor of the course' do
      it 'renders the show template' do
        login_with professor
        get :edit, params: { instance_id: instance.id, id: meeting.id }
        expect(response).to render_template 'edit'
      end
    end

    describe 'as a professor not in the course' do
      it 'redirects to the root path' do
        prof2 = create(:onboarded_professor)
        login_with prof2
        get :edit, params: { instance_id: instance.id, id: meeting.id }
        expect(response).to redirect_to root_path
      end
    end
  end

  describe 'PATCH #update' do
    describe 'as a student in the course' do
      it 'redirects to the root path' do
        enrollment.reload
        login_with student
        patch :update, params: {
          instance_id: instance.id,
          id: meeting.id,
          meeting: {
            occurrence: Time.current + 2.days,
          },
        }
        expect(response).to redirect_to root_path
      end
    end

    describe 'as a student not in the course' do
      it 'redirects to the root path' do
        login_with student
        patch :update, params: {
          instance_id: instance.id,
          id: meeting.id,
          meeting: {
            occurrence: Time.current + 2.days,
          },
        }
        expect(response).to redirect_to root_path
      end
    end

    describe 'as a professor of the course' do
      describe 'with valid fields' do
        it 'updates the occurrence' do
          login_with professor
          time = Time.current + 2.days
          patch :update, params: {
            instance_id: instance.id,
            id: meeting.id,
            meeting: {
              occurrence: time,
            },
          }
          meeting.reload
          expect(meeting.occurrence.utc.to_i).to eq(time.utc.to_i)
        end
      end

      describe 'with invalid fields' do
        it 'does not update the occurrence' do
          login_with professor
          patch :update, params: {
            instance_id: instance.id,
            id: meeting.id,
            meeting: {
              occurrence: '',
            },
          }
          meeting.reload
          expect(meeting.occurrence).not_to eq('')
        end
      end
    end

    describe 'as a professor not in the course' do
      it 'redirects to the root path' do
        prof2 = create(:onboarded_professor)
        login_with prof2
        patch :update, params: {
          instance_id: instance.id,
          id: meeting.id,
          meeting: {
            occurrence: Time.current + 2.days,
          },
        }
        expect(response).to redirect_to root_path
      end
    end
  end
end
