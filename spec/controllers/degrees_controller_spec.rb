# frozen_string_literal: true

require 'rails_helper'

describe DegreesController do
  let(:division) { create(:division) }
  let(:degree) { create(:degree) }
  let(:course) { create(:course, status: 'approved') }

  describe 'GET #show' do
    before do
      division.degrees << degree
      degree.courses << course
    end

    it 'populates an array of courses' do
      get :show, params: { division_id: division.id, id: degree.id }
      expect(assigns(:courses)).to include(course)
    end

    it 'renders the :show view' do
      get :show, params: { division_id: division.id, id: degree.id }
      expect(response).to render_template :show
    end
  end
end
