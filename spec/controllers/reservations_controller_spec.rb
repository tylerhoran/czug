# frozen_string_literal: true

require 'rails_helper'

describe ReservationsController do
  let(:student) { create(:onboarded_student) }
  let(:professor) { create(:onboarded_professor) }

  describe 'GET #index' do
    describe 'as a student' do
      it 'renders the index page' do
        login_with student
        get :index
        expect(response).to render_template 'index'
      end
    end

    describe 'as a professor' do
      it 'redirects to the homepage' do
        login_with professor
        get :index
        expect(response).to redirect_to root_path
      end
    end
  end

  describe 'DELETE #destroy' do
    let(:reservation) { create(:reservation, user: student) }

    describe 'as the owner' do
      it 'deletes and render the index page' do
        reservation.reload
        login_with student
        expect { delete :destroy, params: { id: reservation.id } }.to change { Reservation.count }.by(-1)
      end
    end

    describe 'as a professor' do
      it 'redirects to the homepage' do
        login_with professor
        delete :destroy, params: { id: reservation.id }
        expect(response).to redirect_to root_path
      end
    end
  end
end
