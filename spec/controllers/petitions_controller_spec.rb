# frozen_string_literal: true

require 'rails_helper'

describe PetitionsController do
  let(:student) { create(:onboarded_student) }
  let(:enrollment) { create(:enrollment, user: user) }

  describe 'POST #create' do
    it 'creates the petition for the user' do
      login_with student
      expect { post :create }.to change { Petition.count }.by(1)
    end
  end

  describe 'GET #show' do
    it 'creates the petition for the user' do
      petition = create(:petition, user: student)
      login_with student
      get :show, params: { id: petition.id }
      expect(response).to render_template 'show'
    end
  end
end
