# frozen_string_literal: true

require 'rails_helper'

describe AvailabilitiesController do
  let(:professor) { create(:onboarded_professor) }
  let(:availability) { create(:availability, user: professor) }
  let(:student) { create(:onboarded_student) }

  describe 'GET #index' do
    describe 'as a student' do
      it 'redirects to the homepage' do
        login_with student
        get :index
        expect(response).to redirect_to root_path
      end
    end

    describe 'as a professor' do
      it 'renders the new template' do
        login_with professor
        get :index
        expect(response).to render_template 'index'
      end
    end
  end

  describe 'GET #show' do
    let(:availability) { create(:availability, user: professor) }

    describe 'as a student' do
      it 'redirects to the homepage' do
        login_with student
        get :show, params: { id: availability.id }
        expect(response).to redirect_to root_path
      end
    end

    describe 'as a professor who created the availability' do
      it 'renders the new template' do
        login_with professor
        get :show, params: { id: availability.id }
        expect(response).to render_template 'show'
      end
    end

    describe 'as a professor who did not create the availability' do
      it 'redirects to the homepage' do
        prof2 = create(:onboarded_professor)
        login_with prof2
        get :show, params: { id: availability.id }
        expect(response).to redirect_to root_path
      end
    end
  end

  describe 'PATCH #update' do
    let(:availability) { create(:availability, user: professor) }

    describe 'as a student' do
      it 'redirects to the homepage' do
        login_with student
        patch :update, params: { id: availability.id, availability: { course_id: create(:course).id } }
        expect(response).to redirect_to root_path
      end
    end

    describe 'as a professor who created the availability' do
      describe 'with valid fields' do
        it 'creates an instance with a schedule and enrollments' do
          login_with professor
          instance_attributes = build(:instance).attributes
          instance_attributes['syllabus'] = fixture_file_upload(
            Rails.root + 'spec/assets/transcript.pdf', 'application/pdf'
            )
          instance_attributes['schedule_attributes'] = build(:schedule).attributes
          instance_attributes['enrollments_attributes'] = build(:enrollment).attributes
          expect do
            patch :update, params: {
              id: availability.id,
              availability: {
                instances_attributes: instance_attributes,
              },
            }
          end.to change { Instance.count }.by(1)
        end
      end

      describe 'with invalid fields' do
        it 'creates an instance with a schedule and enrollments' do
          login_with professor
          instance_attributes = build(:instance).attributes
          instance_attributes['schedule_attributes'] = build(:schedule).attributes
          instance_attributes['enrollments_attributes'] = build(:enrollment).attributes
          patch :update, params: {
            id: availability.id,
            availability: {
              instances_attributes: instance_attributes,
            },
          }
          expect(response).to render_template 'show'
        end
      end
    end

    describe 'as a professor who did not create the availability' do
      it 'redirects to the homepage' do
        prof2 = create(:onboarded_professor)
        login_with prof2
        instance_attributes = build(:instance).attributes
        instance_attributes['syllabus'] = fixture_file_upload(
          Rails.root + 'spec/assets/transcript.pdf', 'application/pdf'
        )
        instance_attributes['schedule_attributes'] = build(:schedule).attributes
        instance_attributes['enrollments_attributes'] = build(:enrollment).attributes
        patch :update, params: {
          id: availability.id,
          availability: {
            instances_attributes: instance_attributes,
          },
        }
        expect(response).to redirect_to root_path
      end
    end
  end

  describe 'DELETE #destroy' do
    let(:availability) { create(:availability, user: professor) }

    describe 'as a student' do
      it 'redirects the user to the dashboard' do
        login_with student
        delete :destroy, params: { id: availability.id }
        expect(response).to redirect_to root_path
      end
    end

    describe 'as the professor' do
      it 'removes the availability' do
        login_with professor
        availability.reload
        expect do
          delete :destroy, params: {
            id: availability.id,
          }
        end.to change { Availability.count }.by(-1)
      end
    end

    describe 'as another professor' do
      it 'redirects to the home page' do
        prof2 = create(:onboarded_professor)
        login_with prof2
        delete :destroy, params: { id: availability.id }
        expect(response).to redirect_to root_path
      end
    end
  end
end
