# frozen_string_literal: true

require 'rails_helper'

describe UsersController do
  let(:student) { create(:onboarded_student) }
  let(:professor) { create(:onboarded_professor) }
  let(:admin) { User.admin.first }
  let(:course) { create(:course, degree: student.taught_degree) }
  let(:availability) { create(:availability, user: professor, course: course) }

  describe 'GET #show' do
    it 'renders the user profile' do
      login_with professor
      get :show, params: { id: student.id }
      expect(response).to render_template 'show'
    end
  end

  describe 'PATCH #update' do
    describe 'updating reservations' do
      describe 'with valid fields' do
        it 'updates the user reservations' do
          login_with student
          expect do
            patch :update, params: {
              id: student.id,
              user: {
                reservations_attributes: [
                  {
                    availability_id: availability.id,
                  },
                ],
              },
            }
          end.to change { Reservation.count }.by(1)
        end
      end

      describe 'with invalid fields' do
        it 'does not update the user reservations' do
          login_with student
          request.env['HTTP_REFERER'] = reservations_path
          patch :update, params: {
            id: student.id,
            user: {
              reservations_attributes: [
                {
                  availability_id: '32893',
                },
              ],
            },
          }
          expect(response).to render_template 'reservations/index'
        end
      end
    end

    describe 'updating availabilities' do
      describe 'with valid fields' do
        it 'updates the user availabilities' do
          login_with professor
          expect do
            patch :update, params: {
              id: professor.id,
              user: {
                availabilities_attributes: [
                  {
                    course_id: course.id,
                  },
                ],
              },
            }
          end.to change { Availability.count }.by(1)
        end
      end

      describe 'with invalid fields' do
        it 'does not update the user availabilities' do
          login_with professor
          request.env['HTTP_REFERER'] = availabilities_path
          patch :update, params: {
            id: professor.id,
            user: {
              availabilities_attributes: [
                {
                  course_id: 38_293,
                },
              ],
            },
          }
          expect(response).to render_template 'availabilities/index'
        end
      end
    end

    describe 'updating slots' do
      describe 'with valid fields' do
        it 'updates the user availabilities' do
          login_with professor
          expect do
            patch :update, params: {
              id: professor.id,
              user: {
                slots_attributes: [
                  {
                    start_time: Time.current,
                    end_time: Time.current + 2.hours,
                    day: 'monday',
                  },
                ],
              },
            }
          end.to change { Slot.count }.by(1)
        end
      end

      describe 'with invalid fields' do
        it 'does not update the user availabilities' do
          login_with professor
          request.env['HTTP_REFERER'] = slots_path
          patch :update, params: {
            id: professor.id,
            user: {
              slots_attributes: [
                {
                  start_time: Time.current,
                },
              ],
            },
          }
          expect(response).to render_template 'slots/index'
        end
      end
    end
  end

  describe 'GET #reject' do
    describe 'as a student' do
      it 'redirects to the root path' do
        login_with student
        get :reject, params: { id: student.id }
        expect(response).to redirect_to root_path
      end
    end

    describe 'as an admin' do
      it 'renders the reject template and reject the user' do
        login_with admin
        get :reject, params: { id: student.id }
        expect(response).to render_template 'reject'
      end
    end

    describe 'as a professor' do
      it 'redirects to the root path' do
        login_with professor
        get :reject, params: { id: student.id }
        expect(response).to redirect_to root_path
      end
    end
  end

  describe 'GET #interview' do
    describe 'as a student' do
      it 'redirects to the root path' do
        login_with student
        get :interview, params: { id: student.id }
        expect(response).to redirect_to root_path
      end
    end

    describe 'as an admin' do
      it 'renders the interview template and interview the user' do
        login_with admin
        get :interview, params: { id: student.id }
        expect(response).to render_template 'interview'
      end
    end

    describe 'as a professor' do
      it 'redirects to the root path' do
        login_with professor
        get :interview, params: { id: student.id }
        expect(response).to redirect_to root_path
      end
    end
  end

  describe 'GET #approve' do
    describe 'as a student' do
      it 'redirects to the root path' do
        login_with student
        get :approve, params: { id: student.id }
        expect(response).to redirect_to root_path
      end
    end

    describe 'as an admin' do
      it 'renders the approve template and approve the user' do
        login_with admin
        get :approve, params: { id: student.id }
        expect(response).to render_template 'approve'
      end
    end

    describe 'as a professor' do
      it 'redirects to the root path' do
        login_with professor
        get :approve, params: { id: student.id }
        expect(response).to redirect_to root_path
      end
    end
  end

  describe 'GET #student_welcome' do
    it 'renders the student welcome template' do
      login_with student
      get :student_welcome, params: { id: student.id }
      expect(response).to render_template 'student_welcome'
    end
  end

  describe 'GET #professor_welcome' do
    it 'renders the professor welcome template' do
      login_with professor
      get :professor_welcome, params: { id: professor.id }
      expect(response).to render_template 'professor_welcome'
    end
  end
end
