# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Schedule, type: :model do
  let(:schedule) { build_stubbed(:schedule) }

  it 'is valid' do
    expect(schedule).to be_valid
  end

  describe 'should require' do
    it 'a start time' do
      schedule.start_date = nil
      expect(schedule).not_to be_valid
    end

    it 'a time 1' do
      schedule.time_1 = nil
      expect(schedule).not_to be_valid
    end

    it 'a day 1' do
      schedule.day_1 = nil
      expect(schedule).not_to be_valid
    end

    it 'a time 2' do
      schedule.time_2 = nil
      expect(schedule).not_to be_valid
    end

    it 'a day 2' do
      schedule.day_2 = nil
      expect(schedule).not_to be_valid
    end
  end
end
