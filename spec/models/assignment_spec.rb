# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Assignment, type: :model do
  let(:assignment) { build_stubbed(:assignment) }

  it 'is valid' do
    expect(assignment).to be_valid
  end

  describe 'should require' do
    it 'an instance' do
      assignment.instance = nil
      expect(assignment).not_to be_valid
    end

    it 'a name' do
      assignment.name = nil
      expect(assignment).not_to be_valid
    end
  end
end
