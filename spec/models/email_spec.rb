# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Email, type: :model do
  let(:email) { build_stubbed(:email) }

  it 'is valid' do
    expect(email).to be_valid
  end

  describe 'should require' do
    it 'a name' do
      email.name = nil
      expect(email).not_to be_valid
    end
  end
end
