# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Slot, type: :model do
  let(:slot) { build_stubbed(:slot) }

  it 'is valid' do
    expect(slot).to be_valid
  end

  describe 'should require' do
    it 'a day' do
      slot.day = nil
      expect(slot).not_to be_valid
    end

    it 'a end time' do
      slot.end_time = nil
      expect(slot).not_to be_valid
    end

    it 'a start time' do
      slot.start_time = nil
      expect(slot).not_to be_valid
    end
  end
end
