# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Faq, type: :model do
  let(:faq) { build_stubbed(:faq) }

  it 'is valid' do
    expect(faq).to be_valid
  end

  describe 'should require' do
    it 'a title' do
      faq.title = nil
      expect(faq).not_to be_valid
    end

    it 'a body' do
      faq.body = nil
      expect(faq).not_to be_valid
    end

    it 'a type' do
      faq.faq_type = nil
      expect(faq).not_to be_valid
    end
  end
end
