# frozen_string_literal: true

require 'rails_helper'
require 'cancan/matchers'

describe Ability do
  describe 'as a professor' do
    let(:user) { create(:user, prof: true, status: 'approved') }
    let(:ability) { described_class.new(user) }

    describe 'admin' do
      it 'cannot access' do
        expect(ability).not_to be_able_to(:access, :rails_admin)
      end
    end

    describe 'activity' do
      describe 'of another user' do
        let(:activity) { build(:activity) }

        it 'cannot read' do
          expect(ability).not_to be_able_to(:read, activity)
        end
        it 'cannot delete' do
          expect(ability).not_to be_able_to(:delete, activity)
        end
        it 'cannot create' do
          expect(ability).not_to be_able_to(:create, activity)
        end
        it 'cannot update' do
          expect(ability).not_to be_able_to(:update, activity)
        end
      end

      describe 'of their feed' do
        let(:activity) { build(:activity, user: user) }

        it 'can read' do
          expect(ability).to be_able_to(:read, activity)
        end
        it 'cannot delete' do
          expect(ability).not_to be_able_to(:delete, activity)
        end
        it 'cannot create' do
          expect(ability).not_to be_able_to(:create, activity)
        end
        it 'cannot update' do
          expect(ability).not_to be_able_to(:update, activity)
        end
      end
    end

    describe 'assignment' do
      describe "of a course they're not teaching" do
        let(:enrollment) { create(:enrollment) }
        let(:assignment) { create(:assignment, instance: enrollment.instance) }

        before do
          enrollment.reload
        end

        it 'cannot read' do
          expect(ability).not_to be_able_to(:read, assignment)
        end
        it 'cannot delete' do
          expect(ability).not_to be_able_to(:delete, assignment)
        end
        it 'cannot create' do
          expect(ability).not_to be_able_to(:create, assignment)
        end
        it 'cannot update' do
          expect(ability).not_to be_able_to(:update, assignment)
        end
      end

      describe "of a course they're currently teaching" do
        let(:enrollment) { create(:enrollment) }
        let(:assignment) { create(:assignment, instance: enrollment.instance) }

        before do
          enrollment.instance.update(user: user)
        end

        it 'can read' do
          expect(ability).to be_able_to(:read, assignment)
        end
        it 'can delete' do
          expect(ability).to be_able_to(:delete, assignment)
        end
        it 'can create' do
          expect(ability).to be_able_to(:create, assignment)
        end
        it 'can update' do
          expect(ability).to be_able_to(:update, assignment)
        end
      end
    end

    describe 'availability' do
      describe 'of their own' do
        let(:availability) { build(:availability, user: user) }

        it 'can read' do
          expect(ability).to be_able_to(:read, availability)
        end
        it 'can delete' do
          expect(ability).to be_able_to(:delete, availability)
        end
        it 'can create' do
          expect(ability).to be_able_to(:create, availability)
        end
        it 'can update' do
          expect(ability).to be_able_to(:update, availability)
        end
      end
      describe 'of another prof' do
        let(:availability) { build(:availability) }

        it 'cannot read' do
          expect(ability).not_to be_able_to(:read, availability)
        end
        it 'cannot delete' do
          expect(ability).not_to be_able_to(:delete, availability)
        end
        it 'cannot create' do
          expect(ability).not_to be_able_to(:create, availability)
        end
        it 'cannot update' do
          expect(ability).not_to be_able_to(:update, availability)
        end
      end
    end

    describe 'card' do
      describe 'another user card' do
        let(:card) { build(:card) }

        it 'cannot read' do
          expect(ability).not_to be_able_to(:read, card)
        end
        it 'cannot delete' do
          expect(ability).not_to be_able_to(:delete, card)
        end
        it 'cannot create' do
          expect(ability).not_to be_able_to(:create, card)
        end
        it 'cannot update' do
          expect(ability).not_to be_able_to(:update, card)
        end
      end

      describe 'their own cards' do
        let(:card) { build(:card, user: user) }

        it 'can read' do
          expect(ability).to be_able_to(:read, card)
        end
        it 'can delete' do
          expect(ability).to be_able_to(:delete, card)
        end
        it 'can create' do
          expect(ability).to be_able_to(:create, card)
        end
        it 'cannot update' do
          expect(ability).not_to be_able_to(:update, card)
        end
      end
    end

    describe 'charge' do
      describe 'of another user' do
        let(:charge) { build(:charge) }

        it 'cannot read' do
          expect(ability).not_to be_able_to(:read, charge)
        end
        it 'cannot delete' do
          expect(ability).not_to be_able_to(:delete, charge)
        end
        it 'cannot create' do
          expect(ability).not_to be_able_to(:create, charge)
        end
        it 'cannot update' do
          expect(ability).not_to be_able_to(:update, charge)
        end
      end
    end

    describe 'contact' do
      it 'cannot read' do
        expect(ability).not_to be_able_to(:read, Contact)
      end
      it 'cannot delete' do
        expect(ability).not_to be_able_to(:delete, Contact)
      end
      it 'can create' do
        expect(ability).to be_able_to(:create, Contact)
      end
      it 'cannot update' do
        expect(ability).not_to be_able_to(:update, Contact)
      end
    end

    describe 'course' do
      describe 'of their own' do
        let(:course) { build(:course, proposer: user) }

        it 'can read' do
          expect(ability).to be_able_to(:read, course)
        end
        it 'can delete' do
          expect(ability).to be_able_to(:delete, course)
        end
        it 'can create' do
          expect(ability).to be_able_to(:create, course)
        end
        it 'can update' do
          expect(ability).to be_able_to(:update, course)
        end
      end
      describe 'of another prof' do
        let(:course) { build(:course) }

        it 'can read' do
          expect(ability).to be_able_to(:read, course)
        end
        it 'cannot delete' do
          expect(ability).not_to be_able_to(:delete, course)
        end
        it 'cannot create' do
          expect(ability).not_to be_able_to(:create, course)
        end
        it 'cannot update' do
          expect(ability).not_to be_able_to(:update, course)
        end
      end
    end

    describe 'degree' do
      let(:degree) { build(:degree) }

      it 'can read' do
        expect(ability).to be_able_to(:read, degree)
      end
      it 'cannot delete' do
        expect(ability).not_to be_able_to(:delete, degree)
      end
      it 'cannot create' do
        expect(ability).not_to be_able_to(:create, degree)
      end
      it 'cannot update' do
        expect(ability).not_to be_able_to(:update, degree)
      end
    end

    describe 'division' do
      let(:division) { build(:division) }

      it 'can read' do
        expect(ability).to be_able_to(:read, division)
      end
      it 'cannot delete' do
        expect(ability).not_to be_able_to(:delete, division)
      end
      it 'cannot create' do
        expect(ability).not_to be_able_to(:create, division)
      end
      it 'cannot update' do
        expect(ability).not_to be_able_to(:update, division)
      end
    end

    describe 'enrollment' do
      describe 'of the prof students' do
        let(:enrollment) { build(:enrollment) }

        before do
          enrollment.instance.update(user: user)
        end

        it 'can read' do
          expect(ability).to be_able_to(:read, enrollment)
        end
        it 'can delete' do
          expect(ability).to be_able_to(:delete, enrollment)
        end
        it 'can create' do
          expect(ability).to be_able_to(:create, enrollment)
        end
        it 'can update' do
          expect(ability).to be_able_to(:update, enrollment)
        end
      end

      describe 'of another prof students' do
        let(:enrollment) { build(:enrollment) }

        it 'cannot read' do
          expect(ability).not_to be_able_to(:read, enrollment)
        end
        it 'cannot delete' do
          expect(ability).not_to be_able_to(:delete, enrollment)
        end
        it 'cannot create' do
          expect(ability).not_to be_able_to(:create, enrollment)
        end
        it 'cannot update' do
          expect(ability).not_to be_able_to(:update, enrollment)
        end
      end
    end

    describe 'faq' do
      let(:faq) { build(:faq) }

      it 'can read' do
        expect(ability).to be_able_to(:read, faq)
      end
      it 'cannot delete' do
        expect(ability).not_to be_able_to(:delete, faq)
      end
      it 'cannot create' do
        expect(ability).not_to be_able_to(:create, faq)
      end
      it 'cannot update' do
        expect(ability).not_to be_able_to(:update, faq)
      end
    end

    describe 'instance' do
      describe 'not created' do
        let(:instance) { build(:instance) }

        it 'cannot read' do
          expect(ability).not_to be_able_to(:read, instance)
        end
        it 'cannot delete' do
          expect(ability).not_to be_able_to(:delete, instance)
        end
        it 'cannot create' do
          expect(ability).not_to be_able_to(:create, instance)
        end
        it 'cannot update' do
          expect(ability).not_to be_able_to(:update, instance)
        end
      end

      describe 'of their own' do
        let(:enrollment) { create(:enrollment, user: user) }
        let(:instance) { enrollment.instance }

        before do
          enrollment.instance.update(user: user)
        end

        it 'can read' do
          expect(ability).to be_able_to(:read, instance)
        end
        it 'can delete' do
          expect(ability).to be_able_to(:delete, instance)
        end
        it 'can create' do
          expect(ability).to be_able_to(:create, instance)
        end
        it 'can update' do
          expect(ability).to be_able_to(:update, instance)
        end
      end
    end

    describe 'meeting' do
      describe 'of course not created' do
        let(:meeting) { build(:meeting) }

        it 'cannot read' do
          expect(ability).not_to be_able_to(:read, meeting)
        end

        it 'cannot delete' do
          expect(ability).not_to be_able_to(:delete, meeting)
        end

        it 'cannot create' do
          expect(ability).not_to be_able_to(:create, meeting)
        end

        it 'cannot update' do
          expect(ability).not_to be_able_to(:update, meeting)
        end
      end

      describe 'of course created' do
        let(:instance) { create(:instance) }
        let(:schedule) { create(:schedule, instance: instance) }
        let(:meeting) { create(:meeting, schedule: schedule) }

        let(:enrollment) { create(:enrollment, instance: instance, user: user) }

        before do
          instance.update(user: user)
        end

        it 'can read' do
          expect(ability).to be_able_to(:read, meeting)
        end

        it 'can delete' do
          expect(ability).to be_able_to(:delete, meeting)
        end

        it 'can create' do
          expect(ability).to be_able_to(:create, meeting)
        end

        it 'can update' do
          expect(ability).to be_able_to(:update, meeting)
        end
      end
    end

    describe 'petition' do
      describe 'of other people' do
        let(:petition) { build(:petition) }

        it 'cannot read' do
          expect(ability).not_to be_able_to(:read, petition)
        end
        it 'cannot delete' do
          expect(ability).not_to be_able_to(:delete, petition)
        end
        it 'cannot create' do
          expect(ability).not_to be_able_to(:create, petition)
        end
        it 'cannot update' do
          expect(ability).not_to be_able_to(:update, petition)
        end
      end
      describe 'of their own' do
        let(:petition) { build(:petition, user: user) }

        it 'cannot read' do
          expect(ability).to be_able_to(:read, petition)
        end
        it 'cannot delete' do
          expect(ability).to be_able_to(:delete, petition)
        end
        it 'cannot create' do
          expect(ability).to be_able_to(:create, petition)
        end
        it 'cannot update' do
          expect(ability).to be_able_to(:update, petition)
        end
      end
    end

    describe 'post' do
      describe 'of a course not created' do
        let(:post) { build(:post) }

        it 'cannot read' do
          expect(ability).not_to be_able_to(:read, post)
        end
        it 'cannot delete' do
          expect(ability).not_to be_able_to(:delete, post)
        end
        it 'cannot create' do
          expect(ability).not_to be_able_to(:create, post)
        end
        it 'cannot update' do
          expect(ability).not_to be_able_to(:update, post)
        end
      end

      describe 'of a course created' do
        let(:enrollment) { create(:enrollment, user: user) }
        let(:post) { build(:post, instance: enrollment.instance) }

        before do
          enrollment.instance.update(user: user)
        end

        it 'can read' do
          expect(ability).to be_able_to(:read, post)
        end
        it 'can delete' do
          expect(ability).to be_able_to(:delete, post)
        end
        it 'can create' do
          expect(ability).to be_able_to(:create, post)
        end
        it 'can update' do
          expect(ability).to be_able_to(:update, post)
        end
      end
    end

    describe 'prerequisite' do
      let(:prerequisite) { build(:prerequisite) }

      it 'can read' do
        expect(ability).to be_able_to(:read, prerequisite)
      end
      it 'can delete' do
        expect(ability).to be_able_to(:delete, prerequisite)
      end
      it 'can create' do
        expect(ability).to be_able_to(:create, prerequisite)
      end
      it 'can update' do
        expect(ability).to be_able_to(:update, prerequisite)
      end
    end

    describe 'refund' do
      let(:refund) { build(:refund) }

      it 'cannot read' do
        expect(ability).not_to be_able_to(:read, refund)
      end
      it 'cannot delete' do
        expect(ability).not_to be_able_to(:delete, refund)
      end
      it 'cannot create' do
        expect(ability).not_to be_able_to(:create, refund)
      end
      it 'cannot update' do
        expect(ability).not_to be_able_to(:update, refund)
      end
    end

    describe 'reservation' do
      describe 'for other prof availabilities' do
        let(:reservation) { build(:reservation) }

        it 'cannot read' do
          expect(ability).not_to be_able_to(:read, reservation)
        end
        it 'cannot delete' do
          expect(ability).not_to be_able_to(:delete, reservation)
        end
        it 'cannot create' do
          expect(ability).not_to be_able_to(:create, reservation)
        end
        it 'cannot update' do
          expect(ability).not_to be_able_to(:update, reservation)
        end
      end

      describe 'for their availabilities' do
        let(:availability) { build(:availability, user: user) }
        let(:reservation) { build(:reservation, availability: availability) }

        it 'can read' do
          expect(ability).to be_able_to(:read, reservation)
        end
        it 'can delete' do
          expect(ability).to be_able_to(:delete, reservation)
        end
        it 'can create' do
          expect(ability).to be_able_to(:create, reservation)
        end
        it 'can update' do
          expect(ability).to be_able_to(:update, reservation)
        end
      end
    end

    describe 'schedule' do
      describe 'of course created' do
        let(:enrollment) { create(:enrollment, user: user) }
        let(:schedule) { build(:schedule, instance: enrollment.instance) }

        before do
          enrollment.instance.update(user: user)
        end

        it 'can read' do
          expect(ability).to be_able_to(:read, schedule)
        end
        it 'cannot delete' do
          expect(ability).not_to be_able_to(:delete, schedule)
        end
        it 'cannot create' do
          expect(ability).not_to be_able_to(:create, schedule)
        end
        it 'cannot update' do
          expect(ability).not_to be_able_to(:update, schedule)
        end
      end

      describe 'of course not created' do
        let(:schedule) { build(:schedule) }

        it 'cannot read' do
          expect(ability).not_to be_able_to(:read, schedule)
        end
        it 'cannot delete' do
          expect(ability).not_to be_able_to(:delete, schedule)
        end
        it 'cannot create' do
          expect(ability).not_to be_able_to(:create, schedule)
        end
        it 'cannot update' do
          expect(ability).not_to be_able_to(:update, schedule)
        end
      end
    end

    describe 'slot' do
      describe 'of their own' do
        let(:slot) { build(:slot, user: user) }

        it 'can read' do
          expect(ability).to be_able_to(:read, slot)
        end
        it 'cannot delete' do
          expect(ability).to be_able_to(:delete, slot)
        end
        it 'cannot create' do
          expect(ability).to be_able_to(:create, slot)
        end
        it 'cannot update' do
          expect(ability).to be_able_to(:update, slot)
        end
      end

      describe 'of other people' do
        let(:slot) { build(:slot) }

        it 'cannot read' do
          expect(ability).not_to be_able_to(:read, slot)
        end
        it 'cannot delete' do
          expect(ability).not_to be_able_to(:delete, slot)
        end
        it 'cannot create' do
          expect(ability).not_to be_able_to(:create, slot)
        end
        it 'cannot update' do
          expect(ability).not_to be_able_to(:update, slot)
        end
      end
    end

    describe 'submission' do
      describe 'of other students' do
        let(:submission) { build(:submission) }

        it 'cannot read' do
          expect(ability).not_to be_able_to(:read, submission)
        end
        it 'cannot delete' do
          expect(ability).not_to be_able_to(:delete, submission)
        end
        it 'cannot create' do
          expect(ability).not_to be_able_to(:create, submission)
        end
        it 'cannot update' do
          expect(ability).not_to be_able_to(:update, submission)
        end
      end
    end

    describe 'transfer' do
      describe 'of other people' do
        let(:transfer) { build(:transfer) }

        it 'cannot read' do
          expect(ability).not_to be_able_to(:read, transfer)
        end
        it 'cannot delete' do
          expect(ability).not_to be_able_to(:delete, transfer)
        end
        it 'cannot create' do
          expect(ability).not_to be_able_to(:create, transfer)
        end
        it 'cannot update' do
          expect(ability).not_to be_able_to(:update, transfer)
        end
      end

      describe 'of their own' do
        let(:card) { build(:card, user: user) }
        let(:transfer) { create(:transfer, card: card) }

        before do
          transfer.reload
        end
        it 'can read' do
          expect(ability).to be_able_to(:read, transfer)
        end
        it 'cannot delete' do
          expect(ability).not_to be_able_to(:delete, transfer)
        end
        it 'cannot create' do
          expect(ability).not_to be_able_to(:create, transfer)
        end
        it 'cannot update' do
          expect(ability).not_to be_able_to(:update, transfer)
        end
      end
    end

    describe 'user' do
      describe 'updating another account' do
        let(:u) { build(:user) }

        it 'can read' do
          expect(ability).to be_able_to(:read, u)
        end
        it 'cannot delete' do
          expect(ability).not_to be_able_to(:delete, u)
        end
        it 'cannot create' do
          expect(ability).not_to be_able_to(:create, u)
        end
        it 'cannot update' do
          u.save
          expect(ability).not_to be_able_to(:update, u)
        end
      end

      describe 'updating own account' do
        it 'can read' do
          expect(ability).to be_able_to(:read, user)
        end
        it 'cannot delete' do
          expect(ability).to be_able_to(:delete, user)
        end
        it 'cannot create' do
          expect(ability).to be_able_to(:create, user)
        end
        it 'cannot update' do
          expect(ability).to be_able_to(:update, user)
        end
      end
    end
  end
end
