# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Bank, type: :model do
  let(:bank) { build_stubbed(:bank) }

  it 'is valid' do
    expect(bank).to be_valid
  end

  describe 'should require' do
    it 'a token' do
      bank.token = nil
      expect(bank).not_to be_valid
    end

    it 'a name' do
      bank.name = nil
      expect(bank).not_to be_valid
    end

    it 'a country' do
      bank.country = nil
      expect(bank).not_to be_valid
    end
  end
end
