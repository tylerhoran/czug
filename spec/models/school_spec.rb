# frozen_string_literal: true

require 'rails_helper'

RSpec.describe School, type: :model do
  let(:school) { build(:school) }

  it 'is valid' do
    expect(school).to be_valid
  end

  describe 'should require' do
    it 'a name' do
      school.name = nil
      expect(school).not_to be_valid
    end
  end
end
