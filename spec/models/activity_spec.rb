# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Activity, type: :model do
  let(:activity) { build_stubbed(:activity) }

  it 'is valid' do
    expect(activity).to be_valid
  end

  describe 'should require' do
    it 'a user' do
      activity.user = nil
      expect(activity).not_to be_valid
    end

    it 'a subject' do
      activity.subject = nil
      expect(activity).not_to be_valid
    end

    it 'a name' do
      activity.name = nil
      expect(activity).not_to be_valid
    end

    it 'a direction' do
      activity.direction = nil
      expect(activity).not_to be_valid
    end
  end
end
