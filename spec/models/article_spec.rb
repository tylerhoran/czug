# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Article, type: :model do
  let(:article) { build_stubbed(:article) }

  it 'is valid' do
    expect(article).to be_valid
  end

  describe 'should require' do
    it 'a name' do
      article.name = nil
      expect(article).not_to be_valid
    end

    it 'a body' do
      article.body = nil
      expect(article).not_to be_valid
    end
  end
end
