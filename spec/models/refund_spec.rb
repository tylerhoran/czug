# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Refund, type: :model do
  let(:refund) { build_stubbed(:refund) }

  it 'is valid' do
    expect(refund).to be_valid
  end

  describe 'should require' do
    it 'a user' do
      refund.user = nil
      expect(refund).not_to be_valid
    end
    it 'a charge' do
      refund.charge = nil
      expect(refund).not_to be_valid
    end
  end
end
