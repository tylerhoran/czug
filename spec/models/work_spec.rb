# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Work, type: :model do
  let(:work) { build(:work) }

  it 'is valid' do
    expect(work).to be_valid
  end

  describe 'should require' do
    it 'a name' do
      work.name = nil
      expect(work).not_to be_valid
    end
  end
end
