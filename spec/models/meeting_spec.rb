# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Meeting, type: :model do
  let(:meeting) { build_stubbed(:meeting) }

  it 'is valid' do
    expect(meeting).to be_valid
  end

  describe 'should require' do
    it 'an occurrence' do
      meeting.occurrence = nil
      expect(meeting).not_to be_valid
    end
  end

  it 'deletes the zoom event', callbacks: true do
    instance = create(:instance)
    meeting = instance.schedule.meetings.first
    expect { meeting.destroy }.to change { Meeting.count }.by(-1)
  end
end
