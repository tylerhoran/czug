# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Charge, type: :model do
  let(:charge) { build_stubbed(:charge) }

  it 'is valid' do
    expect(charge).to be_valid
  end

  describe 'should require' do
    it 'a amount' do
      charge.amount = nil
      expect(charge).not_to be_valid
    end

    it 'a instance' do
      charge.instance = nil
      expect(charge).not_to be_valid
    end
  end
end
