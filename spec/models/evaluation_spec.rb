# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Evaluation, type: :model do
  let(:evaluation) { build_stubbed(:evaluation) }

  it 'is valid' do
    expect(evaluation).to be_valid
  end

  describe 'should require' do
    it 'an enrollment' do
      evaluation.enrollment = nil
      expect(evaluation).not_to be_valid
    end

    it 'a rating' do
      evaluation.rating = nil
      expect(evaluation).not_to be_valid
    end
  end
end
