# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Availability, type: :model do
  let(:availability) { build_stubbed(:availability) }

  it 'is valid' do
    expect(availability).to be_valid
  end

  describe 'should require' do
    it 'a user' do
      availability.user = nil
      expect(availability).not_to be_valid
    end
    it 'a course' do
      availability.course = nil
      expect(availability).not_to be_valid
    end
  end
end
