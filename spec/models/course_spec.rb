# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Course, type: :model do
  let(:course) { build_stubbed(:course) }

  it 'is valid' do
    expect(course).to be_valid
  end

  describe 'should require' do
    it 'a name' do
      course.name = nil
      expect(course).not_to be_valid
    end
    it 'a degree' do
      course.degree = nil
      expect(course).not_to be_valid
    end
  end
end
