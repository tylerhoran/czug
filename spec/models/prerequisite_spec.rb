# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Prerequisite, type: :model do
  let(:prerequisite) { build_stubbed(:prerequisite) }

  it 'is valid' do
    expect(prerequisite).to be_valid
  end

  describe 'should require' do
    it 'a prior course' do
      prerequisite.prior = nil
      expect(prerequisite).not_to be_valid
    end

    it 'a course' do
      prerequisite.course = nil
      expect(prerequisite).not_to be_valid
    end
  end
end
