# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Transfer, type: :model do
  let(:transfer) { build_stubbed(:transfer) }

  it 'is valid' do
    expect(transfer).to be_valid
  end

  describe 'should require' do
    it 'a card' do
      transfer.card = nil
      expect(transfer).not_to be_valid
    end

    it 'a amount' do
      transfer.amount = nil
      expect(transfer).not_to be_valid
    end
  end
end
