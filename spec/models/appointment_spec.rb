# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Appointment, type: :model do
  let(:appointment) { build(:appointment) }

  it 'is valid' do
    expect(appointment).to be_valid
  end

  describe 'should require' do
    it 'a user' do
      appointment.user = nil
      expect(appointment).not_to be_valid
    end

    it 'an attendee' do
      appointment.attendee = nil
      expect(appointment).not_to be_valid
    end

    it 'a time' do
      appointment.time = nil
      expect(appointment).not_to be_valid
    end
  end
end
