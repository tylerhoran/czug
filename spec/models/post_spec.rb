# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Post, type: :model do
  let(:post) { build_stubbed(:post) }

  it 'is valid' do
    expect(post).to be_valid
  end

  describe 'should require' do
    it 'a user' do
      post.user = nil
      expect(post).not_to be_valid
    end
    it 'a instance' do
      post.instance = nil
      expect(post).not_to be_valid
    end

    it 'a body' do
      post.body = nil
      expect(post).not_to be_valid
    end
  end
end
