# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Card, type: :model do
  let(:card) { build_stubbed(:card) }

  it 'is valid' do
    expect(card).to be_valid
  end

  describe 'should require' do
    it 'a user' do
      card.user = nil
      expect(card).not_to be_valid
    end
    it 'a card type' do
      card.card_type = nil
      expect(card).not_to be_valid
    end

    it 'a last four digits' do
      card.last_four = nil
      expect(card).not_to be_valid
    end

    it 'an expiration month' do
      card.expiration_month = nil
      expect(card).not_to be_valid
    end

    it 'an expiration year' do
      card.expiration_year = nil
      expect(card).not_to be_valid
    end
  end

  it 'remainings as default if the last one', callbacks: true do
    card = create(:card, default: true)
    second_card = create(:card, user: card.user, default: false)
    card.destroy
    second_card.reload
    expect(second_card.default).to be_truthy
  end
end
