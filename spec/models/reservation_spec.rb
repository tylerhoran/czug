# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Reservation, type: :model do
  let(:reservation) { build_stubbed(:reservation) }

  it 'is valid' do
    expect(reservation).to be_valid
  end

  describe 'should require' do
    it 'an availability' do
      reservation.availability = nil
      expect(reservation).not_to be_valid
    end
  end
end
