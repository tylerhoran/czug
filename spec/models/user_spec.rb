# frozen_string_literal: true

require 'rails_helper'

RSpec.describe User, type: :model do
  let(:user) { build_stubbed(:user) }

  it 'is valid' do
    expect(user).to be_valid
  end

  describe 'should require' do
    it 'an email' do
      user.email = nil
      expect(user).not_to be_valid
    end
  end
end
