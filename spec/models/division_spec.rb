# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Division, type: :model do
  let(:division) { build_stubbed(:division) }

  it 'is valid' do
    expect(division).to be_valid
  end

  describe 'should require' do
    it 'a name' do
      division.name = nil
      expect(division).not_to be_valid
    end
  end
end
