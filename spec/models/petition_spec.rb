# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Petition, type: :model do
  let(:petition) { build_stubbed(:petition) }

  it 'is valid' do
    expect(petition).to be_valid
  end

  describe 'should require' do
    it 'a user' do
      petition.user = nil
      expect(petition).not_to be_valid
    end
    it 'a degree' do
      petition.degree = nil
      expect(petition).not_to be_valid
    end
  end
end
