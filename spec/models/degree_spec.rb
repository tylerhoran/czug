# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Degree, type: :model do
  let(:degree) { build_stubbed(:degree) }

  it 'is valid' do
    expect(degree).to be_valid
  end

  describe 'should require' do
    it 'a name' do
      degree.name = nil
      expect(degree).not_to be_valid
    end
    it 'a degree' do
      degree.degree_type = nil
      expect(degree).not_to be_valid
    end
  end
end
