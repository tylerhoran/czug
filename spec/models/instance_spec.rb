# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Instance, type: :model do
  let(:instance) { build(:instance) }

  it 'is valid' do
    expect(instance).to be_valid
  end

  describe 'should require' do
    it 'a user' do
      instance.user = nil
      expect(instance).not_to be_valid
    end

    it 'a course' do
      instance.course = nil
      expect(instance).not_to be_valid
    end

    it 'a schedule' do
      instance.schedule = nil
      expect(instance).not_to be_valid
    end

    # it 'enrollments' do
    #   instance.enrollments.delete_all
    #   expect(instance).to_not be_valid
    # end
  end

  it 'gives the prof karma if completed', callbacks: true do
    instance.active!
    expect { instance.completed! }.to change { instance.user.karma }.by(20)
  end
end
