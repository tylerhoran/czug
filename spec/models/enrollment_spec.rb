# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Enrollment, type: :model do
  let(:student) { create(:onboarded_student) }
  let(:enrollment) { create(:enrollment, user: student, status: 'active') }
  let(:charge) { enrollment.charge }

  it 'is valid' do
    expect(enrollment).to be_valid
  end

  describe 'should require' do
    it 'a user' do
      enrollment.user = nil
      expect(enrollment).not_to be_valid
    end
  end

  describe 'process_refunds', callbacks: true do
    it 'refunds the student' do
      enrollment.save
      expect { enrollment.update(status: 'withdrawal') }.to change { Refund.count }.by(1)
    end
  end
end
