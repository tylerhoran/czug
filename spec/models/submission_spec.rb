# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Submission, type: :model do
  let(:submission) { build_stubbed(:submission) }

  it 'is valid' do
    expect(submission).to be_valid
  end

  describe 'should require' do
    it 'a user' do
      submission.user = nil
      expect(submission).not_to be_valid
    end

    it 'an assignment' do
      submission.assignment = nil
      expect(submission).not_to be_valid
    end
  end
end
