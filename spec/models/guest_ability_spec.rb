# frozen_string_literal: true

require 'rails_helper'
require 'cancan/matchers'

describe Ability do
  describe 'as guest' do
    let(:user) { nil }
    let(:ability) { described_class.new(user) }

    describe 'admin' do
      it 'cannot access' do
        expect(ability).not_to be_able_to(:access, :rails_admin)
      end
    end

    describe 'activity' do
      let(:activity) { build(:activity) }

      it 'cannot read' do
        expect(ability).not_to be_able_to(:read, activity)
      end
      it 'cannot delete' do
        expect(ability).not_to be_able_to(:delete, activity)
      end
      it 'cannot create' do
        expect(ability).not_to be_able_to(:create, activity)
      end
      it 'cannot update' do
        expect(ability).not_to be_able_to(:update, activity)
      end
    end

    describe 'assignment' do
      let(:assignment) { build(:assignment) }

      it 'cannot read' do
        expect(ability).not_to be_able_to(:read, assignment)
      end
      it 'cannot delete' do
        expect(ability).not_to be_able_to(:delete, assignment)
      end
      it 'cannot create' do
        expect(ability).not_to be_able_to(:create, assignment)
      end
      it 'cannot update' do
        expect(ability).not_to be_able_to(:update, assignment)
      end
    end

    describe 'availability' do
      let(:availability) { build(:availability) }

      it 'cannot read' do
        expect(ability).not_to be_able_to(:read, availability)
      end
      it 'cannot delete' do
        expect(ability).not_to be_able_to(:delete, availability)
      end
      it 'cannot create' do
        expect(ability).not_to be_able_to(:create, availability)
      end
      it 'cannot update' do
        expect(ability).not_to be_able_to(:update, availability)
      end
    end

    describe 'card' do
      let(:card) { build(:card) }

      it 'cannot read' do
        expect(ability).not_to be_able_to(:read, card)
      end
      it 'cannot delete' do
        expect(ability).not_to be_able_to(:delete, card)
      end
      it 'cannot create' do
        expect(ability).not_to be_able_to(:create, card)
      end
      it 'cannot update' do
        expect(ability).not_to be_able_to(:update, card)
      end
    end

    describe 'charge' do
      let(:charge) { build(:charge) }

      it 'cannot read' do
        expect(ability).not_to be_able_to(:read, charge)
      end
      it 'cannot delete' do
        expect(ability).not_to be_able_to(:delete, charge)
      end
      it 'cannot create' do
        expect(ability).not_to be_able_to(:create, charge)
      end
      it 'cannot update' do
        expect(ability).not_to be_able_to(:update, charge)
      end
    end

    describe 'contact' do
      it 'cannot read' do
        expect(ability).not_to be_able_to(:read, Contact)
      end
      it 'cannot delete' do
        expect(ability).not_to be_able_to(:delete, Contact)
      end
      it 'can create' do
        expect(ability).to be_able_to(:create, Contact)
      end
      it 'cannot update' do
        expect(ability).not_to be_able_to(:update, Contact)
      end
    end

    describe 'course' do
      let(:course) { build(:course) }

      it 'can read' do
        expect(ability).to be_able_to(:read, course)
      end
      it 'cannot delete' do
        expect(ability).not_to be_able_to(:delete, course)
      end
      it 'cannot create' do
        expect(ability).not_to be_able_to(:create, course)
      end
      it 'cannot update' do
        expect(ability).not_to be_able_to(:update, course)
      end
    end

    describe 'degree' do
      let(:degree) { build(:degree) }

      it 'can read' do
        expect(ability).to be_able_to(:read, degree)
      end
      it 'cannot delete' do
        expect(ability).not_to be_able_to(:delete, degree)
      end
      it 'cannot create' do
        expect(ability).not_to be_able_to(:create, degree)
      end
      it 'cannot update' do
        expect(ability).not_to be_able_to(:update, degree)
      end
    end

    describe 'division' do
      let(:division) { build(:division) }

      it 'can read' do
        expect(ability).to be_able_to(:read, division)
      end
      it 'cannot delete' do
        expect(ability).not_to be_able_to(:delete, division)
      end
      it 'cannot create' do
        expect(ability).not_to be_able_to(:create, division)
      end
      it 'cannot update' do
        expect(ability).not_to be_able_to(:update, division)
      end
    end

    describe 'enrollment' do
      let(:enrollment) { build(:enrollment) }

      it 'cannot read' do
        expect(ability).not_to be_able_to(:read, enrollment)
      end
      it 'cannot delete' do
        expect(ability).not_to be_able_to(:delete, enrollment)
      end
      it 'cannot create' do
        expect(ability).not_to be_able_to(:create, enrollment)
      end
      it 'cannot update' do
        expect(ability).not_to be_able_to(:update, enrollment)
      end
    end

    describe 'faq' do
      let(:faq) { build(:faq) }

      it 'can read' do
        expect(ability).to be_able_to(:read, faq)
      end
      it 'cannot delete' do
        expect(ability).not_to be_able_to(:delete, faq)
      end
      it 'cannot create' do
        expect(ability).not_to be_able_to(:create, faq)
      end
      it 'cannot update' do
        expect(ability).not_to be_able_to(:update, faq)
      end
    end

    describe 'instance' do
      let(:instance) { build(:instance) }

      it 'cannot read' do
        expect(ability).not_to be_able_to(:read, instance)
      end
      it 'cannot delete' do
        expect(ability).not_to be_able_to(:delete, instance)
      end
      it 'cannot create' do
        expect(ability).not_to be_able_to(:create, instance)
      end
      it 'cannot update' do
        expect(ability).not_to be_able_to(:update, instance)
      end
    end

    describe 'meeting' do
      let(:meeting) { build(:meeting) }

      it 'cannot read' do
        expect(ability).not_to be_able_to(:read, meeting)
      end
      it 'cannot delete' do
        expect(ability).not_to be_able_to(:delete, meeting)
      end
      it 'cannot create' do
        expect(ability).not_to be_able_to(:create, meeting)
      end
      it 'cannot update' do
        expect(ability).not_to be_able_to(:update, meeting)
      end
    end

    describe 'petition' do
      let(:petition) { build(:petition) }

      it 'cannot read' do
        expect(ability).not_to be_able_to(:read, petition)
      end
      it 'cannot delete' do
        expect(ability).not_to be_able_to(:delete, petition)
      end
      it 'cannot create' do
        expect(ability).not_to be_able_to(:create, petition)
      end
      it 'cannot update' do
        expect(ability).not_to be_able_to(:update, petition)
      end
    end

    describe 'post' do
      let(:post) { build(:post) }

      it 'cannot read' do
        expect(ability).not_to be_able_to(:read, post)
      end
      it 'cannot delete' do
        expect(ability).not_to be_able_to(:delete, post)
      end
      it 'cannot create' do
        expect(ability).not_to be_able_to(:create, post)
      end
      it 'cannot update' do
        expect(ability).not_to be_able_to(:update, post)
      end
    end

    describe 'prerequisite' do
      let(:prerequisite) { build(:prerequisite) }

      it 'can read' do
        expect(ability).to be_able_to(:read, prerequisite)
      end
      it 'cannot delete' do
        expect(ability).not_to be_able_to(:delete, prerequisite)
      end
      it 'cannot create' do
        expect(ability).not_to be_able_to(:create, prerequisite)
      end
      it 'cannot update' do
        expect(ability).not_to be_able_to(:update, prerequisite)
      end
    end

    describe 'refund' do
      let(:refund) { build(:refund) }

      it 'cannot read' do
        expect(ability).not_to be_able_to(:read, refund)
      end
      it 'cannot delete' do
        expect(ability).not_to be_able_to(:delete, refund)
      end
      it 'cannot create' do
        expect(ability).not_to be_able_to(:create, refund)
      end
      it 'cannot update' do
        expect(ability).not_to be_able_to(:update, refund)
      end
    end

    describe 'reservation' do
      let(:reservation) { build(:reservation) }

      it 'cannot read' do
        expect(ability).not_to be_able_to(:read, reservation)
      end
      it 'cannot delete' do
        expect(ability).not_to be_able_to(:delete, reservation)
      end
      it 'cannot create' do
        expect(ability).not_to be_able_to(:create, reservation)
      end
      it 'cannot update' do
        expect(ability).not_to be_able_to(:update, reservation)
      end
    end

    describe 'schedule' do
      let(:schedule) { build(:schedule) }

      it 'cannot read' do
        expect(ability).not_to be_able_to(:read, schedule)
      end
      it 'cannot delete' do
        expect(ability).not_to be_able_to(:delete, schedule)
      end
      it 'cannot create' do
        expect(ability).not_to be_able_to(:create, schedule)
      end
      it 'cannot update' do
        expect(ability).not_to be_able_to(:update, schedule)
      end
    end

    describe 'slot' do
      let(:slot) { build(:slot) }

      it 'cannot read' do
        expect(ability).not_to be_able_to(:read, slot)
      end
      it 'cannot delete' do
        expect(ability).not_to be_able_to(:delete, slot)
      end
      it 'cannot create' do
        expect(ability).not_to be_able_to(:create, slot)
      end
      it 'cannot update' do
        expect(ability).not_to be_able_to(:update, slot)
      end
    end

    describe 'submission' do
      let(:submission) { build(:submission) }

      it 'cannot read' do
        expect(ability).not_to be_able_to(:read, submission)
      end
      it 'cannot delete' do
        expect(ability).not_to be_able_to(:delete, submission)
      end
      it 'cannot create' do
        expect(ability).not_to be_able_to(:create, submission)
      end
      it 'cannot update' do
        expect(ability).not_to be_able_to(:update, submission)
      end
    end

    describe 'transfer' do
      let(:transfer) { build(:transfer) }

      it 'cannot read' do
        expect(ability).not_to be_able_to(:read, transfer)
      end
      it 'cannot delete' do
        expect(ability).not_to be_able_to(:delete, transfer)
      end
      it 'cannot create' do
        expect(ability).not_to be_able_to(:create, transfer)
      end
      it 'cannot update' do
        expect(ability).not_to be_able_to(:update, transfer)
      end
    end

    describe 'user' do
      let(:u) { build(:user) }

      it 'can read' do
        expect(ability).to be_able_to(:read, u)
      end
      it 'cannot delete' do
        expect(ability).not_to be_able_to(:delete, u)
      end
      it 'cannot create' do
        expect(ability).not_to be_able_to(:create, u)
      end
      it 'cannot update' do
        u.save
        expect(ability).not_to be_able_to(:update, u)
      end
    end
  end
end
